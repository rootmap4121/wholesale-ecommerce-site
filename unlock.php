<?php 
session_start();
include('ajax/db_Class.php');
$obj = new db_class();
@$cart = $obj->cart($_SESSION['SESS_CART']);
$obj->checkmenu();
$orderid=@$_SESSION['SESS_ORDER'];
if($orderid=='')
{
    session_regenerate_id();
    $_SESSION['SESS_ORDER']=time();
    session_write_close();
}
else 
{
    $_SESSION['SESS_ORDER']=$orderid;
}
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>Mobile online store</title>
        <meta name="description" content="Mobile online store" />
        <link href="images/favicon.png" rel="icon" />
	<?php include('include/headlink.php'); ?>
	
        <link href="css/select2-bootstrap.css" rel="stylesheet" type="text/css">
        <link href="css/select2.css" rel="stylesheet" type="text/css">
        <script>
        function showUser(str)
        {
        if (str=="")
          {
          document.getElementById("sp1").innerHTML="";
          return;
          }
        if (window.XMLHttpRequest)
          {// code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttp=new XMLHttpRequest();
          }
        else
          {// code for IE6, IE5
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
          }
        xmlhttp.onreadystatechange=function()
          {
          if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {
            $("#sp1").fadeOut();
            $("#sp1").fadeIn();
            document.getElementById("sp1").innerHTML=xmlhttp.responseText;
            }
          } 
        xmlhttp.open("GET","ajax/search.php?q="+str,true);
        xmlhttp.send();
         
        var xmlhttps;    
        if (str=="")
          {
          document.getElementById("sp").innerHTML="";
          return;
          }
        if (window.XMLHttpRequest)
          {// code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttps=new XMLHttpRequest();
          }
        else
          {// code for IE6, IE5
          xmlhttps=new ActiveXObject("Microsoft.XMLHTTPS");
          }
        xmlhttps.onreadystatechange=function()
          {
          if (xmlhttps.readyState==4 && xmlhttps.status==200)
            {
            $("#sp").fadeOut();
            $("#sp").fadeIn();
            document.getElementById("sp").innerHTML=xmlhttps.responseText;
            }
          }  
        xmlhttps.open("GET","ajax/pd.php?p="+str,true);
        xmlhttps.send();
         
        }
        </script>
        <script type="text/javascript">
            jQuery(function($) {
                    $("#single").change(function(){
                      $("#sp").show();
                      $("#sp1").show();
                    });
            })
        </script>
        
        <style type="text/css">
        #sp{ display:none; }
        #sp1{ display:none; }
        </style>
    </head>
    <body class="common-home">
        <div class="bg-1">
            <div class="main-shining">
                <div class="row-1">
                    <?php include('include/header.php'); ?>
                    <div class="clear"></div>
<?php include('include/menu.php'); ?>
                </div>
                <div class="clear"></div>

<?php //include('include/slider_home.php');  ?>

                <div class="main-container">
                    <p id="back-top"> <a href="#top"><span></span></a> </p>
                    <div id="container">
                        <div id="notification"> </div>

<?php include('include/category.php'); ?>

                        <div id="content">
                            
                           


<div class="breadcrumb">
    <a href="index.php">Home</a>
    » <a class="last" href="unlock.php">Unlocking Service </a> » <a class="last" href="unlock.php">IMEI Service</a>
      </div>
  
    <form action="unlockorder.php" method="post" id="contact">
   
    <div class="content contact-f"><h2><strong>Place A New IMEI Order</strong></h2>
        <div class="padd-content">   
            
            
            
             <div id="sp1" class="extra-wrap"> 
                <div class="search-pice-detail border-1">
                    <div class="col-3"><img src="images/ajax_loader_green_64.gif" alt="Ajax Loading" /></div>
                    <div class="clear"></div>
                </div>
            </div>
            
            <div class="extra-wrap"> 
                <b>Services :</b><br>
                <select  id="single" class="form-control select2" onchange="showUser(this.value)" name="service">
                        <?php
                        $da=$obj->SelectAll("label");
                        if(!empty($da))
                        foreach($da as $rr):
                        ?>
                        <optgroup label="<?php echo $rr->name; ?>">
                            <?php 
                            $data=$obj->SelectAllByID("service",array("label"=>$rr->id));
                            if(!empty($data))
                            foreach ($data as $row):
                            ?>
                            <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                            <?php endforeach; ?>
                        </optgroup>
                        <?php
                        endforeach;
                        ?>
                </select>
                <br>
                <br>
                <div class="clear"></div>
            </div>
            
            <div id="sp" class="extra-wrap"> 
                <div class="col-3"><img src="images/ajax_loader_green_64.gif" alt="Ajax Loading" /></div>
            </div>
            
                       
            
            <div class="extra-wrap"> 
                <div class="contact-form-left"><b>Notes:</b><br>
                <input class="q2" name="notes" value="" type="text">
                <br>
                </div>
            </div>
        </div>
    <b>Comments:</b><br>
    <textarea name="comments" cols="40" rows="10" style="width: 99%;"></textarea>
        <br>
    <b class="cap-p">Response E-Mail:</b><br>
    <input class="capcha" name="respond_email" type="text">
    <br>
    <div class="buttons">
        <div class="left"><button class="button" name="submit" type="submit"><span>Continue</span></button></div>
    </div>
  </div>

  </form>




                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
<?php include('include/footer.php'); ?>
                <script type="text/javascript" 	src="js/livesearch.js"></script>
            </div>
        </div>
        <script>

  var placeholder = "Please Select";

  $('.select2, .select2-multiple').select2({ placeholder: placeholder });

  $(".select2, .select2-multiple, .select2-allow-clear, .select2-remote").on( select2OpenEventName, function() {
	if ( $(this).parents('[class*="has-"]').length ) {
	  var classNames = $(this).parents('[class*="has-"]')[0].className.split(/\s+/);
	  for (var i=0; i<classNames.length; ++i) {
		  if ( classNames[i].match("has-") ) {
			$('#select2-drop').addClass( classNames[i] );
		  }
	  }
	}

  });

</script>
    </body>
</html>
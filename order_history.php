<?php 
session_start();
$errmsg_arr[]="";
$errflag = false;
include('ajax/db_Class.php');
$obj = new db_class();
@$cart = $obj->cart($_SESSION['SESS_CART']);
if(isset($_POST['login']))
{
    extract($_POST);
    $obj->login($email,$password,"account.php");
}

function payment($pay)
{
    if($pay==1)
    {
        return "paypal";
    }
    else 
    {
        return "Local payment";   
    }
}
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>Mobile online store</title>
        <meta name="description" content="Mobile online store" />
        <link href="images/favicon.png" rel="icon" />
        <?php include('include/headlink.php'); ?>
    </head>
    <body class="account-login">
        <div class="bg-1">
            <div class="main-shining">
                <div class="row-1">
                    <?php include('include/header.php'); ?>
                    <div class="clear"></div>
                    <?php include('include/menu.php'); ?>
                </div>
                <div class="clear"></div>

                <div class="main-container">
                    <p id="back-top"> <a href="#top"><span></span></a> </p>
                    <div id="container"><?php echo $obj->ShowMsg(); ?>
                        <div id="notification"> </div>
                        
                           <?php include('include/category.php'); ?>
                        
                        <div id="content">  <div class="breadcrumb">
                                <a href="index.php">Home</a>
                                &raquo; <a href="account.php">Account</a>
                                &raquo; <a href="order_history.php">Order History</a>
                            </div>

                            <div class="box-container">
                                <h1>Your Order History</h1>
  <table class="list">
    <thead>
      <tr>
        <td class="left">Order Date</td>
        <td class="left">Order ID</td>
        <td class="right">Payment Method</td>
        <td class="right">Amount (USD)</td>
        <td class="right">Order Status</td>
      </tr>
    </thead>
    <tbody>
        <?php
        $chkorder=$obj->exists_multiple("product_order",array("cusid"=>$_SESSION['SESS_CUSID']));
        if($chkorder==0)
        {
        ?>
      <tr>
        <td class="center" colspan="4">You do not have any transactions!</td>
      </tr>
        <?php 
        }
        else 
        {
     $data=$obj->SelectAllByID("product_order",array("cusid"=>$_SESSION['SESS_CUSID']));
     if(!empty($data))
     foreach($data as $row)
     {
         $order=$obj->SelectAllByID("cart",array("cart_id"=>$row->cart_id));
         $am=0;
         foreach ($order as $or)
         {
             $price=$obj->SelectAllByVal("product","id",$or->pid,"price");
             $tp=$or->quantity*$price;
             $am+=$tp;
         }
         
         $orderdis=$obj->SelectAllByID("order_discount",array("cart_id"=>$row->cart_id));
         $disc=0;
         if(!empty($orderdis))
         foreach ($orderdis as $dis)
         {
             
             $disc+=$dis->discount_amount;
         }
         
         $amount=$am-$disc;
         ?>
      <tr>
        <td class="left"><?php echo $row->date; ?></td>
        <td class="left"><?php echo $row->cart_id; ?></td>
        <td class="right"><?php echo payment($row->payment_method); ?></td>
        <td class="right"><?php echo $amount; ?></td>
        <td class="right"><?php echo $obj->order_status($row->status); ?></td>
      </tr>
      <?php
     }
     }
        ?>
     </tbody>
  </table>
  
  <div class="buttons">
      <div class="right"><a href="account.php" class="button-cont-right"><span>Continue</span></a></div>
  </div>
                            </div>
                        </div>
                        <script type="text/javascript"><!--
                        $('#login input').keydown(function(e) {
                                if (e.keyCode == 13) {
                                    $('#login').submit();
                                }
                            });
//--></script> 
                        <div class="clear"></div>
                    </div>
                </div>
                <?php include('include/footer.php'); ?>
                <script type="text/javascript" 	src="js/livesearch.js"></script>
            </div>
        </div>
        <script type="text/javascript"></script>
    </body>
</html>
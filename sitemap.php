<?php session_start();
include('ajax/db_Class.php');
$obj = new db_class();
@$cart = $obj->cart($_SESSION['SESS_CART']); ?>
<!DOCTYPE html>
<html dir="ltr" lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>Mobile online store</title>
        <meta name="description" content="Mobile online store" />
        <link href="images/favicon.png" rel="icon" />
<?php include('include/headlink.php'); ?>
    </head>
    <body class="common-home">
        <div class="bg-1">
            <div class="main-shining">
                <div class="row-1">
                    <?php include('include/header.php'); ?>
                    <div class="clear"></div>
<?php include('include/menu.php'); ?>
                </div>
                <div class="clear"></div>

<?php //include('include/slider_home.php');  ?>

                <div class="main-container">
                    <p id="back-top"> <a href="#top"><span></span></a> </p>
                    <div id="container">
                        <div id="notification"> </div>

<?php include('include/category.php'); ?>

                        <div id="content">
                            
                           



                            
                            <div class="breadcrumb">
                                <a href="index.php">Home</a>
                                » <a class="last" href="sitemap.php">Site Map</a>
      </div>
  
  <div class="box-container">
    <h1>Site Map</h1>
    <div class="sitemap-info">
    <div class="left">
      <ul>
          <?php
          $data=$obj->SelectAll("category");
          if(!empty($data))
          foreach ($data as $row):
          ?>
                <li><a href="category.php?cid=<?php echo $row->id; ?>"><?php echo $row->name; ?></a>
                <?php
                if($obj->exists_multiple("subcategory",array("cid"=>$row->id))!=0)
                {
                ?>
                <ul>
                    <?php
                    $sdata=$obj->SelectAllByID("subcategory",array("cid"=>$row->id));
                    if(!empty($sdata))
                    foreach($sdata as $sro):
                    ?>
                    <li><a href="subcategory.php?scid=<?php echo $sro->id; ?>"><?php echo $sro->name; ?> </a></li>
                    <?php 
                    endforeach;
                    ?>
               </ul>    
                <?php 
                }
                ?>
                </li>
             <?php
              endforeach;
             ?>
                
              </ul>
    </div>
    <div class="right">
      <ul>
          <li><a href="create_account.php">Register </a>
          <li><a href="login.php">Login</a>
        <li><a href="account.php">My Account</a>
          <ul>
              <li><a href="account.php">Account Information</a></li>
              <li><a href="account.php">Password</a></li>
            <li><a href="account.php">Address Book</a></li>
            <li><a href="order_history.php">Order History</a></li>
            
          </ul>
        </li>
        <li><a href="shoppingcart.php">Shopping Cart</a></li>
        <li><a href="checkout.php">Checkout</a></li>
        <li>Information          <ul>
                <li><a href="about.php">About Us</a></li>
                
                <li><a href="privacy.php">Privacy Policy</a></li>
                <li><a href="term.php">Terms &amp; Conditions</a></li>
                <li><a href="contact.php">Contact Us</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
  </div>




                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
<?php include('include/footer.php'); ?>
                <script type="text/javascript" 	src="js/livesearch.js"></script>
            </div>
        </div>
        <script type="text/javascript"></script>
    </body>
</html>
<?php 
session_start();
$errmsg_arr[]="";
$errflag = false;
include('ajax/db_Class.php');
$obj = new db_class();
@$cart = $obj->cart($_SESSION['SESS_CART']);

if(isset($_POST['submit']))
{
    extract($_POST);
    if(empty($firstname) or empty($lastname) or empty($email) or empty($telephone) or empty($address1) or empty($city) or empty($postcode) or empty($country) or empty($state) or empty($password) or empty($cpassword))
    {
        echo $obj->Error("Some Field is Empty",$obj->filename());
    }
    elseif($password!=$cpassword)
    {
        echo $obj->Error("Password Mismatch",$obj->filename());
    }
    else
    {
        $insertarray=array("fname"=>$firstname,"lname"=>$lastname,"email"=>$email,"talephone"=>$telephone,"address1"=>$address1,"address2"=>$address2,"city"=>$city,"company"=>$company,"company_id"=>$companyid,"postcode"=>$postcode,"country"=>$country,"state"=>$state,"password"=>$obj->password($password),"date"=>date('Y-m-d'),"status"=>1);
        if($obj->insert("customer", $insertarray)==1)
        {
            $shipping=array("cusid"=>$obj->SelectAllByVal("customer","email",$email,"id"),"address"=>$firstname.",".$lastname.",".$address1.",".$city.",".$postcode.",".$country.",".$state,"status"=>1);
            $obj->insert("shipping_address",$shipping);
            //email Script Start 
        //$subject = "Product Order Via Wireless Geeks";
        // Receive form's message value into php $message variable
        $message = '<!DOCTYPE HTML><head><meta http-equiv="content-type" content="text/html"><title>Email notification</title>';
 $message .= "<link href='http://wirelessgeekswholesale.com/css/stylesheet.css' rel='stylesheet' type='text/css' /></head>";
 $message .= "<body><div class='bg-1'><div style='display: block;' class='checkout-content'><div class='checkout-product'>";
        $message .= "Automated Account Opening Confirmation Email From Wireless Geeks ";
        $message .= "<img src='http://wirelessgeekswholesale.com/images/logo.png'><br>";
        $message .= "Dear Sir ,<br> Your account has been created successfully, Please wait while for admin review & appove your account.<br> You will be notify shortly using your email, Keep in Touch. <br><br> Thank You. <br></div></div></div></body>";

                
                
                
        $to=$email;   
        $too='admin@wirelessgeekswholesale.com'; // give to email address
        $subject = 'Wiressless Geeks - Account Opening Notification';  //change subject of email
        $from    = 'admin@wirelessgeekswholesale.com';                           // give from email address

        // mandatory headers for email message, change if you need something different in your setting.
        $headers  = "From: " . $from . "\r\n";
        $headers .= "Reply-To: ". $from . "\r\n";
        $headers .= "CC: admin@wirelessgeekswholesale.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        mail($to, $subject, $message, $headers);  
        mail($too, $subject, $message, $headers); 
        //email Script End
            echo $obj->Success("Your Account Successfully Created , Please wait while we review & appove your account.",$obj->filename());
        }
        else
        {
            echo $obj->Error("Failed, Please Try Again",$obj->filename());
        }
    }
}

?>
<!DOCTYPE html>
<html dir="ltr" lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>Mobile online store</title>
        <meta name="description" content="Mobile online store" />
        <link href="images/favicon.png" rel="icon" />
        <?php include('include/headlink.php'); ?>
    </head>
    <body class="account-register">
        <div class="bg-1">
            <div class="main-shining">
                <div class="row-1">
                    <?php include('include/header.php'); ?>
                    <div class="clear"></div>
                    <?php include('include/menu.php'); ?>
                </div>
                <div class="clear"></div>

                <div class="main-container">
                    <p id="back-top"> <a href="#top"><span></span></a> </p>
                    <div id="container">
                        <div id="notification"> </div>
                        <?php include('include/category.php'); ?>

                        <div id="content">  <div class="breadcrumb">
                                <a href="index.php">Home</a>
                                &raquo; <a href="account.php">Account</a>
                                &raquo; <a href="create_account.php">Register</a>
                            </div>


                            <div class="box-container">
                                <?php echo $obj->ShowMsg(); ?>
                                <h1>Register Account</h1>
                                <p>If you already have an account with us, please login at the <a href="login.php">login page</a>.</p>
                                <form action="" method="post" enctype="multipart/form-data" id="register">
                                    <h2>Your Personal Details</h2>
                                    <div class="content">
                                        <table class="form">      
                                            <tr>
                                                <td><span class="required">*</span> First Name:</td>
                                                <td><input class="q1" type="text" name="firstname" value="" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="required">*</span> Last Name:</td>
                                                <td><input class="q1" type="text" name="lastname" value="" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="required">*</span> E-Mail:</td>
                                                <td><input class="q1" type="text" name="email" value="" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="required">*</span> Telephone:</td>
                                                <td><input class="q1" type="text" name="telephone" value="" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Fax:</td>
                                                <td><input class="q1" type="text" name="fax" value="" /></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <h2>Your Address</h2>
                                    <div class="content">
                                        <table class="form">
                                            <tr>
                                                <td>Company:</td>
                                                <td><input class="q1" type="text" name="company" value="" /></td>
                                            </tr>     
                                                   
                                            <tr id="company-id-display">
                                                <td> Company ID:</td>
                                                <td><input class="q1" type="text" name="companyid" value="" />
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td><span class="required">*</span> Address 1:</td>
                                                <td><input class="q1" type="text" name="address1" value="" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Address 2:</td>
                                                <td><input class="q1" type="text" name="address2" value="" /></td>
                                            </tr>
                                            <tr>
                                                <td><span class="required">*</span> City:</td>
                                                <td><input class="q1" type="text" name="city" value="" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span id="postcode-required" class="required">*</span> Post Code:</td>
                                                <td><input class="q1" type="text" name="postcode" value="" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="required">*</span> Country:</td>
                                                <td><select name="country">
                                                        <option value=""> --- Please Select --- </option>
                                                        <option value="United States" selected="selected">United States</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="required">*</span> Region / State:</td>
                                                <td>
                                                    <select name="state">
                                                        <option value=""> --- Please Select --- </option>
                                                        <option value="Alabama">Alabama</option>
                                                        <option value="Alaska">Alaska</option>
                                                        <option value="American Samoa">American Samoa</option>
                                                        <option value="Arizona">Arizona</option>
                                                        <option value="Arkansas">Arkansas</option>
                                                        <option value="Armed Forces Africa">Armed Forces Africa</option>
                                                        <option value="Armed Forces Americas">Armed Forces Americas</option>
                                                        <option value="Armed Forces Canada">Armed Forces Canada</option>
                                                        <option value="Armed Forces Europe">Armed Forces Europe</option>
                                                        <option value="Armed Forces Middle East">Armed Forces Middle East</option>
                                                        <option value="Armed Forces Pacific">Armed Forces Pacific</option>
                                                        <option value="California">California</option>
                                                        <option value="Colorado">Colorado</option>
                                                        <option value="Connecticut">Connecticut</option>
                                                        <option value="Delaware">Delaware</option>
                                                        <option value="District of Columbia">District of Columbia</option>
                                                        <option value="Federated States Of Micronesia">Federated States Of Micronesia</option>
                                                        <option value="Florida">Florida</option>
                                                        <option value="Georgia">Georgia</option>
                                                        <option value="Guam">Guam</option>
                                                        <option value="Hawaii">Hawaii</option>
                                                        <option value="Idaho">Idaho</option>
                                                        <option value="Illinois">Illinois</option>
                                                        <option value="Indiana">Indiana</option>
                                                        <option value="Iowa">Iowa</option>
                                                        <option value="Kansas">Kansas</option>
                                                        <option value="Kentucky">Kentucky</option>
                                                        <option value="Louisiana">Louisiana</option>
                                                        <option value="Maine">Maine</option>
                                                        <option value="Marshall Islands">Marshall Islands</option>
                                                        <option value="Maryland">Maryland</option>
                                                        <option value="Massachusetts">Massachusetts</option>
                                                        <option value="Michigan">Michigan</option>
                                                        <option value="Minnesota">Minnesota</option>
                                                        <option value="Mississippi">Mississippi</option>
                                                        <option value="Missouri">Missouri</option>
                                                        <option value="Missouri">Montana</option>
                                                        <option value="Nebraska">Nebraska</option>
                                                        <option value="Nevada">Nevada</option>
                                                        <option value="New Hampshire">New Hampshire</option>
                                                        <option value="New Jersey">New Jersey</option>
                                                        <option value="New Mexico">New Mexico</option>
                                                        <option value="New York">New York</option>
                                                        <option value="North Carolina">North Carolina</option>
                                                        <option value="North Dakota">North Dakota</option>
                                                        <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                        <option value="Ohio">Ohio</option>
                                                        <option value="Oklahoma">Oklahoma</option>
                                                        <option value="Oregon">Oregon</option>
                                                        <option value="Palau">Palau</option>
                                                        <option value="Pennsylvania">Pennsylvania</option>
                                                        <option value="Puerto Rico">Puerto Rico</option>
                                                        <option value="Rhode Island">Rhode Island</option>
                                                        <option value="South Carolina">South Carolina</option>
                                                        <option value="South Dakota">South Dakota</option>
                                                        <option value="Tennessee">Tennessee</option>
                                                        <option value="Texas">Texas</option>
                                                        <option value="Utah">Utah</option>
                                                        <option value="Vermont">Vermont</option>
                                                        <option value="Virgin Islands">Virgin Islands</option>
                                                        <option value="Virginia">Virginia</option>
                                                        <option value="Washington">Washington</option>
                                                        <option value="West Virginia">West Virginia</option>
                                                        <option value="Wisconsin">Wisconsin</option>
                                                        <option value="Wyoming">Wyoming</option>
                                                    </select>
                
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <h2>Your Password</h2>
                                    <div class="content">
                                        <table class="form">
                                            <tr>
                                                <td><span class="required">*</span> Password:</td>
                                                <td><input class="q1" type="password" name="password"  />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><span class="required">*</span> Password Confirm:</td>
                                                <td><input class="q1" type="password" name="cpassword"  />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    
                                    <div class="buttons">
                                        <div class="right">
                                            <button style="margin-right: 20px;" type="reset" name="submit" class="btn"><span>Reset Data</span></button><button type="submit" name="submit" class="btn"><span>Create Account Now</span></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>


                        <script type="text/javascript"><!--
                        $(document).ready(function() {
                                $('.colorbox').colorbox({
                                    width: 640,
                                    height: 480
                                });
                            });
//--></script> 
                        <div class="clear"></div>
                    </div>
                </div>
                <?php include('include/footer.php'); ?>
                <script type="text/javascript" 	src="js/livesearch.js"></script>
            </div>
        </div>
        <script type="text/javascript"></script>
    </body>
</html>
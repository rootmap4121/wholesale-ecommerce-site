-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 19, 2014 at 06:37 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `igeek`
--

-- --------------------------------------------------------

--
-- Table structure for table `barcode`
--

CREATE TABLE IF NOT EXISTS `barcode` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `barcode`
--

INSERT INTO `barcode` (`id`, `name`, `date`, `status`) VALUES
(1, 'Napa', '2014-03-07', 1),
(2, 'qqq', '2014-03-18', 1),
(3, 'asdsa', '2014-03-18', 1),
(4, 'dsfdsf', '2014-03-18', 1),
(5, '346356', '2014-03-18', 1),
(6, '346356', '2014-03-18', 1),
(7, '346356', '2014-03-18', 1),
(8, '', '2014-03-18', 1),
(9, '346356', '2014-03-18', 1),
(10, '346356', '2014-03-18', 1),
(11, '346356', '2014-03-18', 1),
(12, '346356', '2014-03-18', 1),
(13, '346356', '2014-03-18', 1),
(14, '346356', '2014-03-18', 1),
(15, '', '2014-03-18', 1),
(16, 'sdgdsfg', '2014-03-18', 1),
(17, '346356', '2014-03-18', 1),
(18, '346356', '2014-03-18', 1),
(19, 'yt', '2014-03-18', 1),
(20, 'sdfgsdfg', '2014-03-18', 1),
(21, '', '2014-03-18', 1),
(22, '564564', '2014-03-18', 1),
(23, '564564', '2014-03-18', 1),
(24, '564564', '2014-03-18', 1),
(25, '564564', '2014-03-18', 1),
(26, '564564', '2014-03-18', 1),
(27, '346356', '2014-03-19', 1),
(28, '', '2014-04-18', 1),
(29, '', '2014-04-18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `date`, `status`) VALUES
(10, 'IPHONE PARTS', '2014-04-18', 1),
(11, 'IPAD PARTS', '2014-04-19', 1),
(12, 'SAMSUNG GALAXY PARTS', '2014-04-19', 1),
(13, 'UNLOCKING SERVICES', '2014-04-19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `blood_group` varchar(20) NOT NULL,
  `dob` date DEFAULT NULL,
  `contactnumber` varchar(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `name`, `gender`, `blood_group`, `dob`, `contactnumber`, `address`, `username`, `password`, `date`, `status`) VALUES
(13, 'amsit', '1', 'A+', '2014-01-01', '1927608261', 'qq', 'amsit', '8139be3698a555383168ec998c0e65bf', '2014-02-09', 2),
(56, 'justin', '1', '', '1988-02-02', '+8888888888888888888', 'Null', 'justin', 'e10adc3949ba59abbe56e057f20f883e', '2014-04-18', 2);

-- --------------------------------------------------------

--
-- Table structure for table `employee_basic_info`
--

CREATE TABLE IF NOT EXISTS `employee_basic_info` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emplid` int(20) NOT NULL,
  `father_husbandname` varchar(255) DEFAULT NULL,
  `mothers_name` varchar(255) DEFAULT NULL,
  `blood_group` varchar(20) DEFAULT NULL,
  `marital_status` varchar(255) DEFAULT NULL,
  `marrige_date` date DEFAULT NULL,
  `religion` varchar(255) DEFAULT NULL,
  `home_district` varchar(255) DEFAULT NULL,
  `relative_in_sc` varchar(255) DEFAULT NULL,
  `relation` varchar(255) DEFAULT NULL,
  `tin_number` varchar(255) DEFAULT NULL,
  `driving_licence` varchar(255) DEFAULT NULL,
  `passport_number` varchar(255) DEFAULT NULL,
  `date_of_expire` date DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `employee_basic_info`
--

INSERT INTO `employee_basic_info` (`id`, `emplid`, `father_husbandname`, `mothers_name`, `blood_group`, `marital_status`, `marrige_date`, `religion`, `home_district`, `relative_in_sc`, `relation`, `tin_number`, `driving_licence`, `passport_number`, `date_of_expire`, `date`, `status`) VALUES
(1, 22, 'Rofiqur', 'Rokia', 'A+', 'Single', '2012-01-01', 'islam', 'Dhaka', 'Yes', 'Friend', 'BD32432432434', 'AP21321312321', 'CPA3432498324', '2020-01-01', NULL, NULL),
(11, 13, 'rofiqur', 'rokia', 'A+', 'Single', '0000-00-00', 'islam', 'Dhaka', 'Yes', 'Friend', '4455556666666666', '4444444444', '444444444444488', '0000-00-00', NULL, NULL),
(12, 53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employee_educational_info`
--

CREATE TABLE IF NOT EXISTS `employee_educational_info` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emplid` int(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `board` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `division` varchar(255) DEFAULT NULL,
  `gpa` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `employee_educational_info`
--

INSERT INTO `employee_educational_info` (`id`, `emplid`, `title`, `board`, `year`, `division`, `gpa`, `date`, `status`) VALUES
(11, 22, 'SSC', 'comilla', '2006', 'A', '4.25', NULL, NULL),
(12, 22, 'HSC', 'comilla', '2008', 'A+', '5.00', NULL, NULL),
(13, 22, 'Diploma', 'comilla', '2010', 'A', '3.75', NULL, NULL),
(20, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 53, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employee_emergency_contact`
--

CREATE TABLE IF NOT EXISTS `employee_emergency_contact` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emplid` int(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `relation` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `employee_emergency_contact`
--

INSERT INTO `employee_emergency_contact` (`id`, `emplid`, `name`, `address`, `phone`, `relation`, `date`, `status`) VALUES
(1, 22, 'Md Mahamod', 'address', 'phones', 'Me', NULL, 1),
(2, 22, 'Md Mahamod', 'address', 'phones', 'Me', NULL, 2),
(19, 13, NULL, NULL, NULL, NULL, NULL, 1),
(20, 13, NULL, NULL, NULL, NULL, NULL, 2),
(21, 53, NULL, NULL, NULL, NULL, NULL, 1),
(22, 53, NULL, NULL, NULL, NULL, NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `employee_present_address`
--

CREATE TABLE IF NOT EXISTS `employee_present_address` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emplid` int(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=279 ;

--
-- Dumping data for table `employee_present_address`
--

INSERT INTO `employee_present_address` (`id`, `emplid`, `address`, `phone`, `fax`, `date`, `status`) VALUES
(1, 22, 'dfsd', 'sds', 'dsdsd', NULL, 1),
(2, 22, 'dfsd', 'sds', 'dsdsd', NULL, 2),
(275, 13, NULL, NULL, NULL, NULL, 1),
(276, 13, NULL, NULL, NULL, NULL, 2),
(277, 53, NULL, NULL, NULL, NULL, 1),
(278, 53, NULL, NULL, NULL, NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `label`
--

CREATE TABLE IF NOT EXISTS `label` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` text,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `label`
--

INSERT INTO `label` (`id`, `name`, `date`, `status`) VALUES
(1, 'iPhone USA - Listed below in section "iPhones" are for Non-USA iPhones', '2014-04-18', 1),
(2, 'Checker', '2014-04-18', 1),
(3, 'USA Services', '2014-04-18', 1),
(4, 'HTC Services / Specials', '2014-04-18', 1),
(5, 'Samsung', '2014-04-18', 1),
(6, 'Blackberry Q5 / Q10 / Z10 / Z30', '2014-04-18', 1),
(7, 'Alcatel', '2014-04-18', 1),
(8, 'Huawei', '2014-04-18', 1),
(9, 'USA', '2014-04-18', 1),
(10, 'ESN Cleaning', '2014-04-18', 1),
(11, 'Blackberry', '2014-04-18', 1),
(12, 'Dell', '2014-04-18', 1),
(13, 'HTC', '2014-04-18', 1),
(14, 'iPhones', '2014-04-18', 1),
(15, 'LG', '2014-04-18', 1),
(16, 'Motorola', '2014-04-18', 1),
(17, 'Sidekick', '2014-04-18', 1),
(18, 'Sony Ericsson', '2014-04-18', 1),
(19, 'MSL / SPC (CDMA Phones)', '2014-04-18', 1),
(20, 'Manufacture (BB/HTC/LG/Motorola/Samsung ABOVE)', '2014-04-18', 1),
(21, 'CDMA / GSM Services', '2014-04-18', 1),
(22, 'Canada', '2014-04-18', 1),
(23, 'Old Services', '2014-04-18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `photo` text NOT NULL,
  `photo2` varchar(255) NOT NULL,
  `photo3` varchar(255) NOT NULL,
  `photo4` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `emplid` int(20) DEFAULT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` varchar(20) DEFAULT NULL,
  `cid` int(20) DEFAULT NULL,
  `scid` int(20) DEFAULT NULL,
  `reorder` int(20) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `photo`, `photo2`, `photo3`, `photo4`, `description`, `emplid`, `quantity`, `price`, `cid`, `scid`, `reorder`, `date`, `status`) VALUES
(7, 'Iphone Parts', '90X90_1397822717_loock.png', '160X160_1397822717_loock.png', '170X170_1397822717_loock.png', '300X300_1397822717_loock.png', '<p>zcxzxzc</p>\r\n', 56, 2, '200', 10, 4, 1, '2014-04-18', 1),
(8, 'Iphone 4 & 4s Diagnosis', '90X90_1397913693_diagnostic.png', '160X160_1397913693_diagnostic.png', '170X170_1397913693_diagnostic.png', '300X300_1397913693_diagnostic.png', '<p>Iphone 4 &amp; 4s Diagnosis</p>\r\n', 56, 1, '00', 10, 4, 1, '2014-04-19', 1),
(9, 'Led Replacement', '90X90_1397916370_glass.png', '160X160_1397916370_glass.png', '170X170_1397916370_glass.png', '300X300_1397916370_glass.png', '<p>Led Replacement</p>\r\n', 56, 1, '79', 10, 4, 1, '2014-04-19', 1),
(10, 'Phone Back Cover', '90X90_1397923033_back.png', '160X160_1397923033_back.png', '170X170_1397923033_back.png', '300X300_1397923033_back.png', '<p>description :<br />\r\n&nbsp;</p>\r\n', 56, 2, '200', 10, 4, 1, '2014-04-19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE IF NOT EXISTS `service` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` text,
  `label` int(20) DEFAULT NULL,
  `price` varchar(20) DEFAULT NULL,
  `delivery_time` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id`, `name`, `label`, `price`, `delivery_time`, `description`, `date`, `status`) VALUES
(1, 'AT&T iPhone (3-10 days) - 3G/3GS/4/4S/5 CLEAN IMEIS 70-80% FOUND - 40 Credit', 1, '40', '3-10 Days', '<div class="info" id="information">\r\n<p>This service will unlock AT&amp;T iPhones that are : 2G, 3G, 3GS, 4, 4S, 5.</p>\r\n&nbsp;\r\n\r\n<p>Orders that are completed are between 70-80%.</p>\r\n&nbsp;\r\n\r\n<p>Reasons the devices can fail are due to: Phone is in contract, active on a line, reported lost/stolen or has an unpaid balance. Some of these orders may still go through if meet the criterias of in contract or active on a line.</p>\r\n&nbsp;\r\n\r\n<p>Speed of service is 3-10 days.</p>\r\n</div>\r\n', '2014-04-19', 1),
(2, 'AT & T iPhone SPECIAL (3-10 days) - 2G/3G/3GS/4 ONLY (NO 4S/5) 70-80% FOUND - 25 Credit', 1, '30', '3-10 Days', '<div class="info" id="information">\r\n<p>This service will unlock AT&amp;T iPhones that are : 2G, 3G, 3GS, 4</p>\r\n&nbsp;\r\n\r\n<p><span style="font-size: medium; color: #ff0000;"><strong>ANY IPHONE 4S / 5 / 5S / 5C ORDERED WILL BE REJECTED</strong></span></p>\r\n&nbsp;\r\n\r\n<p>Orders that are completed are between 70-80%.</p>\r\n&nbsp;\r\n\r\n<p>Reasons the devices can fail are due to: Phone is in contract, active on a line, reported lost/stolen or has an unpaid balance. Some of these orders may still go through if meet the criterias of in contract or active on a line.</p>\r\n&nbsp;\r\n\r\n<p>Speed of service is 3-10 days.</p>\r\n</div>\r\n', '2014-04-19', 1),
(3, 'AT & T iPhone (3-10 days) - 5C/5S CLEAN IMEIS 60-80% FOUND - 39 Credit', 1, '44', '3-10 Days', '<p><span style="font-size: small;"><strong>Active:</strong> No (Phone can not be Active on an Account)</span><br />\r\n<span style="font-size: small;"><strong>Activated:</strong> Yes</span><br />\r\n<span style="font-size: small;"><strong>Blacklist:</strong> No</span><br />\r\n<span style="font-size: small;"><strong>Country:</strong> USA</span><br />\r\n<span style="font-size: small;"><strong>Delivery:</strong> 3-10 Days {Average}</span><br />\r\n<span style="font-size: small;"><strong>Manufacture:</strong> Apple</span><br />\r\n<span style="font-size: small;"><strong>Model:</strong> 5C, 5S </span><br />\r\n<span style="font-size: small;"><strong>Network:</strong> AT&amp;T &nbsp;</span><br />\r\n<span style="font-size: small;"><strong>Notes:</strong> If IMEI is Not Found the Credits can not be refunded to PayPal, only to the users&#39; account.</span><br />\r\n<span style="font-size: small;">Many IMEI fail due to Active on Account or device not paid in full, we are not able to check this prior to submission.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style="font-size: medium; color: #ff0000;"><strong>IMPORTANT:</strong> Service is successful <strong>70%</strong> of the time even if meets criterias (No blacklisted &amp; not active on an account). if unsuccessful you can use the <strong>$99 (1-3 business days)</strong> option to unlock the device which is <strong>100% successful</strong> <span style="font-size: medium; color: #ff0000;"><strong>always</strong></span>.</span></p>\r\n', '2014-04-19', 1),
(4, 'AT & T iPhone (1-2 BUSINESS DAYS) - 4/4S/5/5S/5C 100% ALL IMEIS - 130 Credit', 1, '135', '1-2 Days', '<div class="info" id="information">\r\n<p>This service unlocks ALL AT&amp;T USA iPhone. 100% Success for this service.</p>\r\n<strong>Models</strong>: 3GS, 4, 4S, 5, 5S, 5C\r\n\r\n<p><strong>Supported</strong>: Lost, Stolen, Unpaid balance, Active on a line, Contract, Blacklist<br />\r\n<strong>Processing Days</strong>: Submit is Monday through Friday ONLY. Orders placed on Friday will be ready by Monday or Tuesday.<br />\r\n<strong>Service Timeframe</strong>: 1-2 WORKING days</p>\r\nPlease note this service doesn&#39;t remove iCloud logins.</div>\r\n', '2014-04-19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `service_order`
--

CREATE TABLE IF NOT EXISTS `service_order` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `order_id` int(255) NOT NULL,
  `service` int(20) DEFAULT NULL,
  `notes` text,
  `comments` text,
  `respond_email` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `service_order`
--

INSERT INTO `service_order` (`id`, `order_id`, `service`, `notes`, `comments`, `respond_email`, `date`, `status`) VALUES
(10, 1397904980, 4, 'ddd', 'werwerewrewr', 'contact@amsitsoft.com', '2014-04-19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `signup`
--

CREATE TABLE IF NOT EXISTS `signup` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `mobileno` varchar(24) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `photo` text NOT NULL,
  `date` date NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `signup`
--

INSERT INTO `signup` (`id`, `fullname`, `email`, `username`, `password`, `mobileno`, `dob`, `photo`, `date`, `status`) VALUES
(1, 'Mahamod', 'contyact@amsitsoft.com', 'mahamod', '123456', '23423434', '234324324324', '', '2014-03-19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE IF NOT EXISTS `subcategory` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `cid` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`id`, `name`, `cid`, `date`, `status`) VALUES
(4, 'Iphone 4 & 4s Parts', 10, '2014-04-18', 1),
(5, 'IPHONE 5 Parts', 10, '2014-04-19', 1),
(6, 'IPHONE 5c PARTS', 10, '2014-04-19', 1),
(7, 'IPHONE 5s PARTS', 10, '2014-04-19', 1),
(8, 'IPAD 1st Generation', 11, '2014-04-19', 1),
(9, 'IPAD 2nd GENERATION', 11, '2014-04-19', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

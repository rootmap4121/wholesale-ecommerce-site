-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2014 at 03:21 AM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `igeek`
--

-- --------------------------------------------------------

--
-- Table structure for table `barcode`
--

CREATE TABLE IF NOT EXISTS `barcode` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `barcode`
--

INSERT INTO `barcode` (`id`, `name`, `date`, `status`) VALUES
(1, 'Napa', '2014-03-07', 1),
(2, 'qqq', '2014-03-18', 1),
(3, 'asdsa', '2014-03-18', 1),
(4, 'dsfdsf', '2014-03-18', 1),
(5, '346356', '2014-03-18', 1),
(6, '346356', '2014-03-18', 1),
(7, '346356', '2014-03-18', 1),
(8, '', '2014-03-18', 1),
(9, '346356', '2014-03-18', 1),
(10, '346356', '2014-03-18', 1),
(11, '346356', '2014-03-18', 1),
(12, '346356', '2014-03-18', 1),
(13, '346356', '2014-03-18', 1),
(14, '346356', '2014-03-18', 1),
(15, '', '2014-03-18', 1),
(16, 'sdgdsfg', '2014-03-18', 1),
(17, '346356', '2014-03-18', 1),
(18, '346356', '2014-03-18', 1),
(19, 'yt', '2014-03-18', 1),
(20, 'sdfgsdfg', '2014-03-18', 1),
(21, '', '2014-03-18', 1),
(22, '564564', '2014-03-18', 1),
(23, '564564', '2014-03-18', 1),
(24, '564564', '2014-03-18', 1),
(25, '564564', '2014-03-18', 1),
(26, '564564', '2014-03-18', 1),
(27, '346356', '2014-03-19', 1),
(28, '', '2014-04-18', 1),
(29, '', '2014-04-18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `cart_id` int(20) DEFAULT NULL,
  `pid` int(20) DEFAULT NULL,
  `quantity` int(20) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `cart_id`, `pid`, `quantity`, `date`, `status`) VALUES
(1, 1398502956, 10, 1, NULL, 2),
(2, 1398502956, 9, 1, NULL, 2),
(3, 1398502956, 8, 1, NULL, 2),
(4, 1398544266, 10, 1, NULL, 2),
(5, 1398613506, 10, 1, NULL, 2),
(6, 1398764740, 10, 1, NULL, 2),
(7, 1398764740, 9, 1, NULL, 2),
(8, 1398923033, 10, 1, NULL, 2),
(9, 1398973143, 10, 2, NULL, 2),
(10, 1398973143, 9, 1, NULL, 2),
(11, 1398973169, 10, 1, NULL, 2),
(12, 1399031480, 10, 1, NULL, 2),
(13, 1399031480, 9, 1, NULL, 2),
(14, 1399053368, 10, 1, NULL, 2),
(15, 1399055884, 10, 1, NULL, 2),
(16, 1399057184, 10, 1, NULL, 1),
(17, 1399058801, 10, 1, NULL, 2),
(24, 1399062671, 10, 1, NULL, 1),
(25, 1399062671, 9, 1, NULL, 1),
(26, 1399062671, 8, 1, NULL, 1),
(27, 1399100129, 10, 1, NULL, 2),
(28, 1399137437, 13, 1, NULL, 2),
(29, 1399137437, 12, 1, NULL, 2),
(30, 1399147977, 13, 1, NULL, 2),
(31, 1399147977, 12, 1, NULL, 2),
(32, 1399410173, 12, 1, NULL, 2),
(33, 1399500174, 13, 1, NULL, 1),
(34, 1399500174, 12, 1, NULL, 1),
(35, 1399500174, 11, 1, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `date`, `status`) VALUES
(10, 'IPHONE PARTS', '2014-04-18', 1),
(11, 'IPAD PARTS', '2014-04-19', 1),
(12, 'SAMSUNG GALAXY PARTS', '2014-04-19', 1),
(13, 'UNLOCKING SERVICES', '2014-04-19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cupon_code`
--

CREATE TABLE IF NOT EXISTS `cupon_code` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) DEFAULT NULL,
  `amount` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `cupon_code`
--

INSERT INTO `cupon_code` (`id`, `code`, `amount`, `date`, `status`) VALUES
(2, '1234', 11, '2014-05-02', 2),
(4, 'fahad', 5, '2014-05-02', 2);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `talephone` varchar(25) DEFAULT NULL,
  `fax` varchar(30) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `company_id` varchar(55) DEFAULT NULL,
  `address1` text,
  `address2` text,
  `city` varchar(255) DEFAULT NULL,
  `postcode` varchar(20) DEFAULT NULL,
  `country` varchar(20) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `fname`, `lname`, `email`, `password`, `talephone`, `fax`, `company`, `company_id`, `address1`, `address2`, `city`, `postcode`, `country`, `state`, `date`, `status`) VALUES
(18, 'Md Mahamodur Zaman', 'Bhuyian', 'f.bhuyian@gmail.com', '022543a76e4e6a27bec6d96907429f04', '+8801927608261', NULL, 'AMS IT', 'AMS2371', '44/P,Kazi Bhavan,Zigatola New Road,Dhanmondi', '44/P,Kazi Bhavan,Zigatola New Road,Dhanmondi', 'Dhaka', '1209', 'United States', 'Arkansas', '2014-05-02', 2),
(19, 'Md Mahamodur Zaman', 'Bhuyian', 'mdmahamodurzaman@gmail.com', '022543a76e4e6a27bec6d96907429f04', '1927608261', NULL, 'ams', 'asd', 'adasd', 'asdasd', 'asdsad', 'amsit', 'United States', 'Arizona', '2014-05-08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `blood_group` varchar(20) NOT NULL,
  `dob` date DEFAULT NULL,
  `contactnumber` varchar(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `name`, `gender`, `blood_group`, `dob`, `contactnumber`, `address`, `username`, `password`, `date`, `status`) VALUES
(13, 'amsit', '1', 'A+', '2014-01-01', '1927608261', 'qq', 'amsit', '8139be3698a555383168ec998c0e65bf', '2014-02-09', 2),
(56, 'justin', '1', '', '1988-02-02', '+8888888888888888888', 'Null', 'justin', 'e10adc3949ba59abbe56e057f20f883e', '2014-04-18', 2);

-- --------------------------------------------------------

--
-- Table structure for table `employee_basic_info`
--

CREATE TABLE IF NOT EXISTS `employee_basic_info` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emplid` int(20) NOT NULL,
  `father_husbandname` varchar(255) DEFAULT NULL,
  `mothers_name` varchar(255) DEFAULT NULL,
  `blood_group` varchar(20) DEFAULT NULL,
  `marital_status` varchar(255) DEFAULT NULL,
  `marrige_date` date DEFAULT NULL,
  `religion` varchar(255) DEFAULT NULL,
  `home_district` varchar(255) DEFAULT NULL,
  `relative_in_sc` varchar(255) DEFAULT NULL,
  `relation` varchar(255) DEFAULT NULL,
  `tin_number` varchar(255) DEFAULT NULL,
  `driving_licence` varchar(255) DEFAULT NULL,
  `passport_number` varchar(255) DEFAULT NULL,
  `date_of_expire` date DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `employee_basic_info`
--

INSERT INTO `employee_basic_info` (`id`, `emplid`, `father_husbandname`, `mothers_name`, `blood_group`, `marital_status`, `marrige_date`, `religion`, `home_district`, `relative_in_sc`, `relation`, `tin_number`, `driving_licence`, `passport_number`, `date_of_expire`, `date`, `status`) VALUES
(1, 22, 'Rofiqur', 'Rokia', 'A+', 'Single', '2012-01-01', 'islam', 'Dhaka', 'Yes', 'Friend', 'BD32432432434', 'AP21321312321', 'CPA3432498324', '2020-01-01', NULL, NULL),
(11, 13, 'rofiqur', 'rokia', 'A+', 'Single', '0000-00-00', 'islam', 'Dhaka', 'Yes', 'Friend', '4455556666666666', '4444444444', '444444444444488', '0000-00-00', NULL, NULL),
(12, 53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employee_educational_info`
--

CREATE TABLE IF NOT EXISTS `employee_educational_info` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emplid` int(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `board` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `division` varchar(255) DEFAULT NULL,
  `gpa` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `employee_educational_info`
--

INSERT INTO `employee_educational_info` (`id`, `emplid`, `title`, `board`, `year`, `division`, `gpa`, `date`, `status`) VALUES
(11, 22, 'SSC', 'comilla', '2006', 'A', '4.25', NULL, NULL),
(12, 22, 'HSC', 'comilla', '2008', 'A+', '5.00', NULL, NULL),
(13, 22, 'Diploma', 'comilla', '2010', 'A', '3.75', NULL, NULL),
(20, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 53, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employee_emergency_contact`
--

CREATE TABLE IF NOT EXISTS `employee_emergency_contact` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emplid` int(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `relation` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `employee_emergency_contact`
--

INSERT INTO `employee_emergency_contact` (`id`, `emplid`, `name`, `address`, `phone`, `relation`, `date`, `status`) VALUES
(1, 22, 'Md Mahamod', 'address', 'phones', 'Me', NULL, 1),
(2, 22, 'Md Mahamod', 'address', 'phones', 'Me', NULL, 2),
(19, 13, NULL, NULL, NULL, NULL, NULL, 1),
(20, 13, NULL, NULL, NULL, NULL, NULL, 2),
(21, 53, NULL, NULL, NULL, NULL, NULL, 1),
(22, 53, NULL, NULL, NULL, NULL, NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `employee_present_address`
--

CREATE TABLE IF NOT EXISTS `employee_present_address` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emplid` int(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=279 ;

--
-- Dumping data for table `employee_present_address`
--

INSERT INTO `employee_present_address` (`id`, `emplid`, `address`, `phone`, `fax`, `date`, `status`) VALUES
(1, 22, 'dfsd', 'sds', 'dsdsd', NULL, 1),
(2, 22, 'dfsd', 'sds', 'dsdsd', NULL, 2),
(275, 13, NULL, NULL, NULL, NULL, 1),
(276, 13, NULL, NULL, NULL, NULL, 2),
(277, 53, NULL, NULL, NULL, NULL, 1),
(278, 53, NULL, NULL, NULL, NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `feature`
--

CREATE TABLE IF NOT EXISTS `feature` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `pid` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `feature`
--

INSERT INTO `feature` (`id`, `pid`, `date`, `status`) VALUES
(5, 14, NULL, NULL),
(6, 13, NULL, NULL),
(7, 11, NULL, NULL),
(8, 12, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `label`
--

CREATE TABLE IF NOT EXISTS `label` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` text,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `label`
--

INSERT INTO `label` (`id`, `name`, `date`, `status`) VALUES
(1, 'iPhone USA - Listed below in section "iPhones" are for Non-USA iPhones', '2014-04-18', 1),
(2, 'Checker', '2014-04-18', 1),
(3, 'USA Services', '2014-04-18', 1),
(4, 'HTC Services / Specials', '2014-04-18', 1),
(5, 'Samsung', '2014-04-18', 1),
(6, 'Blackberry Q5 / Q10 / Z10 / Z30', '2014-04-18', 1),
(7, 'Alcatel', '2014-04-18', 1),
(8, 'Huawei', '2014-04-18', 1),
(9, 'USA', '2014-04-18', 1),
(10, 'ESN Cleaning', '2014-04-18', 1),
(11, 'Blackberry', '2014-04-18', 1),
(12, 'Dell', '2014-04-18', 1),
(13, 'HTC', '2014-04-18', 1),
(14, 'iPhones', '2014-04-18', 1),
(15, 'LG', '2014-04-18', 1),
(16, 'Motorola', '2014-04-18', 1),
(17, 'Sidekick', '2014-04-18', 1),
(18, 'Sony Ericsson', '2014-04-18', 1),
(19, 'MSL / SPC (CDMA Phones)', '2014-04-18', 1),
(20, 'Manufacture (BB/HTC/LG/Motorola/Samsung ABOVE)', '2014-04-18', 1),
(21, 'CDMA / GSM Services', '2014-04-18', 1),
(22, 'Canada', '2014-04-18', 1),
(23, 'Old Services', '2014-04-18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `latest`
--

CREATE TABLE IF NOT EXISTS `latest` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `pid` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `loginfo`
--

CREATE TABLE IF NOT EXISTS `loginfo` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `detail` text,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=65 ;

--
-- Dumping data for table `loginfo`
--

INSERT INTO `loginfo` (`id`, `detail`, `date`, `status`) VALUES
(2, 'Customer Panel Logged out by Md Mahamodur Zaman', '2014-04-27', 1),
(3, 'Login Success in User Panel by f.bhuyian@gmail.com', '2014-04-27', 1),
(4, 'A Order Has Been Placed By Customer', '2014-04-27', 1),
(5, 'Customer Panel Logged out by Md Mahamodur Zaman', '2014-04-27', 1),
(6, 'Admin Panel Logged out by amsit', '2014-04-27', 1),
(7, 'Admin Panel Login by amsit', '2014-04-27', 1),
(8, 'Login Success in User Panel by f.bhuyian@gmail.com', '2014-04-29', 1),
(9, 'A Order Has Been Placed By Customer', '2014-04-29', 1),
(10, 'Admin Panel Login by amsit', '2014-04-29', 1),
(11, 'Admin Panel Login Wrong Try f.bhuyian@gmail.com', '2014-05-01', 1),
(12, 'Admin Panel Login by amsit', '2014-05-01', 1),
(13, 'Account is not Activated  for f.bhuyian@gmail.com', '2014-05-01', 1),
(14, 'Login Success in User Panel by f.bhuyian@gmail.com', '2014-05-01', 1),
(15, 'A Order Has Been Placed By Customer', '2014-05-01', 1),
(16, 'A Order Has Been Placed By Customer', '2014-05-01', 1),
(17, 'Customer Panel Logged out by Md Mahamodur Zaman', '2014-05-01', 1),
(18, 'Login Wrong in User Panel by f.bhuyian@gmail.com', '2014-05-01', 1),
(19, 'Account is not Activated  for mdmahamodurzaman@gmail.com', '2014-05-01', 1),
(20, 'Account is not Activated  for mdmahamodurzaman@gmail.com', '2014-05-01', 1),
(21, 'Account is not Activated  for f.bhuyian@gmail.com', '2014-05-01', 1),
(22, 'Account is not Activated  for f.bhuyian@gmail.com', '2014-05-01', 1),
(23, 'Account is not Activated  for f.bhuyian@gmail.com', '2014-05-01', 1),
(24, 'Login Success in User Panel by f.bhuyian@gmail.com', '2014-05-01', 1),
(25, 'A Order Has Been Placed By Customer', '2014-05-02', 1),
(26, 'Customer Panel Logged out by Md Mahamodur Zaman', '2014-05-02', 1),
(27, 'Login Success in User Panel by f.bhuyian@gmail.com', '2014-05-02', 1),
(28, 'Customer Panel Logged out by Md Mahamodur Zaman', '2014-05-02', 1),
(29, 'Login Success in User Panel by f.bhuyian@gmail.com', '2014-05-02', 1),
(30, 'A Order Has Been Placed By Customer', '2014-05-02', 1),
(31, 'A Order Has Been Placed By Customer', '2014-05-02', 1),
(32, 'Login Success in User Panel by f.bhuyian@gmail.com', '2014-05-02', 1),
(33, 'A Order Has Been Placed By Customer', '2014-05-02', 1),
(34, 'A Order Has Been Placed By Customer', '2014-05-02', 1),
(35, 'A Order Has Been Placed By Customer', '2014-05-02', 1),
(36, 'Login Success in User Panel by f.bhuyian@gmail.com', '2014-05-03', 1),
(37, 'A Order Has Been Placed By Customer', '2014-05-03', 1),
(38, 'Login Success in User Panel by f.bhuyian@gmail.com', '2014-05-03', 1),
(39, 'Admin Panel Login by amsit', '2014-05-03', 1),
(40, 'Login Success in User Panel by f.bhuyian@gmail.com', '2014-05-03', 1),
(41, 'Admin Panel Login by amsit', '2014-05-03', 1),
(42, 'A Order Has Been Placed By Customer', '2014-05-03', 1),
(43, 'A Order Has Been Placed By Customer', '2014-05-03', 1),
(44, 'A Order Has Been Placed By Customer', '2014-05-03', 1),
(45, 'A Order Has Been Placed By Customer', '2014-05-03', 1),
(46, 'A Order Has Been Placed By Customer', '2014-05-03', 1),
(47, 'A Order Has Been Placed By Customer', '2014-05-03', 1),
(48, 'A Order Has Been Placed By Customer', '2014-05-03', 1),
(49, 'A Order Has Been Placed By Customer', '2014-05-03', 1),
(50, 'A Order Has Been Placed By Customer', '2014-05-03', 1),
(51, 'A Order Has Been Placed By Customer', '2014-05-03', 1),
(52, 'A Order Has Been Placed By Customer', '2014-05-03', 1),
(53, 'A Order Has Been Placed By Customer', '2014-05-03', 1),
(54, 'A Order Has Been Placed By Customer', '2014-05-03', 1),
(55, 'A Order Has Been Placed By Customer', '2014-05-03', 1),
(56, 'Login Success in User Panel by f.bhuyian@gmail.com', '2014-05-04', 1),
(57, 'Admin Panel Login by amsit', '2014-05-05', 1),
(58, 'Admin Panel Login by amsit', '2014-05-07', 1),
(59, 'Login Success in User Panel by f.bhuyian@gmail.com', '2014-05-07', 1),
(60, 'A Order Has Been Placed By Customer', '2014-05-07', 1),
(61, 'A Order Has Been Placed By Customer', '2014-05-07', 1),
(62, 'Login Success in User Panel by f.bhuyian@gmail.com', '2014-05-08', 1),
(63, 'Admin Panel Login by amsit', '2014-05-08', 1),
(64, 'Customer Panel Logged out by Md Mahamodur Zaman', '2014-05-08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `order_discount`
--

CREATE TABLE IF NOT EXISTS `order_discount` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `cart_id` int(20) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `discount_amount` varchar(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `order_discount`
--

INSERT INTO `order_discount` (`id`, `cart_id`, `discount`, `discount_amount`, `date`, `status`) VALUES
(1, 1398502956, 'Cupon Code Discount', '10', '2014-04-26', 1),
(2, 1399053368, 'Cupon Code Discount', '10', '2014-05-02', 1),
(3, 1399055884, 'Cupon Code Discount', '10', '2014-05-02', 1),
(4, 1399057184, 'Cupon Code Discount', '10', '2014-05-02', 1),
(5, 1399058801, 'Cupon Code Discount', '10', '2014-05-02', 1);

-- --------------------------------------------------------

--
-- Table structure for table `order_shipping`
--

CREATE TABLE IF NOT EXISTS `order_shipping` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `cart_id` int(20) DEFAULT NULL,
  `shipping` varchar(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `order_shipping`
--

INSERT INTO `order_shipping` (`id`, `cart_id`, `shipping`, `date`, `status`) VALUES
(1, 1399058801, '5', NULL, NULL),
(2, 1399062671, '6', NULL, NULL),
(3, 1399100129, '7', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `photo` text NOT NULL,
  `photo2` varchar(255) NOT NULL,
  `photo3` varchar(255) NOT NULL,
  `photo4` varchar(255) NOT NULL,
  `slider` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `emplid` int(20) DEFAULT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` varchar(20) DEFAULT NULL,
  `cid` int(20) DEFAULT NULL,
  `scid` int(20) DEFAULT NULL,
  `reorder` int(20) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `photo`, `photo2`, `photo3`, `photo4`, `slider`, `description`, `emplid`, `quantity`, `price`, `cid`, `scid`, `reorder`, `date`, `status`) VALUES
(7, 'Iphone Parts', '90X90_1397822717_loock.png', '160X160_1397822717_loock.png', '170X170_1397822717_loock.png', '300X300_1397822717_loock.png', '', '<p>zcxzxzc</p>\r\n', 56, 2, '200', 10, 4, 1, '2014-04-18', 1),
(8, 'Iphone 4 & 4s Diagnosis', '90X90_1397913693_diagnostic.png', '160X160_1397913693_diagnostic.png', '170X170_1397913693_diagnostic.png', '300X300_1397913693_diagnostic.png', '', '<p>Iphone 4 &amp; 4s Diagnosis</p>\r\n', 56, 1, '00', 10, 4, 1, '2014-04-19', 1),
(9, 'Led Replacement', '90X90_1397916370_glass.png', '160X160_1397916370_glass.png', '170X170_1397916370_glass.png', '300X300_1397916370_glass.png', '', '<p>Led Replacement</p>\r\n', 56, 1, '79', 10, 4, 1, '2014-04-19', 1),
(10, 'Phone Back Cover', '90X90_1397923033_back.png', '160X160_1397923033_back.png', '170X170_1397923033_back.png', '300X300_1397923033_back.png', '', '<p>description :<br />\r\n&nbsp;</p>\r\n', 56, 2, '200', 10, 4, 1, '2014-04-19', 1),
(11, 'LCD Specials', '90X90_1399104961_Fly E120 1 copy-145x145.png', '160X160_1399104961_Fly E120 1 copy-145x145.png', '170X170_1399104961_Fly E120 1 copy-145x145.png', '300X300_1399104961_Fly E120 1 copy-145x145.png', '300X300_1399103189_slide-4-785x421.png', '<p>iPhone 5 LCD &amp; Digitizer Combo OEM Quality Parts, Fully Tested, &amp; guaranteed.</p>\r\n', 13, 6, '60', 10, 5, 1, '2014-05-03', 1),
(12, 'LCD Specials', '90X90_1399104953_Anycool D58 1 copy-300x300.png', '160X160_1399104953_Anycool D58 1 copy-300x300.png', '170X170_1399104953_Anycool D58 1 copy-300x300.png', '300X300_1399104953_Anycool D58 1 copy-300x300.png', '300X300_1399104121_slide-1-785x421.png', '<p>iPhone 5s LCD &amp; Digitizer Combo OEM Quality Parts, Fully Tested, &amp; guaranteed.</p>\r\n', 13, 6, '60', 10, 7, 1, '2014-05-03', 1),
(13, 'GSM Unlocks!', '90X90_1399104942_Fly HUMMER HT2 1 copy-160x160.png', '160X160_1399104942_Fly HUMMER HT2 1 copy-160x160.png', '170X170_1399104942_Fly HUMMER HT2 1 copy-160x160.png', '300X300_1399104942_Fly HUMMER HT2 1 copy-160x160.png', '300X300_1399103477_slide-2-785x421.png', '<p>iPhone Factory Unlocks, Android Factory Unlocks Same Day Service! Guaranteed Results</p>\r\n', 13, 6, '60', 13, 10, 1, '2014-05-03', 1),
(14, 'Free Diagnosis', '90X90_1399104930_FLY E145 1 copy-160x160.png', '160X160_1399104930_FLY E145 1 copy-160x160.png', '170X170_1399104930_FLY E145 1 copy-160x160.png', '300X300_1399104930_FLY E145 1 copy-160x160.png', '300X300_1399104086_HTC Desire 1 copy-160x160.png', '<h2>We bring it to you</h2>\r\n\r\n<p>Have a local Repair Store, in the Detroit Metro Area? Set up an account, place an order, &amp; we will deliver it to you! Same day!</p>\r\n', 13, 6, '00', 12, 11, 1, '2014-05-03', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_order`
--

CREATE TABLE IF NOT EXISTS `product_order` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `cusid` int(20) NOT NULL,
  `payment_method` int(20) NOT NULL,
  `payment_method_comment` text NOT NULL,
  `shipping_cost` varchar(20) NOT NULL,
  `shipping_id` int(20) NOT NULL,
  `shipping_address` text NOT NULL,
  `discount` varchar(255) NOT NULL,
  `discount_amount` varchar(20) NOT NULL,
  `cart_id` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `payment_status` int(20) NOT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `product_order`
--

INSERT INTO `product_order` (`id`, `cusid`, `payment_method`, `payment_method_comment`, `shipping_cost`, `shipping_id`, `shipping_address`, `discount`, `discount_amount`, `cart_id`, `date`, `payment_status`, `status`) VALUES
(1, 18, 2, '', '95', 3, 'Md Mahabur,Rahman,undefined,Dhaka,1209,United States,Arizona', '', '', 1399147977, '2014-05-03', 1, 1),
(2, 18, 2, 'addasdasdsa', '95', 3, 'Md Mahabur,Rahman,undefined,Dhaka,1209,United States,Arizona', '', '', 1399410173, '2014-05-07', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE IF NOT EXISTS `service` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` text,
  `label` int(20) DEFAULT NULL,
  `price` varchar(20) DEFAULT NULL,
  `delivery_time` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id`, `name`, `label`, `price`, `delivery_time`, `description`, `date`, `status`) VALUES
(1, 'AT&T iPhone (3-10 days) - 3G/3GS/4/4S/5 CLEAN IMEIS 70-80% FOUND - 40 Credit', 1, '40', '3-10 Days', '<div class="info" id="information">\r\n<p>This service will unlock AT&amp;T iPhones that are : 2G, 3G, 3GS, 4, 4S, 5.</p>\r\n&nbsp;\r\n\r\n<p>Orders that are completed are between 70-80%.</p>\r\n&nbsp;\r\n\r\n<p>Reasons the devices can fail are due to: Phone is in contract, active on a line, reported lost/stolen or has an unpaid balance. Some of these orders may still go through if meet the criterias of in contract or active on a line.</p>\r\n&nbsp;\r\n\r\n<p>Speed of service is 3-10 days.</p>\r\n</div>\r\n', '2014-04-19', 1),
(2, 'AT & T iPhone SPECIAL (3-10 days) - 2G/3G/3GS/4 ONLY (NO 4S/5) 70-80% FOUND - 25 Credit', 1, '30', '3-10 Days', '<div class="info" id="information">\r\n<p>This service will unlock AT&amp;T iPhones that are : 2G, 3G, 3GS, 4</p>\r\n&nbsp;\r\n\r\n<p><span style="font-size: medium; color: #ff0000;"><strong>ANY IPHONE 4S / 5 / 5S / 5C ORDERED WILL BE REJECTED</strong></span></p>\r\n&nbsp;\r\n\r\n<p>Orders that are completed are between 70-80%.</p>\r\n&nbsp;\r\n\r\n<p>Reasons the devices can fail are due to: Phone is in contract, active on a line, reported lost/stolen or has an unpaid balance. Some of these orders may still go through if meet the criterias of in contract or active on a line.</p>\r\n&nbsp;\r\n\r\n<p>Speed of service is 3-10 days.</p>\r\n</div>\r\n', '2014-04-19', 1),
(3, 'AT & T iPhone (3-10 days) - 5C/5S CLEAN IMEIS 60-80% FOUND - 39 Credit', 1, '44', '3-10 Days', '<p><span style="font-size: small;"><strong>Active:</strong> No (Phone can not be Active on an Account)</span><br />\r\n<span style="font-size: small;"><strong>Activated:</strong> Yes</span><br />\r\n<span style="font-size: small;"><strong>Blacklist:</strong> No</span><br />\r\n<span style="font-size: small;"><strong>Country:</strong> USA</span><br />\r\n<span style="font-size: small;"><strong>Delivery:</strong> 3-10 Days {Average}</span><br />\r\n<span style="font-size: small;"><strong>Manufacture:</strong> Apple</span><br />\r\n<span style="font-size: small;"><strong>Model:</strong> 5C, 5S </span><br />\r\n<span style="font-size: small;"><strong>Network:</strong> AT&amp;T &nbsp;</span><br />\r\n<span style="font-size: small;"><strong>Notes:</strong> If IMEI is Not Found the Credits can not be refunded to PayPal, only to the users&#39; account.</span><br />\r\n<span style="font-size: small;">Many IMEI fail due to Active on Account or device not paid in full, we are not able to check this prior to submission.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style="font-size: medium; color: #ff0000;"><strong>IMPORTANT:</strong> Service is successful <strong>70%</strong> of the time even if meets criterias (No blacklisted &amp; not active on an account). if unsuccessful you can use the <strong>$99 (1-3 business days)</strong> option to unlock the device which is <strong>100% successful</strong> <span style="font-size: medium; color: #ff0000;"><strong>always</strong></span>.</span></p>\r\n', '2014-04-19', 1),
(4, 'AT & T iPhone (1-2 BUSINESS DAYS) - 4/4S/5/5S/5C 100% ALL IMEIS - 130 Credit', 1, '135', '1-2 Days', '<div class="info" id="information">\r\n<p>This service unlocks ALL AT&amp;T USA iPhone. 100% Success for this service.</p>\r\n<strong>Models</strong>: 3GS, 4, 4S, 5, 5S, 5C\r\n\r\n<p><strong>Supported</strong>: Lost, Stolen, Unpaid balance, Active on a line, Contract, Blacklist<br />\r\n<strong>Processing Days</strong>: Submit is Monday through Friday ONLY. Orders placed on Friday will be ready by Monday or Tuesday.<br />\r\n<strong>Service Timeframe</strong>: 1-2 WORKING days</p>\r\nPlease note this service doesn&#39;t remove iCloud logins.</div>\r\n', '2014-04-19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `service_order`
--

CREATE TABLE IF NOT EXISTS `service_order` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `order_id` int(255) NOT NULL,
  `service` int(20) DEFAULT NULL,
  `notes` text,
  `comments` text,
  `respond_email` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `service_order`
--

INSERT INTO `service_order` (`id`, `order_id`, `service`, `notes`, `comments`, `respond_email`, `date`, `status`) VALUES
(10, 1397904980, 4, 'ddd', 'werwerewrewr', 'contact@amsitsoft.com', '2014-04-19', 1),
(11, 1398072477, 3, 'ddd', 'dfasadsadsa', 'contact@amsitsoft.com', '2014-04-21', 1),
(12, 1398929444, 3, 'ddd', 'adsdsadasd', 'contact@amsitsoft.com', '2014-05-01', 2),
(13, 1398929444, 3, 'ddd', 'adsdsadasd', 'contact@amsitsoft.com', '2014-05-01', 2),
(14, 1399509532, 1, 'asdsadghsa', 'hjhasdas', 'f.bhuyian@gmail.com', '2014-05-08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shipping_address`
--

CREATE TABLE IF NOT EXISTS `shipping_address` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `cusid` int(20) DEFAULT NULL,
  `address` text,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `shipping_address`
--

INSERT INTO `shipping_address` (`id`, `cusid`, `address`, `status`) VALUES
(1, 18, 'Md Mahamodur Zaman,Bhuyian,44/P,Kazi Bhavan,Zigatola New Road,Dhanmondi,Dhaka,1209,United States,Arkansas', 1),
(2, 18, 'Fahad,Bhuyian,undefined,Dhaka,1209,United States,Alaska', 1),
(3, 18, 'Md Mahabur,Rahman,undefined,Dhaka,1209,United States,Arizona', 1),
(4, 19, 'Md Mahamodur Zaman,Bhuyian,adasd,asdsad,amsit,United States,Arizona', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shipping_cost`
--

CREATE TABLE IF NOT EXISTS `shipping_cost` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `country` varchar(20) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `cost` varchar(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `shipping_cost`
--

INSERT INTO `shipping_cost` (`id`, `country`, `state`, `cost`, `date`, `status`) VALUES
(1, 'United States', 'Alabama', '4', '2014-05-02', 1),
(2, 'United States', 'Alaska', '6', '2014-05-02', 1),
(3, 'United States', 'American Samoa', '7', '2014-05-02', 1),
(4, 'United States', 'Arizona', '10', '2014-05-02', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shipping_method`
--

CREATE TABLE IF NOT EXISTS `shipping_method` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `shipping_method`
--

INSERT INTO `shipping_method` (`id`, `name`, `date`, `status`) VALUES
(1, 'Fedex Weekday Delivery', '2014-05-03', 1),
(2, 'Saturday Express Delivery', '2014-05-03', 1),
(3, 'United States Postal Service', '2014-05-03', 1),
(5, 'Will Call', '2014-05-03', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shipping_method_option`
--

CREATE TABLE IF NOT EXISTS `shipping_method_option` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `smid` int(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `cost` varchar(25) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `shipping_method_option`
--

INSERT INTO `shipping_method_option` (`id`, `smid`, `name`, `cost`, `date`, `status`) VALUES
(1, 1, 'Ground', '12.21', '2014-05-03', 1),
(2, 1, 'Express Saver', '15.17', '2014-05-03', 1),
(3, 2, 'Fedex Express Saturday Delivery', '95', '2014-05-03', 1),
(4, 3, 'First-Class Mail Parcel', '2.53', '2014-05-03', 1),
(5, 5, 'Local Pickup by Appointment', '0', '2014-05-03', 1);

-- --------------------------------------------------------

--
-- Table structure for table `signup`
--

CREATE TABLE IF NOT EXISTS `signup` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `mobileno` varchar(24) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `photo` text NOT NULL,
  `date` date NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `signup`
--

INSERT INTO `signup` (`id`, `fullname`, `email`, `username`, `password`, `mobileno`, `dob`, `photo`, `date`, `status`) VALUES
(1, 'Mahamod', 'contyact@amsitsoft.com', 'mahamod', '123456', '23423434', '234324324324', '', '2014-03-19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `pid` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `pid`, `date`, `status`) VALUES
(1, 14, NULL, NULL),
(2, 13, NULL, NULL),
(3, 12, NULL, NULL),
(4, 11, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `special`
--

CREATE TABLE IF NOT EXISTS `special` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `pid` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `special`
--

INSERT INTO `special` (`id`, `pid`, `date`, `status`) VALUES
(1, 14, NULL, NULL),
(2, 13, NULL, NULL),
(3, 12, NULL, NULL),
(4, 11, NULL, NULL),
(5, 10, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE IF NOT EXISTS `subcategory` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `cid` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`id`, `name`, `cid`, `date`, `status`) VALUES
(4, 'Iphone 4 & 4s Parts', 10, '2014-04-18', 1),
(5, 'IPHONE 5 Parts', 10, '2014-04-19', 1),
(6, 'IPHONE 5c PARTS', 10, '2014-04-19', 1),
(7, 'IPHONE 5s PARTS', 10, '2014-04-19', 1),
(8, 'IPAD 1st Generation', 11, '2014-04-19', 1),
(9, 'IPAD 2nd GENERATION', 11, '2014-04-19', 1),
(10, 'Factory Unlocks', 13, '2014-05-03', 1),
(11, 'Free Diagnosis', 12, '2014-05-03', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

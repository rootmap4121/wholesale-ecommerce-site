<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Product Order Info</a></li><li class='active'>Product Order Detail</li>";
$table="service_order";
if (isset ($_GET['del'])=="order") {
                    $delarray=array("id"=>$_GET['id'],"status"=>2);
                    if($obj->update($table,$delarray)==1)
                    { 
                        $errmsg_arr[]= 'Successfully Updated';
                        $errflag = true;
                        if ($errflag) 
                        {
                            $_SESSION['SMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    } 
                    else 
                    { 
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->          

                                <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                    <div class="widget-box transparent invoice-box">
                            <div class="widget-header widget-header-large">
                                    <h3 class="grey lighter pull-left position-relative">
                                            <i class="icon-leaf green"></i>
                                            Product Order Detail
                                    </h3>



                                    <div class="widget-toolbar hidden-480">
                                        
                                        <button  onclick="javascript:printDiv('printablediv')" class="btn btn-info" type="button" name="submit"><i class="icon-print bigger-110"></i>Print</button>
                                            
                                    </div>
                            </div>

                        <div class="widget-body" id="printablediv">
                                    <div class="widget-main padding-24">
                                            <div class="row">
                                                    <div class="col-sm-5">
                                                            <div class="row">
                                                                    <div class="col-xs-11 label label-lg label-info arrowed-in arrowed-right">
                                                                            <b>Invoice Info</b>
                                                                    </div>
                                                            </div>

                                                            <div class="row">
                                                                    <ul class="list-unstyled spaced">
                                                                            <li><i class="icon-caret-right blue"></i>
                                                                               <span class="invoice-info-label">Invoice:</span>
                                                                                <span class="red">#<?php echo $obj->SelectAllByVal("product_order","id",$_GET['id'],"cart_id"); ?> </span>

                                                                            </li>
                                                                            <li>
                                                                                       <i class="icon-caret-right blue"></i>
                                                                                <span class="invoice-date-label">Date:</span>
                                                                                <span class="blue"><?php echo $obj->SelectAllByVal("product_order","id",$_GET['id'],"date"); ?></span>
                                                                            </li>
                                                                            <li>
                                                                                    <i class="icon-caret-right blue"></i>
                                                                                    
                                                                                    Order Status : <?php echo $obj->order_status($obj->SelectAllByVal("product_order","id",$_GET['id'],"status")); ?>
                                                                            </li>
                                                                            <li>
                                                                                    <i class="icon-caret-right blue"></i>
                                                                                    
                                                                                    Payment Status : <?php echo $obj->order_status($obj->SelectAllByVal("product_order","id",$_GET['id'],"payment_status")); ?>
                                                                            </li>
                                                                            <li>
                                                                                    <i class="icon-caret-right blue"></i>
                                                                                    
                                                                                    Comment : <?php echo $obj->SelectAllByVal("product_order","id",$_GET['id'],"payment_method_comment"); ?>
                                                                            </li>
                                                                    </ul>
                                                            </div>
                                                    </div><!-- /span -->

                                                    <div class="col-sm-7">
                                                            <div class="row">
                                                                    <div class="col-xs-11 label label-lg label-success arrowed-in arrowed-right">
                                                                            <b>Customer Info</b>
                                                                    </div>
                                                            </div>

                                                            <div>
                                                                    <ul class="list-unstyled  spaced">
                                                                            <li>
                                                                                    <i class="icon-caret-right green"></i>
                                                                                    Name : <?php 
                                                                                    $cusid=$obj->SelectAllByVal("product_order","id",$_GET['id'],"cusid");
                                                                                    $cartid=$obj->SelectAllByVal("product_order","id",$_GET['id'],"cart_id");
                                                                                    echo $obj->SelectAllByVal("customer","id",$cusid,"fname")." ".$obj->SelectAllByVal("customer","id",$cusid,"lname");
                                                                                    ?>
                                                                            </li>
                                                                            
                                                                            <li>
                                                                                    <i class="icon-caret-right green"></i>
                                                                                    Email : <?php echo $obj->SelectAllByVal("customer","id",$cusid,"email");  ?>
                                                                            </li>
                                                                            
                                                                            <li>
                                                                                    <i class="icon-caret-right green"></i>
                                                                                    Shipping Address : <?php echo $obj->SelectAllByVal("shipping_address","cusid",$cusid,"address");  ?>
                                                                            </li>
                                                                            <li>
                                                                                    <i class="icon-caret-right green"></i>
                                                                                    Shipping Cost : <?php $shid=$obj->SelectAllByVal("product_order","id",$_GET['id'],"shipping_id"); echo $obj->SelectAllByVal("shipping_method_option","id",$shid,"name");  ?> $<?php echo $obj->SelectAllByVal("product_order","id",$_GET['id'],"shipping_cost");  ?>
                                                                            </li>
                                                                    </ul>
                                                            </div>
                                                    </div><!-- /span -->
                                            </div><!-- row -->

                                            <div class="space"></div>

                                            <div>
                                                    <table class="table table-striped">
                                                            <thead>
                                                                    <tr>
                                                                            <th class="center">#</th>
                                                                            <th>Product</th>
                                                                            <th class="hidden-xs">Category</th>
                                                                            <th class="hidden-480">Quantity</th>
                                                                            <th class="hidden-480">Unite Price</th>
                                                                            <th class="hidden-480">Product Total</th>
                                                                    </tr>
                                                            </thead>

                                                            <tbody>
                                                            <?php 
                                                            $i=1; 
                                                            $ord=$obj->SelectAllByID("cart",array("cart_id"=>$cartid)); 
                                                            $totquantity=0;
                                                            $totuniteprice=0;
                                                            $subtotal=0;
                                                            if(!empty($ord))
                                                            foreach($ord as $order):
                                                                $pid=$order->pid;
                                                            ?>
                                                             <tr>
                                                                     <td class="center"><?php echo $i; ?></td>
                                                                     <td>
                                                                         <?php $cid=$obj->SelectAllByVal("product","id",$pid,"cid"); echo $obj->SelectAllByVal("product","id",$pid,"name"); ?>
                                                                     </td>
                                                                     <td class="hidden-xs"><?php echo $obj->SelectAllByVal("category","id",$cid,"name"); ?></td>
                                                                     <td class="hidden-480"><?php echo $order->quantity; $quantity=$order->quantity; ?></td>
                                                                     <td class="hidden-480">$<?php echo $obj->SelectAllByVal("product","id",$pid,"price"); $price=$obj->SelectAllByVal("product","id",$pid,"price"); ?></td>
                                                                     <td class="hidden-480">$<?php echo $quantity*$price; 
                                                                     $totquantity+=$quantity;
                                                                    $totuniteprice+=$price;
                                                                    $subtotal+=$quantity*$price;
                                                                     
                                                                     
                                                                     ?></td>

                                                             </tr>
                                                             <?php 
                                                             $i++; 
                                                             endforeach; 
                                                             ?>
                                                             <tr>
                                                                 <td class="center" colspan="3"></td>
                                                                     <td class="hidden-480"><?php echo $totquantity; ?></td>
                                                                     <td class="hidden-480">$<?php echo $totuniteprice; ?></td>
                                                                     <td class="hidden-480">$<?php echo $subtotal; ?></td>

                                                             </tr>
                                                             <tr>
                                                                 <td class="center" colspan="4"></td>
                                                                 <td class="hidden-480">Sub-Total</td>
                                                                 <td class="hidden-480"><strong>$<?php echo $subtotal; ?></strong></td>

                                                             </tr>
                                                             <?php
                                                             $sh=$obj->SelectAllByVal("product_order","id",$_GET['id'],"shipping_cost");
                                                             if($sh!=0)
                                                             {
                                                             ?>
                                                             <tr>
                                                                 <td class="center" colspan="4"></td>
                                                                 <td class="hidden-480"><?php echo $obj->SelectAllByVal("shipping_method_option","id",$shid,"name"); ?></td>
                                                                 <td class="hidden-480"><strong>$<?php echo $obj->SelectAllByVal("product_order","id",$_GET['id'],"shipping_cost"); $shipping_cost=$obj->SelectAllByVal("product_order","id",$_GET['id'],"shipping_cost"); ?></strong></td>

                                                             </tr>
                                                             <?php
                                                             }
                                                             $dis=$obj->SelectAllByVal("order_discount","cart_id",$cartid,"discount_amount");
                                                             if($dis=='')
                                                             { $discount=0; }
                                                                else 
                                                                {
                                                                 
                                                             ?>
                                                             <tr>
                                                                 <td class="center" colspan="4"></td>
                                                                 <td class="hidden-480">Discount</td>
                                                                 <td class="hidden-480"><strong>$<?php echo $obj->SelectAllByVal("order_discount","cart_id",$cartid,"discount_amount"); $discount=$obj->SelectAllByVal("order_discount","cart_id",$cartid,"discount_amount"); ?></strong></td>

                                                             </tr>
                                                                <?php } ?>
                                                             
                                                             <tr>
                                                                 <td class="center" colspan="4"></td>
                                                                 <td class="hidden-480"><strong>Total Cost</strong></td>
                                                                 <td class="hidden-480"><strong>$<?php $totalcost=($subtotal+$shipping_cost)-$discount; echo $totalcost; ?></strong></td>

                                                             </tr>
                                                            </tbody>
                                                    </table>
                                            </div>

                                            <div class="hr hr8 hr-single hr-single"></div>

                                            <div class="row">
                                                    <!--<div class="col-sm-5 pull-right">
                                                            <h4 class="pull-right">
                                                                    Total amount :
                                                                    <span class="red">$395</span>
                                                            </h4>
                                                    </div>-->
                                            </div>

                                              
                                        
                                     &nbsp; &nbsp; &nbsp; 
                                        
                                            <div class="hr hr8 hr-single hr-single"></div>
                                    </div>
                            </div>
                    </div>
            </div>
    </div>
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                 <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				var oTable1 = $('#sample-table-2').dataTable( {
				"aoColumns": [
			      { "bSortable": false },
			      null, null,null, null, null,
				  { "bSortable": false }
				] } );
				
				
				$('table th input:checkbox').on('click' , function(){
					var that = this;
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
						
				});
			
			
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
			})
                        
		</script>
    </body>
</html>

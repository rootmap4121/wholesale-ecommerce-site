<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Service Info</a></li><li class='active'>Service Order List</li>";
$table="service_order";
if (isset ($_GET['del'])=="order") {
                    $delarray=array("id"=>$_GET['id'],"status"=>2);
                    if($obj->update($table,$delarray)==1)
                    { 
                        $errmsg_arr[]= 'Successfully Updated';
                        $errflag = true;
                        if ($errflag) 
                        {
                            $_SESSION['SMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    } 
                    else 
                    { 
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->          

                                <div class="row">

                                    <div class="col-xs-12">
                                        <h3 class="header smaller lighter blue">Service Order Detail : Order ID - <?php echo $obj->SelectAllByVal("service_order","id",$_GET['id'],"order_id"); ?></h3>


                                        <div class="table-responsive">
                                            <table style="width: 70%;" class="table table-striped table-bordered table-hover dataTable">
                                                <tbody>
                                                    <tr>
                                                        <th width='200'>Order ID</th>
                                                        <td><?php echo $obj->SelectAllByVal("service_order","id",$_GET['id'],"order_id"); ?></td>  
                                                    </tr>
                                                    <tr>
                                                        <th>Service Name</th>  
                                                        <td><?php 
                                                        $service_id=$obj->SelectAllByVal("service_order","id",$_GET['id'],"service");
                                                        echo $obj->SelectAllByVal("service","id",$service_id,"name");
                                                        ?></td>
                                                    </tr>
                                                    <tr> 
                                                        <th>Price</th>
                                                        <td>$<?php 
                                                        echo $obj->SelectAllByVal("service","id",$service_id,"price");
                                                        ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Delivery Time</th>
                                                        <td><?php  echo $obj->SelectAllByVal("service","id",$service_id,"delivery_time"); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th height='100'>Comments </th>
                                                        <td><?php echo $obj->SelectAllByVal("service_order","id",$_GET['id'],"comments"); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Notes </th>
                                                        <td><?php echo $obj->SelectAllByVal("service_order","id",$_GET['id'],"notes"); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Respond Email</th>
                                                        <td><?php echo $obj->SelectAllByVal("service_order","id",$_GET['id'],"respond_email"); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Status</th>
                                                        <td><?php echo $obj->order_status($obj->SelectAllByVal("service_order","id",$_GET['id'],"status")); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Order Date</th>
                                                        <td><?php echo $obj->SelectAllByVal("service_order","id",$_GET['id'],"date"); ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                 <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				var oTable1 = $('#sample-table-2').dataTable( {
				"aoColumns": [
			      { "bSortable": false },
			      null, null,null, null, null,
				  { "bSortable": false }
				] } );
				
				
				$('table th input:checkbox').on('click' , function(){
					var that = this;
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
						
				});
			
			
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
			})
                        
		</script>
    </body>
</html>

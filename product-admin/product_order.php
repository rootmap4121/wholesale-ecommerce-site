<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Product Order Info</a></li><li class='active'>Product Order List</li>";
$table="product_order";
if (isset ($_GET['del'])=="order") {
                    $delarray=array("id"=>$_GET['id'],"status"=>2);
                    if($obj->update($table,$delarray)==1)
                    { 
                        $errmsg_arr[]= 'Successfully Updated';
                        $errflag = true;
                        if ($errflag) 
                        {
                            $_SESSION['SMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    } 
                    else 
                    { 
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->          

                                <div class="row">

                                    <div class="col-xs-12">
                                        <h3 class="header smaller lighter blue">Product Order List</h3>


                                        <div class="table-responsive">
                                            <table aria-describedby="sample-table-2_info" id="sample-table-2" class="table table-striped table-bordered table-hover dataTable">
                                                <thead>
                                                    <tr>
                                                        <th class="center">S/N</th>
                                                        <th>Order ID</th>
                                                        
                                                        
                                                        <th>Customer ID</th>
                                                        <th>Email</th>
                                                        <th>Payment Method</th>
                                                        <th>Payment Status</th>
                                                        <th>Order Date</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="status">
                                                <?php
                                                if($obj->totalrows($table)!=0)
                                                {
                                                $data=$obj->SelectAllorderBy($table);
                                                $x=1;
                                                foreach ($data as $row): ?>
                                                        <tr>
                                                            <td class="center"><?php echo $x; ?></td>
                                                            <td><a  class="label label-sm label-success" href="product_order_detail.php?id=<?php echo $row->id; ?>"><?php echo $row->cart_id; ?></a></td>
                                                            <td><a href="customer.php?cusid=<?php echo $row->cusid; ?>"><?php echo $obj->SelectAllByVal("customer","id",$row->cusid,"fname"); ?></a></td>
                                                            <td><span class="label label-sm label-info"><?php echo $obj->SelectAllByVal("customer","id",$row->cusid,"email"); ?></span></td>
                                                            <td><span class="label label-sm label-danger"><?php echo $obj->method($row->payment_method); ?></span></td>
                                                            <td>
                                                                <a <?php if($row->status==0){ ?>  class="label label-sm label-danger" <?php } ?> <?php if($row->status==1){ ?>  class="label label-sm label-danger" <?php } ?> <?php if($row->status==2){ ?>  class="label label-sm label-success" <?php } ?> href="<?php echo $obj->filename(); ?>?del=order&AMP;id=<?php echo $row->id; ?>"><?php echo  $obj->order_status($row->status); ?></a>
                                                            </td>
                                                            <td><?php echo $row->date; ?></td>
                                                        </tr>
                                                 <?php 
                                                 $x++; endforeach; 
                                                }
                                                 ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                 <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				var oTable1 = $('#sample-table-2').dataTable( {
				"aoColumns": [
			      { "bSortable": false },
			      null, null,null, null, null,
				  { "bSortable": false }
				] } );
				
				
				$('table th input:checkbox').on('click' , function(){
					var that = this;
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
						
				});
			
			
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
			})
                        
		</script>
    </body>
</html>

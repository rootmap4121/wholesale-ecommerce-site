<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Product Info</a></li><li class='active'>Add New Product</li>";
$table="product";
$w=90; $h=90; $thumb="../product/";
$ww=160; $hh=160; $thumbb="../product/";
$www=170; $hhh=170; $thumbbb="../product/";
$wwww=300; $hhhh=300; $thumbbbb="../product/";
if(isset($_POST['submit']))
{
    if(!empty($_POST['name']))
    {
        $exist_array=array("name"=>$_POST['name'],"cid"=>$_POST['cid'],"scid"=>$_POST['scid']);
        
        if(!empty($_FILES['image']['name']))
        {
            $files =$obj->image_bigcaption($w,$h,$thumb);
            $photo=substr($files,11,1600);
            
            $filess =$obj->image_bigcaption2($ww,$hh,$thumbb);
            $photo2=substr($filess,11,1600);
            
            $filesss =$obj->image_bigcaption3($www,$hhh,$thumbbb);
            $photo3=substr($filesss,11,1600);
            
            $filessss =$obj->image_bigcaption4($wwww,$hhhh,$thumbbbb);
            $photo4=substr($filessss,11,1600);
        }
        else 
        {
                        $errmsg_arr[]= 'Failed You Have to Upload a Picture of Your Product';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
        }
        
        
        $insert=array("name"=>$_POST['name'],"photo"=>$photo,"photo2"=>$photo2,"photo3"=>$photo3,"photo4"=>$photo4,"description"=>$_POST['description'],"cid"=>$_POST['cid'],"scid"=>$_POST['scid'],"quantity"=>$_POST['quantity'],"price"=>$_POST['price'],"reorder"=>$_POST['reorder'],"emplid"=>$input_by,"date"=>  date('Y-m-d'),"status"=>1);
        if($obj->exists_multiple($table,$exist_array)==1)
        {
                        $errmsg_arr[]= 'Already Exists';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
        }
        else
        {
            if($obj->insert($table,$insert)==1)
            {
                    
                    $errmsg_arr[]= 'Successfully Saved';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./product.php");
                        exit();
                    }
            }
            else 
             {
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
            }
        }
    }
 else {
        $errmsg_arr[]= 'Some Field is Empty';
        $errflag = true;
        if ($errflag) {
            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
            session_write_close();
            header("location: ./".$obj->filename());
            exit();
        }
    }   
}
elseif (isset ($_POST['edit'])) {
                    //$success="Edit";
                    if(!empty($_FILES['image']['name']))
                    {

                        $files =$obj->image_bigcaption($w,$h,$thumb);
                        $photo=substr($files,11,1600);

                        $filess =$obj->image_bigcaption2($ww,$hh,$thumbb);
                        $photo2=substr($filess,11,1600);

                        $filesss =$obj->image_bigcaption3($www,$hhh,$thumbbb);
                        $photo3=substr($filesss,11,1600);

                        $filessss =$obj->image_bigcaption4($wwww,$hhhh,$thumbbbb);
                        $photo4=substr($filessss,11,1600);
                        
                        
                        unlink('../product/'.$_POST['exphoto']);
                        unlink('../product/'.$_POST['exphoto2']);
                        unlink('../product/'.$_POST['exphoto3']);
                        unlink('../product/'.$_POST['exphoto4']);
                        
                    }
                    else 
                    {
                        $photo=$_POST['exphoto'];
                        $photo2=$_POST['exphoto2'];
                        $photo3=$_POST['exphoto3'];
                        $photo4=$_POST['exphoto4'];
                    }


                    $updatearray=array("id"=>$_POST['id'],"name"=>$_POST['name'],"description"=>$_POST['description'],"photo"=>$photo,"photo2"=>$photo2,"photo3"=>$photo3,"photo4"=>$photo4,"cid"=>$_POST['cid'],"scid"=>$_POST['scid'],"quantity"=>$_POST['quantity'],"price"=>$_POST['price'],"reorder"=>$_POST['reorder'],"emplid"=>$input_by,"date"=>  date('Y-m-d'),"status"=>1);

                    if($obj->update($table,$updatearray)==1)
                    { 
                        $errmsg_arr[]= 'Successfully Updated';
                        $errflag = true;
                        if ($errflag) 
                        {
                            $_SESSION['SMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./product.php");
                            exit();
                        }
                        
                    } 
                    else 
                    { 
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        } 
                        
                    }
    
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
        <script src="ckeditor/ckeditor.js"></script>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="header smaller lighter blue">Add New Product</h3>
                            <!-- PAGE CONTENT BEGINS -->
                            <?php
                            if(@$_GET['action']=='pedit')
                            { 
                                $pdata=$obj->SelectAllByID($table,array("id"=>$_GET['id']));
                                if(!empty($pdata))
                                    foreach ($pdata as $prow){
                             ?>
                                <form class="form-horizontal" name="addproduct" enctype="multipart/form-data" role="form" action="" method="POST">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Product Name </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" value="<?php echo $prow->name; ?>" name="name" placeholder="Product Name" class="col-xs-10 col-sm-5" />
                                        <input type="hidden" id="form-field-1" value="<?php echo $prow->id; ?>" name="id" placeholder="Product Name" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                               
                                 
                                    
                               <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Desciption </label>

                                    <div class="col-sm-9">
                                        <textarea name="description"  cols="70" style="width: 700px;" id="description"  rows="10"><?php echo $prow->description; ?></textarea>
                                    		<script>

                                                    // This call can be placed at any point after the
                                                    // <textarea>, or inside a <head><script> in a
                                                    // window.onload event handler.

                                                    CKEDITOR.replace( 'description', {
                                                            extraPlugins: 'magicline',	// Ensure that magicline plugin, which is required for this sample, is loaded.
                                                            allowedContent: true		// Switch off the ACF, so very complex content created to
                                                                                                                    // show magicline's power isn't filtered.
                                                    } );

                                            </script>  
                                            <br>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Product Category </label>

                                    <div class="col-sm-9">
                                        <select name="cid">
                                            <option value="">Select Category</option>
                                            <?php $category=$obj->SelectAll("category"); foreach($category as $cat): ?>
                                            <option <?php if($prow->cid==$cat->id){ echo "selected='selected'"; } ?>  value="<?php echo $cat->id; ?>"><?php echo $cat->name; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Product Sub-Category </label>

                                    <div class="col-sm-9">
                                        <select name="scid">
                                            <option value="">Select Sub-Category</option>
                                            <?php $subcategory=$obj->SelectAll("subcategory"); foreach($subcategory as $scat): ?>
                                            <option <?php if($prow->scid==$scat->id){ echo "selected='selected'"; } ?> value="<?php echo $scat->id; ?>"><?php echo $scat->name; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Product Price </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" value="<?php echo $prow->price; ?>" name="price" placeholder="Price" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Quantity </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" value="<?php echo $prow->quantity; ?>" name="quantity" placeholder="Quantity" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Re-Order </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" value="<?php echo $prow->reorder; ?>" name="reorder" placeholder="Re-Order" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Update Photo </label>

                                    <div class="col-sm-9">
                                        <img src="../product/<?php echo $prow->photo; ?>" alt="product Ex Photo" style="float: right;">
                                        <input type="file" id="form-field-1" name="image" placeholder="Address " class="col-xs-10 col-sm-5" />
                                        <input type="hidden" name="exphoto"  value="<?php echo $prow->photo; ?>">
                                        <input type="hidden" name="exphoto2"  value="<?php echo $prow->photo2; ?>">
                                        <input type="hidden" name="exphoto3"  value="<?php echo $prow->photo3; ?>">
                                        <input type="hidden" name="exphoto4"  value="<?php echo $prow->photo4; ?>">
                                        <input type="hidden" name="exslider"  value="<?php echo $prow->slider; ?>">
                                    </div>
                                </div>
                                

                                <div class="space-4"></div>

                                <div class="clearfix form-actions">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn btn-info" type="submit" name="edit"><i class="icon-ok bigger-110"></i>Save</button>
                                        &nbsp; &nbsp; &nbsp;
                                        <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                    </div>
                                </div>
                            </form>
                            <?php 
                                    }
                            
                                    }
                            else
                            {
                            ?>
                            <form class="form-horizontal" name="addproduct" enctype="multipart/form-data" role="form" action="" method="POST">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Product Name </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="name" placeholder="Product Name" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                 
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Product Category </label>

                                    <div class="col-sm-9">
                                        <select name="cid">
                                            <option value="">Select Category</option>
                                            <?php $category=$obj->SelectAll("category"); foreach($category as $cat): ?>
                                            <option value="<?php echo $cat->id; ?>"><?php echo $cat->name; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Product Sub-Category </label>

                                    <div class="col-sm-9">
                                        <select name="scid">
                                            <option value="">Select Sub-Category</option>
                                            <?php $subcategory=$obj->SelectAll("subcategory"); foreach($subcategory as $scat): ?>
                                            <option value="<?php echo $scat->id; ?>"><?php echo $scat->name; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Product Price </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="price" placeholder="Price" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Quantity </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="quantity" placeholder="Quantity" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Re-Order </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="reorder" placeholder="Re-Order" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Upload Product Photo </label>

                                    <div class="col-sm-3">
                                        <input type="file" name="image" id="id-input-file-1" />
                                        
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Desciption </label>

                                    <div class="col-sm-9">
                                        <textarea name="description"  cols="70" style="width: 700px;" id="description"  rows="10"></textarea>
                                    		<script>

                                                    // This call can be placed at any point after the
                                                    // <textarea>, or inside a <head><script> in a
                                                    // window.onload event handler.

                                                    CKEDITOR.replace( 'description', {
                                                            extraPlugins: 'magicline',	// Ensure that magicline plugin, which is required for this sample, is loaded.
                                                            allowedContent: true		// Switch off the ACF, so very complex content created to
                                                                                                                    // show magicline's power isn't filtered.
                                                    } );

                                            </script>  
                                            <br>
                                    </div>
                                </div>
                                
                                
                                

                                <div class="space-4"></div>

                                <div class="clearfix form-actions">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn btn-info" type="submit" name="submit"><i class="icon-ok bigger-110"></i>Save</button>
                                        &nbsp; &nbsp; &nbsp;
                                        <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                    </div>
                                </div>
                            </form>
                            <?php } ?>

                                
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
            include('class/footer.php');
            ?>


                <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->

			<script type="text/javascript">
			jQuery(function($) {
				      $('#id-input-file-1 , #id-input-file-2').ace_file_input({
                no_file:'No File ...',
                btn_choose:'Choose',
                btn_change:'Change',
                droppable:false,
                onchange:null,
                thumbnail:false //| true | large
                //whitelist:'gif|png|jpg|jpeg'
                //blacklist:'exe|php'
                //onchange:''
                //
        });

			})
		</script>
    </body>
</html>

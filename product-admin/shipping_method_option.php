<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Shipping Method Info</a></li><li><a href='#'>Shipping Method List</a></li><li class='active'>Shipping Method Option Add / List</li>";
$table = "shipping_method_option";
if (isset($_POST['submit'])) {
    if (!empty($_POST['name'])) {
        $exist_array = array("name" => $_POST['name']);
        $insert = array("smid" => $_POST['smid'], "name" => $_POST['name'], "cost" => $_POST['cost'], "date" => date('Y-m-d'), "status" => 1);
        if ($obj->exists($table, $exist_array) == 1) {
            $errmsg_arr[] = 'Already Exists';
            $errflag = true;
            if ($errflag) {
                $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                session_write_close();
                header("location: ./" . $obj->filename());
                exit();
            }
        } else {
            if ($obj->insert($table, $insert) == 1) {
                $errmsg_arr[] = 'Successfully Saved';
                $errflag = true;
                if ($errflag) {
                    $_SESSION['SMSG_ARR'] = $errmsg_arr;
                    session_write_close();
                    header("location: ./" . $obj->filename());
                    exit();
                }
            } else {
                $errmsg_arr[] = 'Failed';
                $errflag = true;
                if ($errflag) {
                    $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                    session_write_close();
                    header("location: ./" . $obj->filename());
                    exit();
                }
            }
        }
    } else {
        $errmsg_arr[] = 'Fields is Empty';
        $errflag = true;
        if ($errflag) {
            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
            session_write_close();
            header("location: ./" . $obj->filename());
            exit();
        }
    }
} elseif (isset($_POST['edit'])) {
    //$success="Edit";
    $updatearray = array("id" => $_POST['id'], "smid" => $_POST['smid'], "name" => $_POST['name'], "cost" => $_POST['cost']);
    if ($obj->update($table, $updatearray) == 1) {
        $errmsg_arr[] = 'Successfully Updated';
        $errflag = true;
        if ($errflag) {
            $_SESSION['SMSG_ARR'] = $errmsg_arr;
            session_write_close();
            header("location: ./" . $obj->filename());
            exit();
        }
    } else {
        $errmsg_arr[] = 'Failed';
        $errflag = true;
        if ($errflag) {
            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
            session_write_close();
            header("location: ./" . $obj->filename());
            exit();
        }
    }
} elseif (isset($_GET['del']) == "delete") {
    $delarray = array("id" => $_GET['id']);
    if ($obj->delete($table, $delarray) == 1) {
        $errmsg_arr[] = 'Successfully Delete';
        $errflag = true;
        if ($errflag) {
            $_SESSION['SMSG_ARR'] = $errmsg_arr;
            session_write_close();
            header("location: ./" . $obj->filename());
            exit();
        }
    } else {
        $errmsg_arr[] = 'Failed';
        $errflag = true;
        if ($errflag) {
            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
            session_write_close();
            header("location: ./" . $obj->filename());
            exit();
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="header smaller lighter blue">Add New Shipping Method Option</h3>
                            <!-- PAGE CONTENT BEGINS -->

                            <form class="form-horizontal" name="designationadd" role="form" action="" method="POST">

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Option Name </label>

                                    <div class="col-sm-9">
                                        <select name="smid">
                                            <?php
                                            $datasm = $obj->SelectAll("shipping_method");
                                            if (!empty($datasm))
                                                foreach ($datasm as $sm):
                                                    ?>
                                                    <option value="<?php echo $sm->id; ?>"><?php echo $sm->name; ?></option>
                                                    <?php
                                                endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Option Name </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="name" placeholder="Shipping Method Option" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Cost </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="cost" placeholder="Shipping Method Option Cost" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>


                                <div class="space-4"></div>

                                <div class="clearfix form-actions">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn btn-info" type="submit" name="submit"><i class="icon-ok bigger-110"></i>Save</button>
                                        &nbsp; &nbsp; &nbsp;
                                        <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                    </div>
                                </div>
                            </form>
                            <div class="hr hr-18 dotted hr-double"></div>



                            <div class="hr hr-18 dotted hr-double"></div>           

                            <div class="row">

                                <div class="col-xs-12">
                                    <h3 class="header smaller lighter blue">Shipping Method List</h3>
                                    <div class="table-header">
                                        Total Shipping Method ( <?php echo $obj->totalrows($table); ?> )
                                    </div>

                                    <div class="table-responsive">
                                        <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th class="center">S/N</th>
                                                    <th>Shipping Method</th>
                                                    <th>Option</th>
                                                    <th>Cost</th>
                                                    <th>System Date</th>
                                                    <th>Edit </th>
                                                    <th>Delete</th>
                                                </tr>
                                            </thead>

                                            <tbody id="status">
                                                <?php
                                                $data = $obj->SelectAllorderBy($table);
                                                $x = 1;
                                                if (!empty($data))
                                                    foreach ($data as $row):
                                                        ?>
                                                        <tr>
                                                            <td class="center"><?php echo $x; ?></td>
                                                            <td><?php echo $obj->SelectAllByVal("shipping_method","id",$row->smid,"name"); ?></td>
                                                            <td><?php echo $row->name; ?></td>
                                                            <td><?php echo $row->cost; ?></td>
                                                            <td><span class="label label-sm label-success"><?php echo $row->date; ?></span></td>
                                                            <td>
                                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                                    <a href="#modal-table<?php echo $row->id; ?>" role="button" data-toggle="modal" class="green"><i class="icon-pencil bigger-130"></i> Edit</a>
                                                                </div>
                                                                <div id="modal-table<?php echo $row->id; ?>" class="modal fade" tabindex="-1">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header no-padding">
                                                                                <div class="table-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                                                        <span class="white">&times;</span>
                                                                                    </button>
                                                                                    Edit  Detail
                                                                                </div>
                                                                            </div>
                                                                            <!-- /.modal-content -->
                                                                            <form class="form-horizontal" name="designationedit" role="form" action="" method="POST">
                                                                                <div class="form-group">
                                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Shipping Name </label>

                                                                                    <div class="col-sm-9">
                                                                                        <select name="smid">
                                                                                            <?php 
                                                                                            $datasm=$obj->SelectAll("shipping_method");
                                                                                            if(!empty($datasm))
                                                                                            foreach($datasm as $sm):
                                                                                            ?>
                                                                                            <option value="<?php echo $sm->id; ?>"><?php echo $sm->name; ?></option>
                                                                                            <?php
                                                                                            endforeach;
                                                                                            ?>
                                                                                        </select>
                                                                                        <input type="hidden" name="id" value="<?php echo $row->id; ?>"  />
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Option Name </label>

                                                                                    <div class="col-sm-9">
                                                                                        <input type="text" id="form-field-1" name="name" value="<?php echo $row->name; ?>" class="col-xs-10 col-sm-5" />
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                <div class="form-group">
                                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Cost </label>

                                                                                    <div class="col-sm-9">
                                                                                        <input type="text" id="form-field-1" name="cost" value="<?php echo $row->cost; ?>" class="col-xs-10 col-sm-5" />
                                                                                    </div>
                                                                                </div>


                                                                                <div class="space-4"></div>

                                                                                <div class="clearfix form-actions">
                                                                                    <div class="col-md-offset-3 col-md-9">
                                                                                        <button class="btn btn-info" type="submit" name="edit"><i class="icon-ok bigger-110"></i>Edit Now</button>
                                                                                        &nbsp; &nbsp; &nbsp;
                                                                                        <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div><!-- /.modal-content -->
                                                                    </div><!-- /.modal-dialog -->
                                                                </div><!-- end modal form -->


                                                            </td>
                                                            <td>
                                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                                    <a class="red" href="<?php echo $obj->filename(); ?>?del=delete&AMP;id=<?php echo $row->id; ?>"><i class="icon-trash bigger-130"></i> Delete</a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $x++;
                                                    endforeach;
                                                ?>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>

                            <div id="modal-table" class="modal fade" tabindex="-1">

                                <!-- PAGE CONTENT ENDS -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.page-content -->
                </div><!-- /.main-content -->

                <?php
//include('class/colornnavsetting.php');
                include('class/footer.php');
                ?>


<?php echo $obj->bodyfooter(); ?>

                <!-- inline scripts related to this page -->
                <script type="text/javascript">
                    jQuery(function($) {

                    })
                </script>
                </body>
                </html>

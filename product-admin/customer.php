<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Customer Info</a></li><li class='active'>Customer List</li>";
$table="customer";
if (isset ($_GET['del'])=="order") {
                    $delarray=array("id"=>$_GET['id'],"status"=>2);
                    if($obj->update($table,$delarray)==1)
                    { 
                        $errmsg_arr[]= 'Successfully Updated';
                        $errflag = true;
                        if ($errflag) 
                        {
                            $_SESSION['SMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    } 
                    else 
                    { 
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    }
}

if (isset ($_GET['delete'])) {
                    $delarray=array("id"=>$_GET['delete']);
                    if($obj->delete($table,$delarray)==1)
                    { 
                        $errmsg_arr[]= 'Successfully Deleted';
                        $errflag = true;
                        if ($errflag) 
                        {
                            $_SESSION['SMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    } 
                    else 
                    { 
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    }
}

if(isset($_GET['active']))
{
    if($obj->update("customer",array("id"=>$_GET['active'],"status"=>$_GET['status']))==1)
    {
        //email Script Start 
        //$subject = "Product Order Via Wireless Geeks";
        // Receive form's message value into php $message variable
        $message = '<!DOCTYPE HTML><head><meta http-equiv="content-type" content="text/html"><title>Email notification</title>';
 $message .= "<link href='http://wirelessgeekswholesale.com/css/stylesheet.css' rel='stylesheet' type='text/css' /></head>";
 $message .= "<body><div class='bg-1'><div style='display: block;' class='checkout-content'><div class='checkout-product'>";
        $message .= "Automated Account Opening Confirmation Email From Wireless Geeks ";
        $message .= "<img src='http://wirelessgeekswholesale.com/images/logo.png'><br>";
        $message .= "Dear Sir ,<br> Your account has been successfully Activated, <a href='http://wirelessgeekswholesale.com/login.php'>Please Login -> Click Here to <font color='#09f'>login</font></a></div></div></div></body>";

                
                
                
        $to=$obj->SelectAllByVal("customer","id",$_GET['active'],"email");   
        $too='admin@wirelessgeekswholesale.com'; // give to email address
        $subject = 'Wiressless Geeks - Account Activation Notification';  //change subject of email
        $from    = 'admin@wirelessgeekswholesale.com';                           // give from email address

        // mandatory headers for email message, change if you need something different in your setting.
        $headers  = "From: " . $from . "\r\n";
        $headers .= "Reply-To: ". $from . "\r\n";
        $headers .= "CC: admin@wirelessgeekswholesale.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        mail($to, $subject, $message, $headers);  
        mail($too, $subject, $message, $headers); 
        //email Script End
        
        $errmsg_arr[]='Successfully Updated';
        $errflag = true;
        if ($errflag) 
        {
            $_SESSION['SMSG_ARR'] = $errmsg_arr;
            session_write_close();
            header("location: ./".$obj->filename());
            exit();
        }
    }
    else 
    {
        $errmsg_arr[]='Update Failed';
        $errflag = true;
        if ($errflag) 
        {
            $_SESSION['SMSG_ARR'] = $errmsg_arr;
            session_write_close();
            header("location: ./".$obj->filename());
            exit();
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->          

                                <div class="row">

                                    <div class="col-xs-12">
                                        <h3 class="header smaller lighter blue">Customer List</h3>


                                        <div class="table-responsive">
                                            <table aria-describedby="sample-table-2_info" id="sample-table-2" class="table table-striped table-bordered table-hover dataTable">
                                                <thead>
                                                    <tr>
                                                        <th class="center">S/N</th>
                                                        <td style="width: 50px;">ID</td>
                                                        <th>Full Name</th>
                                                        <th>Email Address</th>
                                                        <th>Telephone</th>
                                                        <th>Address,City-Postcode, State, Country</th>
                                                        <th>Date</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="status">
                                                <?php
                                                if($obj->totalrows($table)!=0)
                                                {
                                                    if(isset($_GET['cusid']))
                                                    {
                                                        $data=$obj->SelectAllByID($table,array("id"=>$_GET['cusid']));
                                                    }
                                                    else 
                                                    {
                                                        $data=$obj->SelectAllorderBy($table);    
                                                    }
                                                
                                                $x=1;
                                                if(!empty($data))
                                                foreach ($data as $row): ?>
                                                        <tr>
                                                            <td class="center"><?php echo $x; ?></td>
                                                            <td><?php echo $row->id; ?></td>
                                                            <td><?php echo $row->fname." ".$row->lname; ?> - <?php echo $row->date; ?></td>
                                                            <td><?php echo $row->email; ?></td>
                                                            <td><span class="label label-sm label-info"><?php echo $row->talephone; ?></span></td>
                                                            <td>
															<?php echo $row->address1; ?>, <?php echo $row->city; ?>-<?php echo $row->postcode; ?>,<?php echo $row->state; ?>,<?php echo $row->country; ?>,,</td>
                                                            <td><a  class="label label-sm label-<?php if($row->status==1){ ?>warning<?php }else{ ?>success<?php } ?>" href="<?php echo $obj->filename(); ?>?active=<?php echo $row->id; ?>&AMP;status=<?php if($row->status==1){ echo 2; }else{ echo 1; } ?>"><?php if($row->status==1){ ?> Active <?php }else{ ?> Activated <?php } ?> </a>
                                                            <a  class="label label-sm label-warning" href="<?php echo $obj->filename(); ?>?delete=<?php echo $row->id; ?>"> Delete User </a>
                                                            </td>
                                                            
                                                        </tr>
                                                 <?php 
                                                 $x++; endforeach; 
                                                }
                                                 ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                 <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				var oTable1 = $('#sample-table-2').dataTable( {
				"aoColumns": [
			      { "bSortable": false },
			      null, null,null, null, null,
				  { "bSortable": false }
				] } );
				
				
				$('table th input:checkbox').on('click' , function(){
					var that = this;
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
						
				});
			
			
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
			})
                        
		</script>
    </body>
</html>

<?php
include('class/auth.php');
$table = "product";
$page = "<li><i class='icon-group group-icon'></i><a href='#'>List of Product</a></li>";
if (@$_GET['feature'] == 'yes') {
    $ex = $obj->exists_multiple("feature", array("pid" => $_GET['pid']));
    if ($ex == 0) {
        $obj->insert("feature", array("pid" => $_GET['pid']));
    }
}

//if (@$_GET['slider'] == 'yes') {
 //   $ex = $obj->exists_multiple("slider", array("pid" => $_GET['pid']));
 //   if ($ex == 0) {
 //       $obj->insert("slider", array("pid" => $_GET['pid']));
  //  }
//}

if (@$_GET['special'] == 'yes') {
    $ex = $obj->exists_multiple("special", array("pid" =>  $_GET['pid']));
    if ($ex == 0) {
        $obj->insert("special", array("pid" => $_GET['pid']));
    }
}



if (@$_GET['feature'] == 'no') { $obj->delete("feature", array("pid" => $_GET['pid'])); }

//if (@$_GET['slider'] == 'no') { $obj->delete("slider", array("pid" => $_GET['pid'])); }

if (@$_GET['specialno'] == 'no') { $obj->delete("special", array("pid" => $_GET['pid'])); }


if (@$_GET['del'] == 'yes') {
        $obj->delete($table, array("id" => $_GET['pid']));
}
extract($_GET);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Product Admin Panel</title>

        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />
        <link rel="stylesheet" href="assets/css/ace.min.css" />
        <link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
        <link rel="stylesheet" href="assets/css/ace-skins.min.css" />
        <script src="assets/js/ace-extra.min.js"></script>
        <script>
            function CreateAdmin(str)
            {
                if (str == "")
                {
                    document.getElementById("status").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("status").innerHTML = xmlhttp.responseText;
                        $("#status").fadeOut(100);
                        $("#status").fadeIn(500);
                    }
                }

                xmlhttp.open("GET", "class/ajax/createadmin.php?chk=" + str, true);
                xmlhttp.send();
            }
        </script>
    </head>

    <body>
<?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
<?php
if (isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) > 0) {
    foreach ($_SESSION['ERRMSG_ARR'] as $msg) {

        echo $msg;
    }
    unset($_SESSION['ERRMSG_ARR']);
}
?>





<?php
$sqlcategory = $obj->SelectAll("category");
foreach ($sqlcategory as $category) {
    ?>
                    <a style="margin-bottom: 5px;" class="btn btn-primary" href="product_organise.php?id=<?php echo $category->id; ?>" data-loading-text="Loading..."><?php echo $category->name; ?></a>
                        <?php
                    }
                    ?>
                    <a id="loading-btn" style="margin-bottom: 5px;" class="btn btn-success" href="product_organise.php?id=all" data-loading-text="Loading...">Load All News</a>
                    <div class="row">

                        <div class="col-xs-12">
                            <h3 class="header smaller lighter blue">Product List Limit - (300)</h3>
                            <div class="table-header">
                                Results for "Total Product&rsquo;s" (<?php
                    if (!@$id) {
                        echo $obj->totalrows("product");
                    } else {
                        echo $obj->exists_multiple("product", array("cid" => $id));
                    }
                    ?>)
                            </div>
                            <fieldset style="padding-bottom: 5px; padding-top: 5px; clear: both;">
                                Search <input type="text" class="text-input" style="width: 330px;" id="topsix"  placeholder="Please Search.."  />
                                <span id="topsix-count"></span>
                            </fieldset> 
                            <div class="clearfix"></div>
                            <div class="table-responsive">
                                <table aria-describedby="sample-table-2_info" id="sample-table-2" class="table table-striped table-bordered table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th class="center">S/N</th>
                                            <th class="hidden-480">Category</th>
                                            <th>Title </th>
                                            <th>Feature</th>
                                            <th>Action</th>
                                            <th>Action</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>

                                    <tbody id="status" class="commentlist">
                                        <?php
                                        if (!@$id) {
                                            $data = $obj->SelectAllLimit($table, "DESC", 40);
                                        } elseif ($id == 'all') {
                                            $data = $obj->SelectAll($table);
                                        } elseif ($id != '') {
                                            $data = $obj->SelectAllByID_site($table, array("cid" => $id), "DESC", 40);
                                        }
                                        $x = 1;
                                        if (!empty($data))
                                            foreach ($data as $row) {
                                                ?>
                                                <tr class="topsix">
                                                    <td class="center"><?php echo $x; ?></td>
                                                    <td>
                                                        <?php echo $obj->SelectAllByVal("category", "id", $row->cid, "name"); ?>
                                                    </td>
                                                    <td><?php echo $row->name; ?>
                                                        <span class="label label-sm label-info"><?php echo $row->date; ?></span>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $chk = $obj->exists_multiple("special", array("pid" => $row->id));
                                                        if ($chk != 1) 
                                                        {
                                                        ?>
                                                        <a class="blue" href="<?php echo $obj->filename(); ?>?pid=<?php echo $row->id; ?>&amp;special=yes"><i class="icon-desktop"></i> Add In Special</a>
                                                        <?php }else{ ?>
                                                        <a class="blue" href="<?php echo $obj->filename(); ?>?pid=<?php echo $row->id; ?>&amp;specialno=no"><i class="icon-desktop"></i> Remove From Special</a>
                                                        <?php } ?>
                                                   </td>
                                                    <td>
                                                        <?php
                                                        $chk = $obj->exists_multiple("feature", array("pid" => $row->id));
                                                        if ($chk != 1) 
                                                        {
                                                        ?>
                                                            <a class="blue" href="<?php echo $obj->filename(); ?>?pid=<?php echo $row->id; ?>&amp;feature=yes"><i class="icon-plus"></i> feature </a> &nbsp;&nbsp;&nbsp;&nbsp;
                                                        
                                                        <?php }else{ ?>
                                                        <a class="blue" href="<?php echo $obj->filename(); ?>?pid=<?php echo $row->id; ?>&amp;feature=no"><i class="icon-plus"></i>Remove feature </a> &nbsp;&nbsp;&nbsp;&nbsp;
                                                        <?php
                                                        }
                                                        //$chk = $obj->exists_multiple("slider", array("pid" => $row->id));
                                                        //if ($chk != 1) {
                                                            ?>
                                                            <!--<a class="blue" href="<?php// echo $obj->filename(); ?>?pid=<?php// echo $row->id; ?>&amp;slider=yes"><i class="icon-plus"></i> Slider</a>-->
                                                        <?php //}else{ ?>
                                                            <!--<a class="blue" href="<?php// echo $obj->filename(); ?>?pid=<?php //echo $row->id; ?>&amp;slider=no"><i class="icon-plus"></i>Remove Slider</a>-->
                                                        <?php// } ?>
                                                    </td>
                                                    <td>
                                                        <a class="blue" href="addproduct.php?action=pedit&id=<?php echo $row->id; ?>"><i class="icon-edit"></i> Edit</a>


                                                    </td>

                                                    <td>

                                                        <a class="red" href="<?php echo $obj->filename(); ?>?pid=<?php echo $row->id; ?>&amp;del=yes"><i class="icon-trash"></i> Delete</a> 

                                                    </td>
                                                </tr>
                                                <?php
                                                $x++;
                                            }
                                        ?>


                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                    <!-- PAGE CONTENT ENDS -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div><!-- /.main-content -->

    <?php
//include('class/colornnavsetting.php');
    include('class/footer.php');
    ?>


    <script src="../../../../ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript">
            window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
    </script>
    <script type="text/javascript">
        if ("ontouchend" in document)
            document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
    </script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/typeahead-bs2.min.js"></script>
    <script src="assets/js/ace-elements.min.js"></script>
    <script src="assets/js/ace.min.js"></script>
    <script src="assets/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/jquery.dataTables.bootstrap.js"></script>



    <script type="text/javascript">
        jQuery(function($) {

            $('#loading-btn').on(ace.click_event, function() {
                var btn = $(this);
                btn.button('loading')
                setTimeout(function() {
                    btn.button('reset')
                }, 2000)
            });

            $("#topsix").keyup(function() {
                var filter = $(this).val(), count = 0;
                $(".topsix").each(function() {
                    if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                        $(this).fadeOut();
                    } else {
                        $(this).show();
                        count++;
                    }
                });
                var numberItems = count;
                $("#topsix-count").text("Result= " + count);
            });


            var oTable1 = $('#sample-table-2').dataTable({
                "aoColumns": [
                    {"bSortable": false},
                    null, null, null, null, null,
                    {"bSortable": false}
                ]});


            $('table th input:checkbox').on('click', function() {
                var that = this;
                $(this).closest('table').find('tr > td:first-child input:checkbox')
                        .each(function() {
                            this.checked = that.checked;
                            $(this).closest('tr').toggleClass('selected');
                        });

            });


            $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
            function tooltip_placement(context, source) {
                var $source = $(source);
                var $parent = $source.closest('table')
                var off1 = $parent.offset();
                var w1 = $parent.width();

                var off2 = $source.offset();
                var w2 = $source.width();

                if (parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2))
                    return 'right';
                return 'left';
            }



        })
    </script>
</body>
</html>

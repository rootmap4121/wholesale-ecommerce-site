<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Profile Info</a></li><li class='active'>User Profile</li>";

if (isset($_POST['submit'])) {
$table = "requisition";
    if (!empty($_POST['empid']) && !empty($_POST['projectid'])) {
                $quantity = $_POST['quantity'];
                $pid=$_POST['pid'];
                
                $table1="requisationlist";
                $insert = array("empid" => $_POST['empid'], "projectid" => $_POST['projectid'],"date" => date('Y-m-d'), "status" => 1);
                
                //$req_id=$obj->insert_id($table1,$insert); 
                
                if($obj->insert($table1,$insert)==1)
                { 
                    $fg=$obj->lastid($table1);
                    foreach ($fg as $rt):
                        $req_id=$rt->id;
                        foreach ($_POST['pid'] as $index=>$aa):
                        if(!empty($aa)){ $obj->insert($table,array("req_id"=>$req_id,"empid"=>$_POST['empid'],"to"=>$_POST['to'],"projectid"=>$_POST['projectid'],"pid"=>$aa,"quantity"=>$_POST['quantity'][$index],"date"=>  date('Y-m-d'),"status"=>1,"description"=>$_POST['des'][$index])); }
                        endforeach;
                    endforeach;
                    $errmsg_arr[]= 'Successfully Saved';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                    }
                }
                else
                { 
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                }          
    }else
    { 
            $errmsg_arr[]= 'Failed,Field is Empty';
            $errflag = true;
            if ($errflag) {
                $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                session_write_close();
                header("location: ./".$obj->filename());
                exit();
            }
        
    }
}

if(isset($_POST['leave']))
{
    $table="requisation_leaveapplication_form";
    if(!empty($_POST['emplid']))
    {
        $exist_array=array("emplid"=>$_POST['emplid']);
        $insert=array("emplid"=>$_POST['emplid'],"title"=>$_POST['title'],"leave_type"=>$_POST['leave_type'],"description"=>$_POST['description'],"datequantity"=>$_POST['datequantity'],"startdate"=>$_POST['startdate'],"enddate"=>$_POST['enddate'],"phone"=>$_POST['phone'],"emplid_m"=>$_POST['emplid_m'],"date"=>  date('Y-m-d'),"status"=>1);
        if($obj->exists($table,$exist_array)==1)
        {
                        $errmsg_arr[]= 'Already Exists';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
        }
        else
        {
            if($obj->insert($table,$insert)==1)
            {
                    $errmsg_arr[]= 'Successfully Saved';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                    }
            }
            else 
             {
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
            }
        }
    }
 else {
                        $errmsg_arr[]= 'Some Field Are Empty';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
    }   
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
<?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

<?php
include('class/esm.php');
?>
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="header smaller lighter blue">User Profile</h3>
                            <!-- PAGE CONTENT BEGINS -->

								
								<div>
									<div id="user-profile-2" class="user-profile">
										<div class="tabbable">
											<ul class="nav nav-tabs padding-18">
												<li class="active">
													<a data-toggle="tab" href="#home">
														<i class="green icon-user bigger-120"></i>
														Profile
													</a>
												</li>

												<li>
													<a data-toggle="tab" href="#feed">
														<i class="orange icon-plus bigger-120"></i>
														Requisation List
													</a>
												</li>
                                                                                                <li>
													<a data-toggle="tab" href="#add_requisation">
														<i class="orange icon-calendar bigger-120"></i>
														Add Requisation
													</a>
												</li>
                                                                                                 <li>
													<a data-toggle="tab" href="#add_leaveapplication">
														<i class="orange icon-plus bigger-120"></i>
														Add Leave Application
													</a>
												</li>
                                                                                                
                                                                                                                                                                                                 <li>
													<a data-toggle="tab" href="#leaveapplication_list">
														<i class="orange icon-calendar bigger-120"></i>
                                                                                                            Leave Application List
													</a>
												</li>
											</ul>

											<div class="tab-content no-border padding-24">
												<div id="home" class="tab-pane in active">
												<?php 
                                                                                                $fff=$obj->SelectAllByID("employee",array("id"=>$_SESSION['SESS_AMSIT_APPS_ID']));
                                                                                                foreach($fff as $data):
                                                                                                ?>	
                                                                                                    <div class="row">
														<div class="col-xs-12 col-sm-3 center">
															<span class="profile-picture">
                                                                                                                            <img class="editable img-responsive" alt="<?php echo $_SESSION['SESS_AMSIT_APPS_ID']; ?>" height="200" width="200" id="avatar2" src="emp/<?php echo $data->photo; ?>" />
															</span>


														</div><!-- /span -->

														<div class="col-xs-12 col-sm-9">
															<h4 class="blue">
																<span class="middle"><?php echo $data->name; ?></span>

																<span class="label label-purple arrowed-in-right">
																	<i class="icon-circle smaller-80 align-middle"></i>
																	online
																</span>
															</h4>

															<div class="profile-user-info">
																<div class="profile-info-row">
																	<div class="profile-info-name"> Username </div>

																	<div class="profile-info-value">
																		<span><?php echo $data->username; ?></span>
																	</div>
																</div>

																<div class="profile-info-row">
																	<div class="profile-info-name"> Location </div>

																	<div class="profile-info-value">
																		<i class="icon-map-marker light-orange bigger-110"></i>
																		<span><?php echo $data->address; ?></span>

																	</div>
																</div>

																<div class="profile-info-row">
																	<div class="profile-info-name"> Date of Birth </div>

																	<div class="profile-info-value">
																		<span><?php echo $data->dob; ?></span>
																	</div>
																</div>

																<div class="profile-info-row">
																	<div class="profile-info-name"> Joined </div>

																	<div class="profile-info-value">
																		<span><?php echo $data->joiningdate; ?></span>
																	</div>
																</div>


															</div>

															<div class="hr hr-8 dotted"></div>

															<div class="profile-user-info">
																<div class="profile-info-row">
																	<div class="profile-info-name"> System Joing Date </div>

																	<div class="profile-info-value">
																		<a href="#" target="_blank"><?php echo $data->date; ?></a>
																	</div>
																</div>

																
															</div>
														</div><!-- /span -->
													</div><!-- /row-fluid -->
                                                                                                        
                                                                                                        <?php endforeach; ?>

													<div class="space-20"></div>

													<div class="row">
														<div class="col-xs-12 col-sm-12">
														
															<div class="widget-box transparent">
																<div class="widget-header widget-header-small header-color-blue2">
																	<h4 class="smaller">
																		<i class="icon-lightbulb bigger-120"></i>
																		My Current Status
																	</h4>
																</div>

																<div class="widget-body">
																	<div class="widget-main">
																		<div class="clearfix">
<div class="grid3">
        <div class="easy-pie-chart percentage" data-percent="<?php $per=$obj->leave_quantity(); echo $per; ?>" data-color="#CA5952">
                <span class="percent"><?php echo $per; ?></span>%
        </div>

        <div class="space-2"></div>
        Leave Taken
</div>
                                                                                                                                                    
                                                                                                                                                    <div class="grid3">
        <div class="easy-pie-chart percentage" data-percent="<?php $per=$obj->leave_quantity(); echo $per; ?>" data-color="#CA5952">
                <span class="percent"><?php echo $per; ?></span>%
        </div>

        <div class="space-2"></div>
        Requisation
</div>
                                                                                                                                                    
                                                                                                                                                    <div class="grid3">
        <div class="easy-pie-chart percentage" data-percent="<?php $per=$obj->leave_quantity(); echo $per; ?>" data-color="#CA5952">
                <span class="percent"><?php echo $per; ?></span>%
        </div>

        <div class="space-2"></div>
        Vehecle Requisation
</div>
                                                                                                                                                    
                                                                                                                                                    

				
																		</div>
                                                                                                                                            
                                                                                                                                            <div style="margin-top: 15px;" class="clearfix">
                                                                                                                               
                                                                                                                                                    

				
																		</div>

																															</div>
																</div>
															</div>
														</div>
													</div>
												</div><!-- #home -->

												<div id="add_requisation" class="tab-pane">
													<div class="profile-feed row-fluid">
														<div class="row">

                                <div class="col-xs-12">
                                    <form class="form-horizontal" name="designationedit" role="form" action="" method="POST">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>Name</td>
                                                    <td>
                                                        <input type="hidden" name="empid" value="<?php echo $input_by; ?>">
                                                        <select class="width-10" name="xccx">
                                                            <option value="">Select Employee</option>
<?php $data = $obj->SelectAllByID("employee",array("id"=>$input_by));
foreach ($data as $rowss):
    ?>
                                                                <option <?php if ($rowss->id == $input_by): ?> selected="selected" <?php endif; ?>  value="<?php echo $rowss->id; ?>"><?php echo $rowss->name; ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </td>
                                                    <td>ID NO.</td>
                                                    <td>
                                                        <input type="text" id="form-field-1" disabled="disabled" name="idno" placeholder="System Will Be Generate " />
                                                    </td>
                                                    <td class="center" colspan="2">
                                                        <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Project</td>
                                                    <td>
                                                        <select class="width-10" name="projectid">
                                                            <option value="">Select Employee</option>
<?php $data = $obj->SelectAllorderBy("project");
foreach ($data as $rowss):
    ?>
                                                                <option value="<?php echo $rowss->id; ?>"><?php echo $rowss->name; ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </td>
                                                    <td>To</td>
                                                    <td>
                                                        <select name="to">
                                                        <?php $tooo=$obj->SelectAll("designation"); foreach($tooo as $to): ?>
                                                            <option value="<?php echo $to->id; ?>"><?php echo $to->name; ?></option>
                                                        <?php endforeach; ?>
                                                        </select>
                                                        
                                                    </td>
                                                    <td class="center" colspan="2">
                                                        <button class="btn btn-info" type="submit" name="submit"><i class="icon-ok bigger-110"></i>Save</button>
                                                    </td>


                                                </tr>
                                            </tbody>


                                        </table>
                                        <div class="table-header">
                                            Please arrange to supply the following items&because;
                                        </div>

                                        <div class="table-responsive">
                                            <table  id="itemTable" class="table">
                                                <thead>
                                                    <tr>
                                                        <th> Select Product </th>
                                                        <th class="center"> Quantity Request </th>
                                                        <th class="center"> Description </th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                        <tr id="InputRow">
                                                            <td>
                                                                <select name="pid[]"  class="form-control" id="form-field-select-1">
                                                                <?php $pro=$obj->SelectAll("product"); foreach($pro as $product): ?>
                                                                    <option value="<?php echo $product->id; ?>"><?php echo $product->name; ?></option>
                                                                <?php endforeach; ?>
                                                                </select>
                                                            </td>
                                                            
                                                            <td class="center"><input type="text" id="form-field-1" name="quantity[]" id="quantity[]" placeholder="Type Your Quantity"  /></td>
                                                            <td><input type="text" id="form-field-3" style="width: 100%;" name="des[]" id="des[]" placeholder="Type Your Product Description"  /></td>

                                                        </tr>



                                                </tbody>
                                            </table>
                                           <button class="btn btn-lg btn-success" type="button" onclick="return addTableRow('#itemTable');return false;">
					    <i class="icon-plus"></i> Add More Products
					   </button>
                                            <div class="clearfix"></div>
                                            <br>
                                        </div>
                                    </form>                                 								

                                    <div class="table-header">
                                        Requested By :________________. Recommended by:____________. Approved by:_______________. Received by:_________________.<br/>
                                        Signature with date .............. Signature with date ...............Signature with date...............Signature with date
                                    </div>

                                </div>


                                <!-- PAGE CONTENT ENDS -->
                            </div><!-- /.col -->
													</div><!-- /row -->

													<div class="space-12"></div>

													
												</div><!-- /#feed end-------------------------------------------------------------------- -->
                                                                                                <div id="feed" class="tab-pane">
													<div class="profile-feed row-fluid">
														<h3 class="header smaller lighter blue">Requisation List <span style="float: right;"><a href="#" style="text-decoration: none;"  onclick="javascript:printDiv('printablediv')"><i class="icon-print"></i> Print All</a></span></h3>    
                            <div class="row">
                                <div class="table-header">
                                          <fieldset>
                                                                        <input type="text" class="text-input" style="width: 200px;" id="topsix"  placeholder="Please Search Anything.."  />
                                                                        <span id="topsix-count"></span>
                                                            </fieldset>
                                        </div>

                                    <div class="col-xs-12" id="printablediv">
                                        

                                        
                                        

                                       
                                            
                            

                                        <div class="table-responsive">
                                            <h4 align="center">
                                               MSH Individual Store Request List
                                                <br>
                                                <small>
                                                    Office and Country: SIAPS Bangladesh
                                                </small>
                                                <br>
                                                <small>
                                                    As of (Date): <?php echo date('M d, Y'); ?>
                                                </small>
                                                <br>
                                                
                                                <small>person who prepared report : 
                                                    <?php 
                                                    $prepar=$obj->SelectAllByID("employee",array("id"=>$input_by));  
                                                    foreach($prepar as $pre):
                                                        echo $pre->name;
                                                    endforeach;
                                                    ?>
                                                </small>    
                                            </h4>
                                            <table aria-describedby="sample-table-2_info" id="sample-table-2" class="table table-striped table-bordered table-hover dataTable">
                                                <thead>
                                                    <tr>
                                                        <th>S/L </th>
                                                        <th>Requisation Id</th>
                                                        <th>employee Name</th>
                                                        <th>Project Name</th>
                                                        <th>Date</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>

                                                <tbody class="commentlist">
                                                <?php
                                                    $data=$obj->SelectAllByID("requisationlist",array("empid"=>$input_by));
                                                $x=1;
                                                foreach ($data as $row): ?>
                                                    <tr class="topsix">
                                                            <td><?php echo $x; ?></td>
                                                            <td><?php echo $row->id; ?></td>
                                                            <td><span class="label label-sm label-warning">
                                                                <?php
                                                                $empsql=$obj->SelectAllByID("employee",array("id"=>$row->empid));
                                                                foreach($empsql as $emp): 
                                                                    echo $emp->name;
                                                                endforeach;
                                                                ?>
                                                                </span>
                                                            </td>
                                                            <td><span class="label label-sm label-info">
                                                                <?php
                                                                $prosql=$obj->SelectAllByID("project",array("id"=>$row->projectid));
                                                                foreach($prosql as $pro): 
                                                                    echo $pro->name;
                                                                endforeach;
                                                                ?>
                                                                </span>
                                                            </td>
                                                            <td><span class="label label-sm label-success"><?php echo $row->date; ?></span></td>
                                                            <td><span class="label label-sm label-danger"><?php  echo $obj->reqstatus($row->status); ?></span></td>
                                                            
                                                        </tr>
                                                 <?php 
                                                 
                                                 $x++; endforeach; 
                                         
                                                 ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
													</div><!-- /row -->

													<div class="space-12"></div>

													
												</div><!-- /#feed end-------------------------------------------------------------------- -->

                                                                                                <div id="add_leaveapplication" class="tab-pane">
													<div class="profile-feed row-fluid">
														<div class="row">
                        <div class="col-xs-12">
                            
                            <!-- PAGE CONTENT BEGINS -->

                            <form class="form-horizontal" name="designationadd" role="form" action="" method="POST">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Requester </label>
                                    <div class="col-sm-9">
                                        <select id="form-field-1" name="emplid" class="col-xs-10 col-sm-5">
                                            <?php 
                                            $empsql=$obj->SelectAll("employee");
                                            foreach($empsql as $emp):
                                            ?>
                                            <option value="<?php echo $emp->id; ?>"><?php echo $emp->name; ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Title </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="title" class="col-xs-10 col-sm-5"/>
                                    </div>
                                </div>                                
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Phone </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="phone" class="col-xs-10 col-sm-5"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Leave Type </label>
                                    <div class="col-sm-9">
                                        <select id="form-field-1" name="leave_type" class="col-xs-10 col-sm-5">
                                            <?php 
                                            $empsql=$obj->SelectAll("leave_type");
                                            foreach($empsql as $emp):
                                            ?>
                                            <option value="<?php echo $emp->id; ?>"><?php echo $emp->name; ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Description </label>
                                    <div class="col-sm-9">
                                        <textarea type="text" id="form-field-1" name="description" class="col-xs-10 col-sm-5"></textarea>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Start Date </label>

                                    <div class="col-xs-6 col-sm-4">
                                            <div class="input-group">
                                                <input class="form-control date-picker" name="startdate" value="<?php echo date('Y-m-d'); ?>" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd" />
                                                    <span class="input-group-addon">
                                                            <i class="icon-calendar bigger-110"></i>
                                                    </span>
                                            </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> End Date </label>

                                    <div class="col-xs-6 col-sm-4">
                                            <div class="input-group">
                                                <input class="form-control date-picker" name="enddate" value="<?php echo date('Y-m-d'); ?>" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd" />
                                                    <span class="input-group-addon">
                                                            <i class="icon-calendar bigger-110"></i>
                                                    </span>
                                            </div>
                                    </div>
                                </div>    
                                
                               <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Leave Days Quantity </label>

                                    <div class="col-sm-9">
                                        <select id="form-field-1" name="datequantity" class="col-xs-10 col-sm-5">
                                        <?php 
                                        for($i=1; $i<=60; $i++):
                                        ?>    
                                            <option value="<?php echo $i; ?>"><?php echo $i; ?> Days</option>
                                        <?php
                                        endfor;
                                        ?>
                                        </select>
                                    </div>
                                </div> 
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Manager </label>

                                    <div class="col-sm-9">
                                        <select id="form-field-1" name="emplid_m" class="col-xs-10 col-sm-5">
                                        <?php 
                                        $mana=$obj->SelectAll("employee");
                                        foreach($mana as $man):
                                        ?>    
                                            <option value="<?php echo $man->id; ?>"><?php echo $man->name; ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="space-4"></div>

                                <div class="clearfix form-actions">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn btn-info" type="submit" name="leave"><i class="icon-ok bigger-110"></i>Save</button>
                                        &nbsp; &nbsp; &nbsp;
                                        <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                    </div>
                                </div>
                            </form>
                                								<div class="hr hr-18 dotted hr-double"></div>

								

				
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
													</div><!-- /row -->

													<div class="space-12"></div>

													
												</div><!-- /#feed end-------------------------------------------------------------------- -->

                                                                                                <div id="leaveapplication_list" class="tab-pane">
													<div class="profile-feed row-fluid">
														<div class="row">
                        <div class="col-xs-12">
                            <h3 class="header smaller lighter blue">Leave Application Form List<span style="margin-left: 20px;"><a  href="#modal-tablesearch" role="button" data-toggle="modal" class="green"><i class="icon-camera-retro"></i> Search in Multiple Dates</a></span> <span style="float: right;"><a href="#" style="text-decoration: none;"  onclick="javascript:printDiv('printablediv')"><i class="icon-print"></i> Print All</a></span></h3>
                            <div id="modal-tablesearch" class="modal fade" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header no-padding">
												<div class="table-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
														<span class="white">&times;</span>
													</button>
													Report Using Multiple Date
												</div>
											</div>
                                                                                        <!-- /.modal-content -->
                                                                                        <form class="form-horizontal" name="designationedit" role="form" action="" method="POST">
                                                                                            <br>
                                    <div class="space-4"></div>
                                    <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Start Date </label>

                                    <div class="col-xs-6 col-sm-6">
                                            <div class="input-group">
                                                <input class="form-control date-picker" name="strdate" value="<?php echo date('Y-m-d'); ?>" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd" />
                                                    <span class="input-group-addon">
                                                            <i class="icon-calendar bigger-110"></i>
                                                    </span>
                                            </div>
                                    </div>
                                    </div>
                                    <div class="space-4"></div>
                                    <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Ending Date </label>

                                    <div class="col-xs-6 col-sm-6">
                                            <div class="input-group">
                                                <input class="form-control date-picker" name="enddate" value="<?php echo date('Y-m-d'); ?>" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd" />
                                                    <span class="input-group-addon">
                                                            <i class="icon-calendar bigger-110"></i>
                                                    </span>
                                            </div>
                                    </div>
                                    </div>                                      


                                                                                            <div class="space-4"></div>

                                                                                            <div class="clearfix form-actions">
                                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                                    <button class="btn btn-info" type="submit" name="search"><i class="icon-ok bigger-110"></i>Edit Now</button>
                                                                                                    &nbsp; &nbsp; &nbsp;
                                                                                                    <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </form>
										</div><!-- /.modal-content -->
									</div><!-- /.modal-dialog -->
								</div><!-- end modal form -->

                                        
                                        <div class="table-header">
                                          <fieldset>
                                                                        <input type="text" class="text-input" style="width: 200px;" id="topsix"  placeholder="Please Search Anything.."  />
                                                                        <span id="topsix-count"></span>
                                                            </fieldset>
                                        </div>
                            <!-- PAGE CONTENT BEGINS -->
							
                                <div class="row" id="printablediv">

                                    <div class="col-xs-12">
                                        <div class="table-responsive">
                                            <h4 align="center">
                                               MSH Online Leave Application Form List
                                                <br>
                                                <small>
                                                    Office and Country: SIAPS Bangladesh
                                                </small>
                                                <br>
                                                <small>
                                                    As of (Date): <?php echo date('M d, Y'); ?>
                                                </small>
                                                <br>
                                                
                                                <small>person who prepared report : 
                                                    <?php 
                                                    $prepar=$obj->SelectAllByID("employee",array("id"=>$input_by));  
                                                    foreach($prepar as $pre):
                                                        echo $pre->name;
                                                    endforeach;
                                                    ?>
                                                </small>    
                                            </h4>
                                            <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="center">S/N</th>
                                                        <th>Employee Name</th>
                                                        <th>Leave Type</th>
                                                        <th>Request Leave Quantity</th>
                                                        <th>Date(Start-End)</th>
                                                        <th>Status</th>
                                                        <th>Manager</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="status">
                                                <?php
                                                    $data=$obj->SelectAllByID("requisation_leaveapplication_form",array("emplid"=>$input_by));
                                                    foreach ($data as $row): 
                                                ?>
                                                        <tr>
                                                            <td class="center"><?php echo $row->id; ?></td>
                                                            <td>
                                                                <a href="ind_onlineleaveapplicationform.php?id=<?php echo $row->id; ?>"><span class="label label-sm label-success">
                                                                <?php
                                                                $bnn=$obj->SelectAllByID("employee",array("id"=>$row->emplid));
                                                                foreach($bnn as $bn):
                                                                    echo $bn->name;
                                                                endforeach;
                                                                ?>
                                                                    
                                                                </span>
                                                                (  <?php echo $row->date; ?> )
                                                                </a>
                                                            </td>
                                                            <td>
                                                                <span class="label label-sm label-success">
                                                                <?php
                                                                $bnn=$obj->SelectAllByID("leave_type",array("id"=>$row->leave_type));
                                                                foreach($bnn as $bn):
                                                                    echo $bn->name;
                                                                endforeach;
                                                                ?>
                                                                </span>
                                                            </td>
                                                            <td class="center"><span class="label label-sm label-success"><?php echo $row->datequantity; ?></span> -( Available Leave<span class="label label-sm label-success">  <?php  echo $obj->leave_quantity(); ?></span>)</td>
                                                            <td><span class="label label-sm label-success"><?php echo $row->startdate; ?></span>-<span class="label label-sm label-success"><?php echo $row->enddate; ?></span></td>
                                                            <td><span class="label label-sm label-info"><?php echo $obj->leave_status($row->status); ?></span></td>
                                                            <td>
                                                                <span class="label label-sm label-success">
                                                                <?php
                                                                $bnn=$obj->SelectAllByID("employee",array("id"=>$row->emplid_m));
                                                                foreach($bnn as $bn):
                                                                    echo $bn->name;
                                                                endforeach;
                                                                ?>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                 <?php endforeach; ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
													</div><!-- /row -->

													<div class="space-12"></div>

													
												</div><!-- /#feed end-------------------------------------------------------------------- -->

												

												
											</div>
										</div>
									</div>
								</div>

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->

				<div class="ace-settings-container" id="ace-settings-container">
					<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
						<i class="icon-cog bigger-150"></i>
					</div>

					<div class="ace-settings-box" id="ace-settings-box">
						<div>
							<div class="pull-left">
								<select id="skin-colorpicker" class="hide">
									<option data-skin="default" value="#438EB9">#438EB9</option>
									<option data-skin="skin-1" value="#222A2D">#222A2D</option>
									<option data-skin="skin-2" value="#C6487E">#C6487E</option>
									<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
								</select>
							</div>
							<span>&nbsp; Choose Skin</span>
						</div>

						<div>
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-navbar" />
							<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
						</div>

						<div>
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-sidebar" />
							<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
						</div>

						<div>
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-breadcrumbs" />
							<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
						</div>

						<div>
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" />
							<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
						</div>

						<div>
							<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-add-container" />
							<label class="lbl" for="ace-settings-add-container">
								Inside
								<b>.container</b>
							</label>
						</div>
					</div>
				</div><!-- /#ace-settings-container -->
			</div><!-- /.main-container-inner -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                 <?php echo $obj->bodyfooter(); ?>


		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="assets/js/jquery.gritter.min.js"></script>
		<script src="assets/js/bootbox.min.js"></script>
		<script src="assets/js/jquery.slimscroll.min.js"></script>
		<script src="assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="assets/js/jquery.hotkeys.min.js"></script>
		

		<!-- ace scripts -->
		<script type="text/javascript">
			jQuery(function($) {
                            
                                                                                        $('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				$('input[name=date-range-picker]').daterangepicker().prev().on(ace.click_event, function(){
					$(this).next().focus();
				});
			
				$('.easy-pie-chart.percentage').each(function(){
				var barColor = $(this).data('color') || '#555';
				var trackColor = '#E2E2E2';
				var size = parseInt($(this).data('size')) || 72;
				$(this).easyPieChart({
					barColor: barColor,
					trackColor: trackColor,
					scaleColor: false,
					lineCap: 'butt',
					lineWidth: parseInt(size/10),
					animate:false,
					size: size
				}).css('color', barColor);
				});
			  
				///////////////////////////////////////////
	
                            $("#topsix").keyup(function(){
                            var filter = $(this).val(), count = 0;
                            $(".topsix").each(function(){
                                if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                                    $(this).fadeOut();
                                } else {
                                    $(this).show();
                                    count++;
                                }
                            });
                            var numberItems = count;
                            $("#topsix-count").text("Result= "+count);
                            });
			});
                        
                                            function addTableRow(table)
                    {
                        var $tr = $(table+' tbody:first').children("tr:last").clone();
                            $tr.find("input[type!='hidden'][name*=first_name],select,button").clone();
                            $tr.find("button[name*='ViewButton']").remove();
                        $(table+' tbody:first').children("tr:last").after($tr);
                    }
		</script>
	</body>

<!-- Mirrored from 192.69.216.111/themes/preview/ace/profile.html by HTTrack Website Copier/3.x [XR&CO'2013], Sun, 10 Nov 2013 12:57:55 GMT -->
</html>

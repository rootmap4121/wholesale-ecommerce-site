<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Service Info</a></li><li class='active'>Service List</li>";
$table="service";
if (isset ($_GET['del'])=="delete") {
                    $delarray=array("id"=>$_GET['id']);
                    if($obj->delete($table,$delarray)==1)
                    { 
                        $errmsg_arr[]= 'Successfully Deleted';
                        $errflag = true;
                        if ($errflag) 
                        {
                            $_SESSION['SMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    } 
                    else 
                    { 
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->          

                                <div class="row">

                                    <div class="col-xs-12">
                                        <h3 class="header smaller lighter blue">Service List</h3>


                                        <div class="table-responsive">
                                            <table aria-describedby="sample-table-2_info" id="sample-table-2" class="table table-striped table-bordered table-hover dataTable">
                                                <thead>
                                                    <tr>
                                                        <th class="center">S/N</th>
                                                        <th>Service Name</th>
                                                        <th>label</th>
                                                        <th>Price</th>
                                                        <th>Delivery Time</th>
                                                        <th>Added In System</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="status">
                                                <?php
                                                if($obj->totalrows($table)!=0)
                                                {
                                                $data=$obj->SelectAllorderBy($table);
                                                $x=1;
                                                foreach ($data as $row): ?>
                                                        <tr>
                                                            <td class="center"><?php echo $x; ?></td>
                                                            <td><?php echo $row->name; ?></td>
                                                            <td>
                                                                <?php echo $obj->SelectAllByVal("label","id",$row->label,"name"); ?>
                                                            </td>
                                                            <td><?php echo $row->price; ?></td>
                                                            <td><span class="label label-sm label-info"><?php echo $row->delivery_time; ?></span></td>
                                                            <td><span class="label label-sm label-danger"><?php echo $row->date; ?></span></td>
                                                            <td>
                                                                    
                                                                    <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                                        <a href="addservice.php?action=pedit&AMP;id=<?php echo $row->id; ?>" role="button" data-toggle="modal" class="green"><i class="icon-edit bigger-130"></i> Edit</a> 
                                                                
                                                                    <a class="red" href="<?php echo $obj->filename(); ?>?del=delete&AMP;id=<?php echo $row->id; ?>"><i class="icon-trash bigger-130"></i> Delete</a>
                                                                </div>
								
                                                                

                                                            </td>
                                                            
                                                        </tr>
                                                 <?php 
                                                 $x++; endforeach; 
                                                }
                                                 ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                 <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				var oTable1 = $('#sample-table-2').dataTable( {
				"aoColumns": [
			      { "bSortable": false },
			      null, null,null, null, null,
				  { "bSortable": false }
				] } );
				
				
				$('table th input:checkbox').on('click' , function(){
					var that = this;
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
						
				});
			
			
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
			})
                        
		</script>
    </body>
</html>

<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Cupon Code Info</a></li><li class='active'>Cupond Code List</li>";
$table="shipping_cost";

function cupon($cupon)
{
    if($cupon==1)
    {
        return "Active";
    }
 else {
        return "Inactive";    
    }
}

if(isset($_POST['submit']))
{
    if(!empty($_POST['cost']))
    {
        $exist_array=array("country"=>$_POST['country'],"state"=>$_POST['state']);
        $insert=array("country"=>$_POST['country'],"state"=>$_POST['state'],"cost"=>$_POST['cost'],"date"=>  date('Y-m-d'),"status"=>1);
        if($obj->exists_multiple($table,$exist_array)==1)
        {
                        $errmsg_arr[]= 'Already Exists';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
        }
        else
        {
            if($obj->insert($table,$insert)==1)
            {
                    $errmsg_arr[]= 'Successfully Saved';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                    }
            }
            else 
             {
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
            }
        }
    }
 else {
                        $errmsg_arr[]= 'Some Field Are Empty';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
    }   
}
elseif (isset ($_POST['edit'])) {
                    //$success="Edit";
                    $updatearray=array("id"=>$_POST['id'],"country"=>$_POST['country'],"state"=>$_POST['state'],"cost"=>$_POST['cost']);
                    if($obj->update($table,$updatearray)==1)
                    { 
                        $errmsg_arr[]= 'Successfully Updated';
                        $errflag = true;
                        if ($errflag) 
                        {
                            $_SESSION['SMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    } 
                    else 
                    { 
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        } 
                        
                    }
    
}
elseif (isset ($_GET['del'])=="delete") {
                    $delarray=array("id"=>$_GET['id']);
                    if($obj->delete($table,$delarray)==1)
                    { 
                        $errmsg_arr[]= 'Successfully Deleted';
                        $errflag = true;
                        if ($errflag) 
                        {
                            $_SESSION['SMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    } 
                    else 
                    { 
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="header smaller lighter blue">Add New Shipping Cost</h3>
                            <!-- PAGE CONTENT BEGINS -->

                            <form class="form-horizontal" name="designationadd" role="form" action="" method="POST">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Add New Country </label>

                                    <div class="col-sm-9">
                                        <select name="country" id="country" class="large-field">
                            <option value=""> --- Please Select --- </option>
                             <option value="United States" selected="selected">United States</option>
                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Add New State </label>

                                    <div class="col-sm-9">
                                        <select name="state" id="state" class="large-field">
                                                        <option value=""> --- Please Select --- </option>
                                                        <option value="Alabama">Alabama</option>
                                                        <option value="Alaska">Alaska</option>
                                                        <option value="American Samoa">American Samoa</option>
                                                        <option value="Arizona">Arizona</option>
                                                        <option value="Arkansas">Arkansas</option>
                                                        <option value="Armed Forces Africa">Armed Forces Africa</option>
                                                        <option value="Armed Forces Americas">Armed Forces Americas</option>
                                                        <option value="Armed Forces Canada">Armed Forces Canada</option>
                                                        <option value="Armed Forces Europe">Armed Forces Europe</option>
                                                        <option value="Armed Forces Middle East">Armed Forces Middle East</option>
                                                        <option value="Armed Forces Pacific">Armed Forces Pacific</option>
                                                        <option value="California">California</option>
                                                        <option value="Colorado">Colorado</option>
                                                        <option value="Connecticut">Connecticut</option>
                                                        <option value="Delaware">Delaware</option>
                                                        <option value="District of Columbia">District of Columbia</option>
                                                        <option value="Federated States Of Micronesia">Federated States Of Micronesia</option>
                                                        <option value="Florida">Florida</option>
                                                        <option value="Georgia">Georgia</option>
                                                        <option value="Guam">Guam</option>
                                                        <option value="Hawaii">Hawaii</option>
                                                        <option value="Idaho">Idaho</option>
                                                        <option value="Illinois">Illinois</option>
                                                        <option value="Indiana">Indiana</option>
                                                        <option value="Iowa">Iowa</option>
                                                        <option value="Kansas">Kansas</option>
                                                        <option value="Kentucky">Kentucky</option>
                                                        <option value="Louisiana">Louisiana</option>
                                                        <option value="Maine">Maine</option>
                                                        <option value="Marshall Islands">Marshall Islands</option>
                                                        <option value="Maryland">Maryland</option>
                                                        <option value="Massachusetts">Massachusetts</option>
                                                        <option value="Michigan">Michigan</option>
                                                        <option value="Minnesota">Minnesota</option>
                                                        <option value="Mississippi">Mississippi</option>
                                                        <option value="Missouri">Missouri</option>
                                                        <option value="Missouri">Montana</option>
                                                        <option value="Nebraska">Nebraska</option>
                                                        <option value="Nevada">Nevada</option>
                                                        <option value="New Hampshire">New Hampshire</option>
                                                        <option value="New Jersey">New Jersey</option>
                                                        <option value="New Mexico">New Mexico</option>
                                                        <option value="New York">New York</option>
                                                        <option value="North Carolina">North Carolina</option>
                                                        <option value="North Dakota">North Dakota</option>
                                                        <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                        <option value="Ohio">Ohio</option>
                                                        <option value="Oklahoma">Oklahoma</option>
                                                        <option value="Oregon">Oregon</option>
                                                        <option value="Palau">Palau</option>
                                                        <option value="Pennsylvania">Pennsylvania</option>
                                                        <option value="Puerto Rico">Puerto Rico</option>
                                                        <option value="Rhode Island">Rhode Island</option>
                                                        <option value="South Carolina">South Carolina</option>
                                                        <option value="South Dakota">South Dakota</option>
                                                        <option value="Tennessee">Tennessee</option>
                                                        <option value="Texas">Texas</option>
                                                        <option value="Utah">Utah</option>
                                                        <option value="Vermont">Vermont</option>
                                                        <option value="Virgin Islands">Virgin Islands</option>
                                                        <option value="Virginia">Virginia</option>
                                                        <option value="Washington">Washington</option>
                                                        <option value="West Virginia">West Virginia</option>
                                                        <option value="Wisconsin">Wisconsin</option>
                                                        <option value="Wyoming">Wyoming</option>
                                                    </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> New Shipping  Cost </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="cost" placeholder="New Shipping Cost" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>


                                <div class="space-4"></div>

                                <div class="clearfix form-actions">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn btn-info" type="submit" name="submit"><i class="icon-ok bigger-110"></i>Save</button>
                                        &nbsp; &nbsp; &nbsp;
                                        <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                    </div>
                                </div>
                            </form>
                                								<div class="hr hr-18 dotted hr-double"></div>

								

								<div class="hr hr-18 dotted hr-double"></div>           

                                <div class="row">

                                    <div class="col-xs-12">
                                        <h3 class="header smaller lighter blue">Shipping Cost List</h3>
                                        <div class="table-header">
                                            Results for "Shipping Cost Detail&rsquo;s" (<?php echo $obj->totalrows($table); ?>)
                                        </div>

                                        <div class="table-responsive">
                                            <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="center">S/N</th>
                                                        <th>Country</th>
                                                        <th>State</th>
                                                        <th>Shipping Cost</th>
                                                        <th>Date</th>
                                                        <th>Status</th>
                                                        <th>Edit </th>
                                                        <th>Delete</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="status">
                                                <?php
                                                $data=$obj->SelectAllorderBy($table);
                                                if(!empty($data))
                                                foreach ($data as $row): ?>
                                                        <tr>
                                                            <td class="center"><?php echo $row->id; ?></td>
                                                            <td><?php echo $row->country; ?></td>
                                                            <td><?php echo $row->state; ?></td>
                                                            <td>$<?php echo number_format($row->cost,2); ?></td>
                                                            <td><span class="label label-sm label-success"><?php echo $row->date; ?></span></td>
                                                            <td><span class="label label-sm label-info"><?php echo cupon($row->status); ?></span></td>
                                                            <td>
                                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                                    <a href="#modal-table<?php echo $row->id; ?>" role="button" data-toggle="modal" class="green"><i class="icon-pencil bigger-130"></i> Edit</a>
                                                                </div>
								<div id="modal-table<?php echo $row->id; ?>" class="modal fade" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header no-padding">
												<div class="table-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
														<span class="white">&times;</span>
													</button>
													Edit Detail
												</div>
											</div>
                                                                                        <!-- /.modal-content -->
                                                                                        <form class="form-horizontal" name="designationedit" role="form" action="" method="POST">
                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Country </label>

                                                                                                <div class="col-sm-9">
                                                                                                    <input type="text" id="form-field-1" name="code" value="<?php echo $row->country; ?>" class="col-xs-10 col-sm-5" />
                                                                                                    <input type="hidden" name="id" value="<?php echo $row->id; ?>"  />
                                                                                                </div>
                                                                                            </div>
                                                                                            
                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> State </label>

                                                                                                <div class="col-sm-9">
                                                                                                    <input type="text" id="form-field-1" name="state" value="<?php echo $row->state; ?>" class="col-xs-10 col-sm-5" />
                                                                                                </div>
                                                                                            </div>
                                                                                            
                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Cost </label>

                                                                                                <div class="col-sm-9">
                                                                                                    <input type="text" id="form-field-1" name="cost" value="<?php echo $row->cost; ?>" class="col-xs-10 col-sm-5" />
                                                                                                </div>
                                                                                            </div>


                                                                                            <div class="space-4"></div>

                                                                                            <div class="clearfix form-actions">
                                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                                    <button class="btn btn-info" type="submit" name="edit"><i class="icon-ok bigger-110"></i>Edit Now</button>
                                                                                                    &nbsp; &nbsp; &nbsp;
                                                                                                    <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </form>
										</div><!-- /.modal-content -->
									</div><!-- /.modal-dialog -->
								</div><!-- end modal form -->


                                                            </td>
                                                            <td>
                                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                                    <a class="red" href="<?php echo $obj->filename(); ?>?del=delete&AMP;id=<?php echo $row->id; ?>"><i class="icon-trash bigger-130"></i> Delete</a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                 <?php endforeach; ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {

			})
		</script>
    </body>
</html>

<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Product Info</a></li><li class='active'>Add New Product</li>";
$table="service";
if(isset($_POST['submit']))
{
    if(!empty($_POST['name']))
    {

            $insert=array("name"=>$_POST['name'],"label"=>$_POST['label'],"price"=>$_POST['price'],"description"=>$_POST['description'],"delivery_time"=>$_POST['delivery_time'],"date"=>date('Y-m-d'),"status"=>1);
            if($obj->insert($table,$insert)==1)
            {
                    
                    $errmsg_arr[]= 'Successfully Saved';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./service.php");
                        exit();
                    }
            }
            else 
             {
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
            }
    }
 else {
        $errmsg_arr[]= 'Some Field is Empty';
        $errflag = true;
        if ($errflag) {
            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
            session_write_close();
            header("location: ./".$obj->filename());
            exit();
        }
    }   
}
elseif (isset ($_POST['edit'])) {
                    //$success="Edit";
                    $updatearray=array("id"=>$_POST['id'],"name"=>$_POST['name'],"label"=>$_POST['label'],"description"=>$_POST['description'],"price"=>$_POST['price'],"delivery_time"=>$_POST['delivery_time'],"date"=>date('Y-m-d'),"status"=>1);

                    if($obj->update($table,$updatearray)==1)
                    { 
                        $errmsg_arr[]= 'Successfully Updated';
                        $errflag = true;
                        if ($errflag) 
                        {
                            $_SESSION['SMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./service.php");
                            exit();
                        }
                        
                    } 
                    else 
                    { 
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        } 
                        
                    }
    
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
        <script src="ckeditor/ckeditor.js"></script>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="header smaller lighter blue">Add New Service</h3>
                            <!-- PAGE CONTENT BEGINS -->
                            <?php
                            if(@$_GET['action']=='pedit')
                            { 
                                $pdata=$obj->SelectAllByID($table,array("id"=>$_GET['id']));
                                if(!empty($pdata))
                                    foreach ($pdata as $prow){
                             ?>
                                <form class="form-horizontal" name="addproduct" enctype="multipart/form-data" role="form" action="" method="POST">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Service Name </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" value="<?php echo $prow->name; ?>" name="name" placeholder="Product Name" class="col-xs-10 col-sm-5" />
                                        <input type="hidden" id="form-field-1" value="<?php echo $prow->id; ?>" name="id" placeholder="Product Name" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                               
                                 
                              
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Product Category </label>

                                    <div class="col-sm-9">
                                        <select name="label">
                                            <option value="">Select Service Label</option>
                                            <?php $category=$obj->SelectAll("label"); foreach($category as $cat): ?>
                                            <option <?php if($prow->label==$cat->id){ echo "selected='selected'"; } ?>  value="<?php echo $cat->id; ?>"><?php echo $cat->name; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Service Price </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" value="<?php echo $prow->price; ?>" name="price" placeholder="Price" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                      
                               
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Delivery Time </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" value="<?php echo $prow->delivery_time; ?>" name="delivery_time" placeholder="Quantity" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Desciption </label>

                                    <div class="col-sm-9">
                                        <textarea name="description"  cols="70" style="width: 700px;" id="description"  rows="10"><?php echo $prow->description; ?></textarea>
                                    		<script>

                                                    // This call can be placed at any point after the
                                                    // <textarea>, or inside a <head><script> in a
                                                    // window.onload event handler.

                                                    CKEDITOR.replace( 'description', {
                                                            extraPlugins: 'magicline',	// Ensure that magicline plugin, which is required for this sample, is loaded.
                                                            allowedContent: true		// Switch off the ACF, so very complex content created to
                                                                                                                    // show magicline's power isn't filtered.
                                                    } );

                                            </script>  
                                            <br>
                                    </div>
                                </div>
                                

                                <div class="space-4"></div>

                                <div class="clearfix form-actions">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn btn-info" type="submit" name="edit"><i class="icon-ok bigger-110"></i>Update Now</button>
                                        &nbsp; &nbsp; &nbsp;
                                        <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                    </div>
                                </div>
                            </form>
                            <?php 
                                    }
                            
                                    }
                            else
                            {
                            ?>
                            <form class="form-horizontal" name="addproduct" enctype="multipart/form-data" role="form" action="" method="POST">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Service Name </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="name" placeholder="Service Name" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                 
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Service Label </label>

                                    <div class="col-sm-9">
                                        <select name="label">
                                            <option value="">Select Label</option>
                                            <?php $category=$obj->SelectAll("label"); foreach($category as $cat): ?>
                                            <option value="<?php echo $cat->id; ?>"><?php echo $cat->name; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                              
                                
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Product Price </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="price" placeholder="Price" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Delivery Time </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="delivery_time" placeholder="Delivery Time" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                               
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Desciption </label>

                                    <div class="col-sm-9">
                                        <textarea name="description"  cols="70" style="width: 700px;" id="description"  rows="10"></textarea>
                                    		<script>

                                                    // This call can be placed at any point after the
                                                    // <textarea>, or inside a <head><script> in a
                                                    // window.onload event handler.

                                                    CKEDITOR.replace( 'description', {
                                                            extraPlugins: 'magicline',	// Ensure that magicline plugin, which is required for this sample, is loaded.
                                                            allowedContent: true		// Switch off the ACF, so very complex content created to
                                                                                                                    // show magicline's power isn't filtered.
                                                    } );

                                            </script>  
                                            <br>
                                    </div>
                                </div>
                                
                                
                                

                                <div class="space-4"></div>

                                <div class="clearfix form-actions">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn btn-info" type="submit" name="submit"><i class="icon-ok bigger-110"></i>Save</button>
                                        &nbsp; &nbsp; &nbsp;
                                        <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                    </div>
                                </div>
                            </form>
                            <?php } ?>

                                
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
            include('class/footer.php');
            ?>


                <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->

			<script type="text/javascript">
			jQuery(function($) {
				  
			})
		</script>
    </body>
</html>

<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>User Info</a></li><li class='active'>Admin List</li>";
$table="employee";
$w=80; $h=100; 
$thumb="emp/";

if (isset ($_POST['edit'])) {
                    //$success="Edit";
                    if($_POST['expassword']!=$_POST['password'])
                    {
                        $password=md5($_POST['password']);
                    }
                    else
                    {
                       $password=$_POST['expassword']; 
                    }
                    $updatearray=array("id"=>$_POST['id'],"name"=>$_POST['name'],"username"=>$_POST['username'],"password"=>$password,"gender"=>$_POST['gender'],"dob"=>$_POST['dob'],"contactnumber"=>$_POST['contact'],"address"=>$_POST['address']);
                    if($obj->update($table,$updatearray)==1)
                    { 
                        $errmsg_arr[]= 'Successfully Saved';
                        $errflag = true;
                        if ($errflag) 
                        {
                            $_SESSION['SMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }   
                    } 
                    else 
                    { 
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        } 
                        
                    }
    
}
elseif (isset ($_GET['del'])=="delete") {
                        $delarray=array("id"=>$_GET['id']);
                        if($obj->delete($table,$delarray)==1)
                        { 
                            $errmsg_arr[]= 'Successfully Deleted';
                            $errflag = true;
                            if ($errflag) 
                            {
                                $_SESSION['SMSG_ARR'] = $errmsg_arr;
                                session_write_close();
                                header("location: ./".$obj->filename());
                                exit();
                            }

                        } 
                        else 
                        { 
                            $errmsg_arr[]= 'Failed';
                            $errflag = true;
                            if ($errflag) {
                                $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                                session_write_close();
                                header("location: ./".$obj->filename());
                                exit();
                            }
                        }
                  
                    
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">

                                <div class="row">

                                    <div class="col-xs-12">
                                        <h3 class="header smaller lighter blue">Employee List <span style="float: right;"><a href="#" style="text-decoration: none;"  onclick="javascript:printDiv('printablediv')"><i class="icon-print"></i> Print Employee List</a></span></h3>
                                        <div class="table-header">
                                            Total Employee (<?php echo $obj->totalrows("employee"); ?>)
                                        </div>

                                        <div class="table-responsive" id="printablediv">
                                            <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="center">S/N</th>
                                                        <th> Full Name </th>
                                                        <th> Dob </th>
                                                        <th> Username </th>
                                                        <th> System Joinging Date </th>
                                                        <th> Edit </th>
                                                        <th> Delete </th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                <?php
                                                $data=$obj->SelectAllorderBy($table);
                                                $x=1;
                                                if(!empty($data))
                                                foreach ($data as $row): ?>
                                                        <tr>
                                                            <td class="center"><?php echo $x; ?></td>
                                                            <td><?php echo $row->name; ?></td>
                                                            <td><span class="label label-sm label-success"><?php echo $row->dob; ?></span></td>
                                                            <td><span class="label label-sm label-danger"><?php echo $row->username; ?></span></td>
                                                            <td><span class="label label-sm label-danger"><?php echo $row->date; ?></span></td>
                                                            
                                                            <td>
                                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                                    <a href="#modal-table<?php echo $row->id; ?>" role="button" data-toggle="modal" class="green"><i class="icon-pencil bigger-130"></i> Edit</a>
                                                                </div>
								<div id="modal-table<?php echo $row->id; ?>" class="modal fade" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header no-padding">
												<div class="table-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
														<span class="white">&times;</span>
													</button>
													Update Admin Detail
												</div>
											</div>
                                                                                        <!-- /.modal-content -->
                                                                                        <form class="form-horizontal" name="employeelist" role="form" action="" method="POST">
                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Full Name </label>

                                                                                                <div class="col-sm-9">
                                                                                                    <input type="text" id="form-field-1" name="name" value="<?php echo $row->name; ?>" class="col-xs-10 col-sm-5" />
                                                                                                    <input type="hidden" name="id" value="<?php echo $row->id; ?>"  />
                                                                                                </div>
                                                                                            </div>
                                                                                            


                                                                                            

                                                                                            

                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Gender </label>

                                                                                                <div class="col-sm-9">
                                                                                                    <select class="width-10" name="gender">
                                                                                                        <option value="">Select Sex</option>
                                                                                                        <option  <?php if($row->gender==1): ?> selected="selected" <?php endif; ?>  value="1">Male</option>
                                                                                                        <option <?php if($row->gender==2): ?> selected="selected" <?php endif; ?> value="2">Female</option>
                                                                                                     </select>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Date of Birth </label>

                                                                                                <div class="col-sm-9">
                                                                                                    <input type="text" id="form-field-1" name="dob" placeholder="Year-Month-Date" value="<?php echo $row->dob; ?>" class="col-xs-10 col-sm-5" />
                                                                    
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Contact Number </label>

                                                                                                <div class="col-sm-9">
                                                                                                    <input type="text" id="form-field-1" name="contact" value="<?php echo $row->contactnumber; ?>"  placeholder="Contact Number " class="col-xs-10 col-sm-5" />
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Address </label>

                                                                                                <div class="col-sm-9">
                                                                                                    <input type="text" id="form-field-1" name="address" value="<?php echo $row->address; ?>" placeholder="Address " class="col-xs-10 col-sm-5" />
                                                                                                </div>
                                                                                            </div>
                                                                                            
                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> User Name </label>

                                                                                                <div class="col-sm-9">
                                                                                                    <input type="text" id="form-field-1" name="username" value="<?php echo $row->username; ?>" placeholder="User Name " class="col-xs-10 col-sm-5" />
                                                                                                </div>
                                                                                            </div>
                                                                                            
                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Password </label>

                                                                                                <div class="col-sm-9">
                                                                                                    <input type="password" id="form-field-1" name="password" value="<?php echo $row->password; ?>" placeholder="Password " class="col-xs-10 col-sm-5" />
                                                                                                    <input type="hidden" id="form-field-1" name="expassword" value="<?php echo $row->password; ?>" />
                                                                                                    
                                                                                                </div>
                                                                                            </div>

                                                                                            







                                                                                            <div class="space-4"></div>

                                                                                            <div class="clearfix form-actions">
                                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                                    <button class="btn btn-info" type="submit" name="edit"><i class="icon-ok bigger-110"></i>Update Now</button>
                                                                                                    &nbsp; &nbsp; &nbsp;
                                                                                                    <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </form>
										</div><!-- /.modal-content -->
									</div><!-- /.modal-dialog -->
								</div><!-- end modal form -->


                                                            </td>
                                                            <td>
                                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                                    <a class="red" href="<?php echo $obj->filename(); ?>?del=delete&AMP;id=<?php echo $row->id; ?>&AMP;status=<?php echo $row->status; ?>"><i class="icon-trash bigger-130"></i> Delete</a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                 <?php 
                                                 $x++;
                                                 endforeach;
                                                 ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                 <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {

			})
		</script>
    </body>
</html>

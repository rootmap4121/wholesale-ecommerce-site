<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Admin Info</a></li><li class='active'>Admin List</li>";
$table="employee";
$w=80; $h=100; $thumb="emp/";
if(isset($_POST['submit']))
{
    if(!empty($_POST['name']) && !empty($_POST['username']) && !empty($_POST['password']))
    {
        $exist_array=array("name"=>$_POST['name']);
        $dob=$_POST['year'].'-'.$_POST['month'].'-'.$_POST['day'];
        $joiningdate=$_POST['year1'].'-'.$_POST['month1'].'-'.$_POST['day1'];
        
        $insert=array("name"=>$_POST['name'],"gender"=>$_POST['gender'],"dob"=>$dob,"contactnumber"=>$_POST['contact'],"address"=>$_POST['address'],"username"=>$_POST['username'],"password"=>md5($_POST['password']),"date"=>  date('Y-m-d'),"status"=>2);
        if($obj->exists($table,$exist_array)==1)
        {
                        $errmsg_arr[]= 'Already Exist';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
        }
        else
        {
            if($obj->insert($table,$insert)==1)
            {
                    $errmsg_arr[]= 'Successfully Saved';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./employeelist.php");
                        exit();
                    }
            }
            else 
             {
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
            }
        }
    }
 else 
     {
                        $errmsg_arr[]= 'Failed,Some Fields is Empty';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
     }
}
elseif (isset ($_GET['del'])=="delete") {
                    if($_GET['status']=1)
                    {
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                    }
                    else 
                    {
                    
                    $delarray=array("id"=>$_GET['id']);
                    if($obj->delete($table,$delarray)==1)
                    { 
                        $errmsg_arr[]= 'Successfully Deleted';
                        $errflag = true;
                        if ($errflag) 
                        {
                            $_SESSION['SMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        } 
                        
                    } 
                    else 
                    { 
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    }
                    
                    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="header smaller lighter blue">Add New Admin</h3>
                            <!-- PAGE CONTENT BEGINS -->

                            <form class="form-horizontal" name="empadd"  enctype="multipart/form-data" role="form" action="" method="POST">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Admin Name* </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="name" placeholder="Admin Full Name" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Gender </label>

                                    <div class="col-sm-9">
                                        <select class="width-10" name="gender">
                                            <option value="">Select Sex</option>
                                            <option value="1">Male</option>
                                            <option value="2">Female</option>
                                         </select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Date of Birth </label>

                                    <div class="col-sm-9">
                                        <select class="width-10" name="day">
                                            <option value="">Select Day</option>
                                            <?php for ($i = 1; $i <= 31; $i++): ?>
                                                <option value="<?php echo $obj->dmy($i); ?>"><?php echo $obj->dmy($i); ?></option>
                                            <?php endfor; ?>
                                        </select>
                                        
                                        <select class="width-10" name="month">
                                            <option value="">Select Month</option>
                                            <?php for ($j = 1; $j <= 12; $j++): ?>
                                                <option value="<?php echo $obj->dmy($j); ?>"><?php echo $obj->dmy($j); ?></option>
                                            <?php endfor; ?>
                                        </select>
                                        
                                        <select class="width-10" name="year">
                                            <option value="">Select Year</option>
                                            <?php for ($k = date('Y'); $k >= date('Y') - 100; $k--) : ?>
                                                <option value="<?php echo $obj->dmy($k); ?>"><?php echo $obj->dmy($k); ?></option>
                                            <?php endfor; ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Contact Number </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="contact" placeholder="Contact Number " class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Address </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="address" placeholder="Address " class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> User Name* </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="username" placeholder="User Name " class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Password* </label>

                                    <div class="col-sm-9">
                                        <input type="password" id="form-field-1" name="password" placeholder="Password " class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                

                                <div class="space-4"></div>

                                <div class="clearfix form-actions">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn btn-info" type="submit" name="submit"><i class="icon-ok bigger-110"></i>Save</button>
                                        &nbsp; &nbsp; &nbsp;
                                        <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                    </div>
                                </div>
                            </form>
                                								<div class="hr hr-18 dotted hr-double"></div>

								

                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                 <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {

			})
		</script>
    </body>
</html>

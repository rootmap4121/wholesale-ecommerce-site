<?php
if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
    foreach($_SESSION['ERRMSG_ARR'] as $msg) 
        {
?>
<span class="label label-warning"><i class="icon-warning-sign bigger-120"></i> <?php echo $msg;  ?> </span>
<?php
        }
    unset($_SESSION['ERRMSG_ARR']);
}
?>



<?php
if( isset($_SESSION['SMSG_ARR']) && is_array($_SESSION['SMSG_ARR']) && count($_SESSION['SMSG_ARR']) >0 ) {
        foreach($_SESSION['SMSG_ARR'] as $msg) 
            {
?>
<span class="label label-success"><i class="icon-check bigger-120"></i> <?php echo $msg;  ?> </span>
<?php
            }
        unset($_SESSION['SMSG_ARR']);
}
?>


<?php if(!empty($error)) { ?>
<span class="label label-warning"><i class="icon-warning-sign bigger-120"></i> <?php echo $error;  ?> </span>

<?php  } ?>



<?php if(!empty($success)) { ?>
<span class="label label-success"><i class="icon-check bigger-120"></i> <?php echo $success;  ?> </span>
<?php } ?>
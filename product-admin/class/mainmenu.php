<ul class="nav nav-list">
    <li>
        <a href="index.php">
            <i class="icon-dashboard"></i>
            <span class="menu-text">Back To Dashboard </span>
        </a>
    </li>
    <li>
        <a href="employee.php">
            <i class="icon-user"></i>
            <span class="menu-text">Add New Admin </span>
        </a>
    </li>
    <li>
        <a href="employeelist.php">
            <i class="icon-user-md"></i>
            <span class="menu-text">Admin List </span>
        </a>
    </li>
    <li>
        <a href="category.php">
            <i class="icon-circle"></i>
            <span class="menu-text">List of Category </span>
        </a>
    </li>
    <li>
        <a href="subcategory.php">
            <i class="icon-circle-blank"></i>
            <span class="menu-text">List of Sub - Category </span>
        </a>
    </li>
    <li>
        <a href="addproduct.php">
            <i class="icon-tag"></i>
            <span class="menu-text">Add New Product </span>
        </a>
    </li>
    <li>
        <a href="product.php">
            <i class="icon-tags"></i>
            <span class="menu-text">List of Product </span>
        </a>
    </li>
    <li>
        <a href="label.php">
            <i class="icon-compass"></i>
            <span class="menu-text">Add New Label </span>
        </a>
    </li>
    <li>
        <a href="addservice.php">
            <i class="icon-yen"></i>
            <span class="menu-text">Add New Service </span>
        </a>
    </li>
    <li>
        <a href="service.php">
            <i class="icon-weibo"></i>
            <span class="menu-text">List of Service </span>
        </a>
    </li>
    <li>
        <a href="service_order.php">
            <i class="icon-ok-sign"></i>
            <span class="menu-text">List of Service Order </span>
        </a>
    </li>
        <li>
        <a href="product_order.php">
            <i class="icon-ok-sign"></i>
            <span class="menu-text">List of Product Order </span>
        </a>
    </li>
        <li>
        <a href="customer.php">
            <i class="icon-ok-sign"></i>
            <span class="menu-text">Customer List </span>
        </a>
    </li>
        <li>
            <a href="cuponcode.php">
            <i class="icon-ok-sign"></i>
            <span class="menu-text">Cupon Code List </span>
        </a>
    </li>
    
    <!--<li>
            <a href="shippingcost.php">
            <i class="icon-ok-sign"></i>
            <span class="menu-text">Shipping Cost List </span>
        </a>
    </li>-->
    
    <li>
        <a href="shipping_method_option.php">
            <i class="icon-ok-sign"></i>
            <span class="menu-text">Delivery Option </span>
        </a>
    </li>
    
    <li>
        <a href="shipping_method.php">
            <i class="icon-ok-sign"></i>
            <span class="menu-text">Shipping Method </span>
        </a>
    </li>
    
    <li>
                <a href="product_organise.php">
            <i class="icon-ok-sign"></i>
            <span class="menu-text">Organize Site Product </span>
        </a>
    </li>
</ul><!-- /.nav-list -->

<?php
class db_class {

    public function open() {
        $con = mysqli_connect("localhost", "root", "", "igeek");
        return $con;
    }

    function baseUrl($suffix = '') {
        $protocol = strpos($_SERVER['SERVER_SIGNATURE'], '443') !== false ? 'https://' : 'http://';
        $web_root = $protocol . $_SERVER['HTTP_HOST'] . "/" . "Dropbox/odesk/template2/product-admin/";
        $suffix = ltrim($suffix, '/');
        return $web_root . trim($suffix);
    }

    
    public function close($con) {
        mysqli_close($con);
    }

    function order_status($status)
    {
        if($status==1)
        {
            return "Pending";
        }
        elseif($status==2) 
        {
            return "Complete";
        }
        elseif($status==0) 
        {
            return "pending";
        }
    }
    
    function method($status)
    {
        if($status==1)
        {
            return "Paypal";
        }
        elseif($status==2) 
        {
            return "Home delivery";
        }
        elseif($status==0) 
        {
            return "pending";
        }
    }
    
    /**
     * Insert query for Object
     * @param type $object
     * @param type $object_array
     * @return string/Exception
     */
    function insert($object, $object_array) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        foreach ($object_array as $col => $val) {
            if ($count++ != 0)
                $fields .= ', ';
            $col = mysqli_real_escape_string($con, $col);
            $val = mysqli_real_escape_string($con, $val);
            $fields .= "`$col` = '$val'";
        }
        $query = "INSERT INTO `$object` SET $fields";
        if (mysqli_query($con, $query)) {
            $this->close($con);
            return 1;
        } else {
            return 0;
        }
    }
    
    
    function lastid($object) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM `$object` ORDER BY id DESC LIMIT 1";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        }
       
    }
    
    function redirect($link)
    {
       echo "<script>location.href='$link'</script>";
    }
    function login($username, $password) {
        $count = 0;
        $fields = '';
        $con = $this->open();

        $fields = "username='$username' and password='$password'";

        $query = "SELECT * FROM `employee` WHERE $fields";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            $this->close($con);
            if ($count == 1) {
                return $count;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }

    /**
     * if the object is exists
     * @param type $object
     * @param type $object_array
     * @return int
     */
    function exists($object, $object_array) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        if (count($object_array) <= 1) {
            foreach ($object_array as $col => $val) {
                if ($count++ != 0)
                    $fields .= ', ';
                $col = mysqli_real_escape_string($con, $col);
                $val = mysqli_real_escape_string($con, $val);
                $fields .= "`$col` = '$val'";
            }
        }
        $query = "SELECT * FROM `$object` WHERE $fields";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            $this->close($con);
            if ($count == 1) {
                return 1;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
    function texists($object, $object_array) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        if (count($object_array) <= 1) {
            foreach ($object_array as $col => $val) {
                if ($count++ != 0)
                    $fields .= ', ';
                $col = mysqli_real_escape_string($con, $col);
                $val = mysqli_real_escape_string($con, $val);
                $fields .= "`$col` = '$val'";
            }
        }
        $query = "SELECT * FROM `$object` WHERE $fields";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            $this->close($con);
            if ($count >= 1) {
                return $count;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
    function existsnew($object, $object_array) {
        $con_key_from_arr = array_keys($object_array);
        $key = $con_key_from_arr[0];
        $value = array_shift($object_array);
        $fields = array();
        $con = $this->open();
        foreach ($object_array as $field => $val) {
            $fields[] = "$field = '" . mysqli_real_escape_string($con, $val) . "'";
        }
        
        $query = "SELECT * FROM `$object` WHERE " . join('AND ', $fields) . "";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            $this->close($con);
            if ($count == 1) {
                return 1;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
    
    function existsnewtotal($object, $object_array) {
        $con_key_from_arr = array_keys($object_array);
        $key = $con_key_from_arr[0];
        $value = array_shift($object_array);
        $fields = array();
        $con = $this->open();
        foreach ($object_array as $field => $val) {
            $fields[] = "$field = '" . mysqli_real_escape_string($con, $val) . "'";
        }
        
        $query = "SELECT * FROM `$object` WHERE " . join('AND ', $fields) . "";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            $this->close($con);
            if ($count >= 1) {
                return $count;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
    function SelectAllByID_Multiple($object, $object_array) 
    {
        $count = 0; 
        $fields = ''; 
        $con = $this->open(); 
            foreach ($object_array as $col => $val) 
            { 
                if ($count++ != 0) 
                $fields .= ' AND '; 
            $col = mysqli_real_escape_string($con, $col); 
            $val = mysqli_real_escape_string($con, $val); 
            $fields .= "`$col` = '$val' "; 
            } 
            
        $query = "SELECT * FROM `$object` WHERE $fields";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
    function exists_multiple($object, $object_array) {
        $count = 0; 
        $fields = ''; 
        $con = $this->open(); 
            foreach ($object_array as $col => $val) 
            { 
                if ($count++ != 0) 
                $fields .= ' AND '; 
            $col = mysqli_real_escape_string($con, $col); 
            $val = mysqli_real_escape_string($con, $val); 
            $fields .= "`$col` = '$val' "; 
            } 
            $query = "SELECT * FROM `$object` WHERE $fields"; 
            $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            $this->close($con);
            if ($count !=0) {
                return $count;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
    
    function SelectAllByVal($object,$field,$fval,$fetch)
    {
        $link=$this->open();
        $sql="SELECT `$fetch` FROM `$object` WHERE `$field`='$fval'";
        $result=mysqli_query($link, $sql);
        if($result)
        {
            $row=  mysqli_fetch_array($result);
            return $row[$fetch];
        }
    }
	
	function SelectAllByVal2($object,$field,$fval,$field2,$fval2,$fetch)
    {
        $link=$this->open();
        $sql="SELECT `$fetch` FROM `$object` WHERE `$field`='$fval' AND `$field2`='$fval2'";
        $result=mysqli_query($link, $sql);
        if($result)
        {
            $row=  mysqli_fetch_array($result);
            return $row[$fetch];
        }
    }
    
        function SelectAllByIDDouble($object, $object_array) {
        $con_key_from_arr = array_keys($object_array);
        $key = $con_key_from_arr[0];
        $value = array_shift($object_array);
        $fields = array();
        $con = $this->open();
        
        foreach ($object_array as $field => $val) {
            $fields[] = "$field = '" . mysqli_real_escape_string($con, $val) . "'";
        }
        $query = "SELECT * FROM `$object` WHERE " . join('AND ', $fields) . "";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
    
    function existsbyid($object, $object_array) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        if (count($object_array) <= 1) {
            foreach ($object_array as $col => $val) {
                if ($count++ != 0)
                    $fields .= ', ';
                $col = mysqli_real_escape_string($con, $col);
                $val = mysqli_real_escape_string($con, $val);
                $fields .= "`$col` = '$val'";
            }
        }
        $query = "SELECT * FROM `$object` WHERE $fields";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            $this->close($con);
            if ($count != 0) {
                return $count;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
    function totalrows($object) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM `$object`";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            $this->close($con);
                return $count;
            } else {
            $this->close($con);
            return 0;
        }
    }
    
        function totalrowsbyID($object,$id) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM `$object` WHERE id='$id'";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            $this->close($con);
                return $count;
            } else {
            $this->close($con);
            return 0;
        }
    }

            function totalrowsbyDID($object,$field,$id) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM `$object` WHERE `$field`='$id'";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            $this->close($con);
                return $count;
            } else {
            $this->close($con);
            return 0;
        }
    }
    
    /**
     * Select all the objects
     * @param type $object
     * @return array
     */
    function SelectAll($object) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM `$object`";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        }
    }
    
        function SelectAllOrder($object,$order) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM `$object` ORDER BY $order";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        }
    }
    
        function SelectAll_sdate($object,$field,$date) {
            $count = 0;
            $fields = '';
            $con = $this->open();
            $query = "SELECT * FROM `$object` WHERE `$field`='$date'";
            $result = mysqli_query($con, $query);
            if ($result) {
                $count = mysqli_num_rows($result);

                if ($count >= 1) {
                    //$object[]=array();
                    while ($rows = $result->fetch_object()) {
                        $objects[] = $rows;
                    }
                    $this->close($con);
                    return $objects;
                }
            }
        }
        
        function SelectAll_ddate($object,$field,$startdate,$enddate) {
            $count = 0;
            $fields = '';
            $con = $this->open();
            $query = "SELECT * FROM `$object` WHERE `$field` >= '$startdate' AND `$field` <= '$enddate'";
            $result = mysqli_query($con, $query);
            if ($result) {
                $count = mysqli_num_rows($result);

                if ($count >= 1) {
                    //$object[]=array();
                    while ($rows = $result->fetch_object()) {
                        $objects[] = $rows;
                    }
                    $this->close($con);
                    return $objects;
                }
            }
        }
    
        function SelectAllorderBy($object) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM `$object` ORDER BY id DESC";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        }
    }
    
    function title()
    {
        return "<title>Product Mamagement System</title>";
    }
    
    function softwarename()
    {
        return "Product Management System";
    }
    
    function filename()
    {
        return basename($_SERVER['PHP_SELF']);
    }
    
    function leave_status($status)
    {
        if($status==1)
        {
            return "Pending";
        }
        elseif($status==2) 
        {
            return "Acceped By Manager";
        }
        elseif($status==3) 
        {
            return "Rejected By Manager";
        }
    }
    
    function leave_quantity()
    {
        $con=$this->open();
        $sql="SELECT * FROM requisation_leaveapplication WHERE year='".date('Y')."'";
        $result=  mysqli_query($con, $sql);
        if($result)
        {
            $count=  mysqli_num_rows($result);
            if($count!=0)
            {
                $objects=0;
                    while($rows=mysqli_fetch_array($result)):
                        $objects+=$rows['quantity'];
                    endwhile;
               return $objects;
            }
            else 
            {
                return 0;
            }
        }
        else 
        {
            return 0;
        }
    }
    
    
    function emp_leave_quantity($emp)
    {
        $con=$this->open();
        $sql="SELECT * FROM requisation_leaveapplication_form WHERE emplid='$emp'";
        $result=  mysqli_query($con, $sql);
        if($result)
        {
            $count=  mysqli_num_rows($result);
            if($count!=0)
            {
                $objects=0;
                    while($rows=mysqli_fetch_array($result)):
                        $objects+=$rows['datequantity'];
                    endwhile;
               return $objects;
            }
            else 
            {
                return 0;
            }
        }
        else 
        {
            return 0;
        }
    }
    
    
    function SelectAllByID_Multiple_site($object, $object_array,$pid,$order,$limit) 
    {
        $count = 0; 
        $fields = ''; 
        $con = $this->open(); 
            foreach ($object_array as $col => $val) 
            { 
                if ($count++ != 0) 
                $fields .= ' AND '; 
            $col = mysqli_real_escape_string($con, $col); 
            $val = mysqli_real_escape_string($con, $val); 
            $fields .= "`$col` = '$val' "; 
            } 
            
        $query = "SELECT * FROM `$object` WHERE $fields AND id!='$pid' ORDER BY id $order LIMIT $limit";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
        function SelectAllByID_site($object, $object_array,$order,$limit) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        if (count($object_array) <= 1) {
            foreach ($object_array as $col => $val) {
                if ($count++ != 0)
                    $fields .= ', ';
                $col = mysqli_real_escape_string($con, $col);
                $val = mysqli_real_escape_string($con, $val);
                $fields .= "`$col` = '$val'";
            }
        }
        $query = "SELECT * FROM `$object` WHERE $fields ORDER BY id $order LIMIT $limit";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
	    
    
    function SelectAllLimit($object,$order,$limit) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM `$object` ORDER BY id $order LIMIT $limit";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
    
    
    /**
     * Select object by ID
     * @param type $object
     * @param type $object_array
     * @return int
     */
    function SelectAllByID($object, $object_array) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        if (count($object_array) <= 1) {
            foreach ($object_array as $col => $val) {
                if ($count++ != 0)
                    $fields .= ', ';
                $col = mysqli_real_escape_string($con, $col);
                $val = mysqli_real_escape_string($con, $val);
                $fields .= "`$col` = '$val'";
            }
        }
        $query = "SELECT * FROM `$object` WHERE $fields";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    

    /**
     * Delete the object from database
     * @param type $object
     * @param type $object_array
     * @return string|\Exception
     */
    function delete($object, $object_array) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        if (count($object_array) <= 1) {
            foreach ($object_array as $col => $val) {
                if ($count++ != 0)
                    $fields .= ', ';
                $col = mysqli_real_escape_string($con, $col);
                $val = mysqli_real_escape_string($con, $val);
                $fields .= "`$col` = '$val'";
            }
        }
        $query = "Delete FROM `$object` WHERE $fields";
        if (mysqli_query($con, $query)) {

            $this->close($con);
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Delete the object
     * @param type $object
     * @param type $object_array
     */
    function update($object, $object_array) {
        $con_key_from_arr = array_keys($object_array);
        $key = $con_key_from_arr[0];
        $value = array_shift($object_array);
        $fields = array();
        $con = $this->open();
        foreach ($object_array as $field => $val) {
            $fields[] = "$field = '" . mysqli_real_escape_string($con, $val) . "'";
        }
        $query = "UPDATE `$object` SET " . join(', ', $fields) . " WHERE $key = '$value'";

        if (mysqli_query($con, $query)) {

            $this->close($con);
            return 1;
        } else {
            return 0;
        }
    }
    
    function quantity($object, $object_array,$quantity) {
        $con_key_from_arr = array_keys($object_array);
        $key = $con_key_from_arr[0];
        $value = array_shift($object_array);
        $fields = array();
        $con = $this->open();
        foreach ($object_array as $field => $val) {
            $fields[] = "$field = '" . mysqli_real_escape_string($con, $val) . "'";
        }
        $query = "UPDATE `$object` SET quantity=quantity+'$quantity' WHERE $key = '$value'";

        if (mysqli_query($con, $query)) {

            $this->close($con);
            return 1;
        } else {
            return 0;
        }
    }
    
    function quantityd($object, $object_array,$quantity) {
        $con_key_from_arr = array_keys($object_array);
        $key = $con_key_from_arr[0];
        $value = array_shift($object_array);
        $fields = array();
        $con = $this->open();
        foreach ($object_array as $field => $val) {
            $fields[] = "$field = '" . mysqli_real_escape_string($con, $val) . "'";
        }
        $query = "UPDATE `$object` SET quantity=quantity-'$quantity' WHERE $key = '$value'";

        if (mysqli_query($con, $query)) {

            $this->close($con);
            return 1;
        } else {
            return 0;
        }
    }
    
    function amount_incre($object, $object_array,$field,$amount) {
        $con_key_from_arr = array_keys($object_array);
        $key = $con_key_from_arr[0];
        $value = array_shift($object_array);
        $fields = array();
        $con = $this->open();
        foreach ($object_array as $field => $val) {
            $fields[] = "$field = '" . mysqli_real_escape_string($con, $val) . "'";
        }
        $query = "UPDATE `$object` SET `$field`=`$field`+'$amount' WHERE $key = '$value'";

        if (mysqli_query($con, $query)) {

            $this->close($con);
            return 1;
        } else {
            return 0;
        }
    }
    
    
    function amount_decre($object, $object_array,$field,$amount) 
    {
        $con_key_from_arr = array_keys($object_array);
        $key = $con_key_from_arr[0];
        $value = array_shift($object_array);
        $fields = array();
        $con = $this->open();
        foreach ($object_array as $field => $val) 
        {
            $fields[] = "$field = '" . mysqli_real_escape_string($con, $val) . "'";
        }
        $query = "UPDATE `$object` SET `$field`=`$field`-'$amount' WHERE $key = '$value'";

        if (mysqli_query($con, $query)) 
        {
            $this->close($con);
            return 1;
        } else {
            return 0;
        }
    }
    

    
    
    
    
    function reqstatus($status)
    {
        if($status==1)
        {
            $msg="Pending";
        }
        elseif($status==2)
        {
            $msg="Accepted By Manager";
        }
        elseif($status==3)
        {
            $msg="Complete";
        }
        elseif($status==4)
        {
            $msg="Partial Complete";
        }
        return $msg;
    }
    
    
    
    
    function bodyhead()
    {
        return '<meta charset="utf-8" />'.$this->title().'<meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href='.$this->baseUrl("assets/css/bootstrap.min.css").' />
        <link rel="stylesheet" href='.$this->baseUrl("assets/css/font-awesome.min.css").' />
        <link rel="stylesheet" href='.$this->baseUrl("assets/css/ace.min.css").' />
        <link rel="stylesheet" href='.$this->baseUrl("assets/css/ace-rtl.min.css").' />
        <link rel="stylesheet" href='.$this->baseUrl("assets/css/ace-skins.min.css").' />
        <script src='.$this->baseUrl("assets/js/ace-extra.min.js").'>
        </script><script src='.$this->baseUrl("assets/print.js").'></script>
        <link rel="stylesheet" href='.$this->baseUrl("assets/css/datepicker.css").' />
	<link rel="stylesheet" href='.$this->baseUrl("assets/css/daterangepicker.css").' />';
    }
    
    function bodyfooter()
    {
        return '<script src='.$this->baseUrl("assets/js/jquery-2.0.3.min.js").'></script>
        <script src='.$this->baseUrl("assets/js/jquery.mobile.custom.min.js").'></script>
        <script src='.$this->baseUrl("assets/js/bootstrap.min.js").'></script>
        <script src='.$this->baseUrl("assets/js/typeahead-bs2.min.js").'></script>
        <script src='.$this->baseUrl("assets/js/jquery.dataTables.min.js").'></script>
        <script src='.$this->baseUrl("assets/js/jquery.dataTables.bootstrap.js").'></script>
        <script src='.$this->baseUrl("assets/js/ace-elements.min.js").'></script>
        <script src='.$this->baseUrl("assets/js/ace.min.js").'></script>
        <script src='.$this->baseUrl("assets/js/date-time/bootstrap-datepicker.min.js").'></script>
        <script src='.$this->baseUrl("assets/js/date-time/daterangepicker.min.js").'></script>';
    }
    
    
    function clean($str) {
        $str = @trim($str);
        if (get_magic_quotes_gpc()) {
            $str = stripslashes($str);
        }
        return mysql_real_escape_string($str);
    }

    function RandNumber($e) {
        for ($i = 0; $i < $e; $i++) {
            @$rand = $rand . rand(0, 9);
        }
        return $rand;
    }
    
    function empstatus($st)
    {
        if($st==1){ $power="Administrator"; }
        elseif ($st==2){ $power="Manager Access"; }
        else{ $power="Employee"; }
        return $power;
    }
            
    function dmy($month)
    {
        $chkj = strlen($month);
        if ($chkj == 1) {
           return $chkjval = "0" . $month;
        } 
        else 
        {
           return $chkjval = $month;
        }
    }

    function randomPassword() {
        $alphabet = "EF+GHI234WXYZ567+89@(0-=1){<>/\_+$}[]%$*ABCD";
        $pass = array();
        for ($i = 0; $i < 6; $i++) {
            $n = rand(0, strlen($alphabet) - 1);
            $pass[$i] = $alphabet[$n];
        }
        return implode($pass);
    }

    function cleanQuery($string) {
        if (get_magic_quotes_gpc())
            $string = stripslashes($string);
        return mysql_escape_string($string);
    }

    function limit_words($string, $word_limit) {
        $words = explode(" ", $string);
        return implode(" ", array_splice($words, 0, $word_limit)) . "...";
    }
    
    function today() {
        return date('Y-m-d');
    }

    function bn_date($str) {
        $en = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 0);
        $bn = array('১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯', '০');
        $str = str_replace($en, $bn, $str);
        $en = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        $en_short = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
        $bn = array('জানুয়ারী', 'ফেব্রুয়ারী', 'মার্চ', 'এপ্রিল', 'মে', 'জুন', 'জুলাই', 'অগাস্ট', 'সেপ্টেম্বর', 'অক্টোবর', 'নভেম্বর', 'ডিসেম্বর');
        $str = str_replace($en, $bn, $str);
        $str = str_replace($en_short, $bn, $str);
        $en = array('Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday');
        $en_short = array('Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri');
        $bn_short = array('শনি', 'রবি', 'সোম', 'মঙ্গল', 'বুধ', 'বৃহঃ', 'শুক্র');
        $bn = array('শনিবার', 'রবিবার', 'সোমবার', 'মঙ্গলবার', 'বুধবার', 'বৃহস্পতিবার', 'শুক্রবার');
        $str = str_replace($en, $bn, $str);
        $str = str_replace($en_short, $bn, $str);
        $en = array('am', 'pm');
        $bn = array('পূর্বাহ্ন', 'অপরাহ্ন');
        $str = str_replace($en, $bn, $str);
        return $str;
    }

    function getExtension($str) {
        $i = strrpos($str, ".");
        if (!$i) {
            return "";
        }
        $l = strlen($str) - $i;
        $ext = substr($str, $i + 1, $l);
        return $ext;
    }


    function image_bigcaption($width, $height, $destination) {
        list($w, $h) = getimagesize($_FILES['image']['tmp_name']);
        $path = $destination . '90X90_' . time() . '_' . $_FILES['image']['name'];
        $imgString = file_get_contents($_FILES['image']['tmp_name']);
        $image = imagecreatefromstring($imgString);
        $tmp = imagecreatetruecolor($width, $height);

        imagealphablending($tmp, false);
        imagesavealpha($tmp, true);
        $transparent = imagecolorallocatealpha($tmp, 255, 255, 255, 127);
        imagefilledrectangle($tmp, 0, 0, $width, $height, $transparent);
        
        imagecopyresized($tmp, $image, 0, 0, 0, 0, $width, $height, $w, $h);

        switch ($_FILES['image']['type']) {
            case 'image/jpeg':
                imagejpeg($tmp, $path, 100);
                break;
            case 'image/png':
                imagepng($tmp, $path, 0);
                break;
            case 'image/gif':
                imagegif($tmp, $path);
                break;
            default:
                exit;
                break;
        }
        return $path;
        imagedestroy($image);
        imagedestroy($tmp);
    }
    
    function image_bigcaption2($width, $height, $destination) {
        list($w, $h) = getimagesize($_FILES['image']['tmp_name']);
        $path = $destination . '160X160_' . time() . '_' . $_FILES['image']['name'];
        $imgString = file_get_contents($_FILES['image']['tmp_name']);
        $image = imagecreatefromstring($imgString);
        $tmp = imagecreatetruecolor($width, $height);

        imagealphablending($tmp, false);
        imagesavealpha($tmp, true);
        $transparent = imagecolorallocatealpha($tmp, 255, 255, 255, 127);
        imagefilledrectangle($tmp, 0, 0, $width, $height, $transparent);
        
        imagecopyresized($tmp, $image, 0, 0, 0, 0, $width, $height, $w, $h);

        switch ($_FILES['image']['type']) {
            case 'image/jpeg':
                imagejpeg($tmp, $path, 100);
                break;
            case 'image/png':
                imagepng($tmp, $path, 0);
                break;
            case 'image/gif':
                imagegif($tmp, $path);
                break;
            default:
                exit;
                break;
        }
        return $path;
        imagedestroy($image);
        imagedestroy($tmp);
    }
    
    
    function image_bigcaption3($width, $height, $destination) {
        list($w, $h) = getimagesize($_FILES['image']['tmp_name']);
        $path = $destination . '170X170_' . time() . '_' . $_FILES['image']['name'];
        $imgString = file_get_contents($_FILES['image']['tmp_name']);
        $image = imagecreatefromstring($imgString);
        $tmp = imagecreatetruecolor($width, $height);

        imagealphablending($tmp, false);
        imagesavealpha($tmp, true);
        $transparent = imagecolorallocatealpha($tmp, 255, 255, 255, 127);
        imagefilledrectangle($tmp, 0, 0, $width, $height, $transparent);
        
        imagecopyresized($tmp, $image, 0, 0, 0, 0, $width, $height, $w, $h);

        switch ($_FILES['image']['type']) {
            case 'image/jpeg':
                imagejpeg($tmp, $path, 100);
                break;
            case 'image/png':
                imagepng($tmp, $path, 0);
                break;
            case 'image/gif':
                imagegif($tmp, $path);
                break;
            default:
                exit;
                break;
        }
        return $path;
        imagedestroy($image);
        imagedestroy($tmp);
    }
    
    
    function image_bigcaption4($width, $height, $destination) {
        list($w, $h) = getimagesize($_FILES['image']['tmp_name']);
        $path = $destination . '300X300_' . time() . '_' . $_FILES['image']['name'];
        $imgString = file_get_contents($_FILES['image']['tmp_name']);
        $image = imagecreatefromstring($imgString);
        $tmp = imagecreatetruecolor($width, $height);

        imagealphablending($tmp, false);
        imagesavealpha($tmp, true);
        $transparent = imagecolorallocatealpha($tmp, 255, 255, 255, 127);
        imagefilledrectangle($tmp, 0, 0, $width, $height, $transparent);
        
        imagecopyresized($tmp, $image, 0, 0, 0, 0, $width, $height, $w, $h);

        switch ($_FILES['image']['type']) {
            case 'image/jpeg':
                imagejpeg($tmp, $path, 100);
                break;
            case 'image/png':
                imagepng($tmp, $path, 0);
                break;
            case 'image/gif':
                imagegif($tmp, $path);
                break;
            default:
                exit;
                break;
        }
        return $path;
        imagedestroy($image);
        imagedestroy($tmp);
    }
    
    function image_bigcaption5($width, $height, $destination) {
        list($w, $h) = getimagesize($_FILES['image']['tmp_name']);
        $path = $destination . '300X300_' . time() . '_' . $_FILES['image']['name'];
        $imgString = file_get_contents($_FILES['image']['tmp_name']);
        $image = imagecreatefromstring($imgString);
        $tmp = imagecreatetruecolor($width, $height);

        imagealphablending($tmp, false);
        imagesavealpha($tmp, true);
        $transparent = imagecolorallocatealpha($tmp, 255, 255, 255, 127);
        imagefilledrectangle($tmp, 0, 0, $width, $height, $transparent);
        
        imagecopyresized($tmp, $image, 0, 0, 0, 0, $width, $height, $w, $h);

        switch ($_FILES['image']['type']) {
            case 'image/jpeg':
                imagejpeg($tmp, $path, 100);
                break;
            case 'image/png':
                imagepng($tmp, $path, 0);
                break;
            case 'image/gif':
                imagegif($tmp, $path);
                break;
            default:
                exit;
                break;
        }
        return $path;
        imagedestroy($image);
        imagedestroy($tmp);
    }
    
    function backup_tables($host,$user,$pass,$name,$tables = '*')
    {

        $link = mysql_connect($host,$user,$pass);
        mysql_select_db($name,$link);
        mysql_query("SET NAMES 'utf8'");

        //get all of the tables
        if($tables == '*')
        {
            $tables = array();
            $result = mysql_query('SHOW TABLES');
            while($row = mysql_fetch_row($result))
            {
                $tables[] = $row[0];
            }
        }
        else
        {
            $tables = is_array($tables) ? $tables : explode(',',$tables);
        }
        $return='';
        //cycle through
        foreach($tables as $table)
        {
            $result = mysql_query("SELECT * FROM `$table`");
            $num_fields = mysql_num_fields($result);

            $return.= 'DROP TABLE '.$table.';';
            $row2 = mysql_fetch_row(mysql_query("SHOW CREATE TABLE `$table`"));
            $return.= "\n\n".$row2[1].";\n\n";

            for ($i = 0; $i < $num_fields; $i++) 
            {
                while($row = mysql_fetch_row($result))
                {
                    $return.= 'INSERT INTO `$table` VALUES(';
                    for($j=0; $j<$num_fields; $j++) 
                    {
                        $row[$j] = addslashes($row[$j]);
                        $row[$j] = str_replace("\n","\\n",$row[$j]);
                        if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                        if ($j<($num_fields-1)) { $return.= ','; }
                    }
                    $return.= ");\n";
                }
            }
            $return.="\n\n\n";
        }

        //save file
        $namesbackup=date('d_M_Y_H_i_s');
        $handle = fopen('db_backup/'.$namesbackup.'inventory_backup_db.sql','w+');
        fwrite($handle,$return);
        fclose($handle);
        return 1;
    }

}
?>

<ul class="nav ace-nav">

   
    <li class="red">
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <i class="icon-unlock icon-animated-vertical"></i>
            <span class="badge badge-success">
			Unlocking Services 
            </span>
        </a>

        <ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
            <li class="dropdown-header">
                <a href="label.php">
                <i class="icon-lightbulb"></i>
                Add New Unlock Category / Label
                </a>
            </li>
            <li class="dropdown-header">
                <a href="addservice.php">
                <i class="icon-unlock-alt"></i>
                Add Unlock Product/Services  
                </a>
            </li>
            <li class="dropdown-header">
                <a href="service.php">
                <i class="icon-unlock-alt"></i>
                List of Unlock Product / Services
                </a>
            </li>
            <li class="dropdown-header">
                <a href="service_order.php">
                <i class="icon-unlock-alt"></i>
                List of Services Order
                </a>
            </li>
            <li class="dropdown-header">
            </li>
        </ul>
    </li>
    
    <li class="purple">
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <i class="icon-puzzle-piece icon-animated-bell"></i>
            <span class="badge badge-important">Product </span>
        </a>

        <ul class="pull-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close">
            <li>
                <a href="addproduct.php">
                    <div class="clearfix">
                        <span class="pull-left">
                            <i class="btn btn-xs no-hover btn-info icon-tag"></i>
                            Product Add
                        </span>
                    </div>
                </a>
            </li>
            <li>
                <a href="product.php">
                    <div class="clearfix">
                        <span class="pull-left">
                            <i class="btn btn-xs no-hover btn-info icon-tags"></i>
                            Product List
                        </span>
                    </div>
                </a>
            </li>
            <li>

            </li>               
        </ul>
    </li>

    
    
    <li class="orange">
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <i class="icon-windows icon-animated-vertical"></i>
            <span class="badge badge-success">
			Product Category 
            </span>
        </a>

        <ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
            <li class="dropdown-header">
                <a href="category.php">
                <i class="icon-windows"></i>
                Add New Category || List
                </a>
            </li>
            <li class="dropdown-header">
                <a href="subcategory.php">
                <i class="icon-bell-alt"></i>
                Add New Sub-Category List  
                </a>
            </li>
            
            <li class="dropdown-header">
            </li>
        </ul>
    </li>

    
    <li class="green">
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <i class="icon-user icon-animated-vertical"></i>
            <span class="badge badge-success">
			Admin Access 
            </span>
        </a>

        <ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
            <li class="dropdown-header">
                <a href="employee.php">
                <i class="icon-user-md"></i>
                Add New Admin
                </a>
            </li>
            <li class="dropdown-header">
                <a href="employeelist.php">
                <i class="icon-user"></i>
                Admin List  
                </a>
            </li>
            
            <li class="dropdown-header">
            </li>
        </ul>
    </li>
    
    
    <li class="light-blue">
        <a data-toggle="dropdown" href="#" class="dropdown-toggle">
            <img class="nav-user-photo" src="assets/avatars/user.jpg" alt="Jason's Photo" />
            <span class="user-info">
                <small>Welcome,</small>
                <?php echo $_SESSION['SESS_AMSIT_EMP_NAME']; ?>
            </span>

            <i class="icon-caret-down"></i>
        </a>

        <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
            <li>
                <a href="#">
                    <i class="icon-cog"></i>
                    Settings
                </a>
            </li>

            <li>
                <a href="test_profile.php">
                    <i class="icon-user"></i>
                    Profile
                </a>
            </li>

            <li class="divider"></li>

            <li>
                <a href="class/logout.php">
                    <i class="icon-off"></i>
                    Logout
                </a>
            </li>
        </ul>
    </li>
</ul><!-- /.ace-nav -->
<?php
session_start(); 
include('db_Class.php'); 
$obj = new db_class();
@$cart=$obj->cart($_SESSION['SESS_CART']);
extract($_GET);

$shipp=$obj->exists_multiple("order_shipping",array("cart_id"=>$cart));
if($shipp==0)
{
    $shippings=0;
}
else 
{
    $shippings=$obj->SelectAllByVal("order_shipping","cart_id",$cart,"shipping");
}
?>
<div style="display: block;" class="checkout-content"><p>Please select the preferred shipping method to use on this order.</p>
    
    
    
    <table class="radio">
        <tbody>
            <?php
            $datash=$obj->SelectAll("shipping_method");
            if(!empty($datash))
            foreach($datash as $sh):
            ?>
            <tr>
                <td colspan="3"><h1><strong><?php echo $sh->name; ?></strong></h1></td>
            </tr>
            <?php
            if($obj->exists_multiple("shipping_method_option",array("smid"=>$sh->id))!=0){
                $dataop=$obj->SelectAllByID("shipping_method_option",array("smid"=>$sh->id));
                $x=1;
                if(!empty($dataop))
                foreach($dataop as $op):
                ?>
                <tr>
                    <td colspan="3"><strong><label><input type="radio" name="sm" id="sm_<?php echo $x; ?>" value="<?php echo $op->id; ?>"> <?php echo $op->name." $".number_format($op->cost,2); ?></label></strong></td>
                </tr>
                <?php
                $x++;
                endforeach;
            }
            endforeach;
            ?>
            <tr class="highlight">
                <td>
                </td>
                <td></td>
                <td style="text-align: right;"></label></td>
        <input type="hidden" name="shipping_cost" id="shipping_cost" value="<?php echo $shippings; ?>" />
            </tr>
        </tbody></table>
    <br>
    
    <input  type="hidden" name="shipping_comment" id="shipping_comment" />
    <br>
    <br>
    <div class="buttons">
        <div class="right">
            <a id="button-shipping-method"  onclick="step4('1')"  class="button"><span>Continue</span></a>
        </div>
    </div>
</div>
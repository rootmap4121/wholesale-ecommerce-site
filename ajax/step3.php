<div style="display: block;" class="checkout-content">
    <input name="shipping_address" onchange="newaddress('1')" value="existing" id="shipping-address-existing" checked="checked" type="radio">
    <label for="shipping-address-existing">I want to use an existing address</label>
    <div id="address">
        <div id="shipping-existing">
            <select name="address_id" style="width: 100%; margin-bottom: 15px;" size="5">
                <option value="29" selected="selected">Md Mahamodur Zaman, 44/P,Kazi bhavan, Dhaka, Dhaka, Bangladesh</option>
            </select>
        </div>
    </div>
    <p>
        <input name="shipping_address" onchange="newaddress('2')" value="new" id="shipping-address-new" type="radio">
        <label for="shipping-address-new">I want to use a new address</label>
    </p>
    
    <br>
    <div class="buttons">
        <div class="right">
            <a id="button-shipping-address" onclick="step3('1')" class="button"><span>Continue</span></a>
        </div>
    </div>

</div>
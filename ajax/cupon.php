<?php
session_start();
extract($_GET);
include('db_Class.php');
$obj=new db_class();
@$cart=$obj->cart($_SESSION['SESS_CART']);
$chk=$obj->exists_multiple("cupon_code",array("code"=>$code));
if($chk!=0) {
    $chkst=$obj->exists_multiple("cupon_code",array("code"=>$code,"status"=>2));
    if($chkst==0)
    {
        $discount=$obj->SelectAllByVal("cupon_code","code",$code,"amount");
        $dis=($subtotal*$discount)/100;
        $total=$subtotal-$dis;
        $chkdis=$obj->exists_multiple("order_discount",array("cart_id"=>$cart));
        if($chkdis==0)
        {
            //$obj->update("cupon_code",array("code"=>$code,"status"=>2));
            $obj->insert("order_discount",array("cart_id"=>$cart,"discount"=>"Cupon Code Discount","discount_amount"=>$discount,"date"=>date('Y-m-d'),"status"=>1));
        }
        else 
        {
            //$obj->update("cupon_code",array("code"=>$code,"status"=>2));
            $obj->update("order_discount",array("cart_id"=>$cart,"discount"=>"Cupon Code Discount","discount_amount"=>$discount,"date"=>date('Y-m-d'),"status"=>1));
        }
        ?>
    <input type="hidden" name="subtotal" id="subtotal" value="<?php echo $subtotal; ?>">
        <table id="total">
                <tbody>
          <tr class="row-table-1">
            <td class="right cart-total-name "><b>Sub-Total:</b></td>
            <td class="right cart-total1 ">$<?php echo number_format($subtotal,2); ?></td>
          </tr>
          <?php
          $datad=$obj->SelectAllByID("order_discount",array("cart_id"=>$cart));
          $discoun=0;
          if(!empty($datad))
          foreach($datad as $dd):
              $discoun+=$dd->discount_amount;
          ?>
          <tr class="row-table-1">
            <td class="right cart-total-name "><b>(-) Cupon Discount :</b></td>
            <td class="right cart-total1 ">$<?php echo number_format($dd->discount_amount,2); ?></td>
          </tr>
          <?php
            endforeach;

            $shipp=$obj->SelectAllByVal("order_shipping","cart_id",$cart,"shipping");
            if($shipp!=0 || $shipp!='')
            {
                $shipping=$shipp;
             ?>
          <tr class="row-table-4">
            <td class="right cart-total-name "><b>(+) Shipping Cost :</b></td>
            <td class="right cart-total1 ">$<?php echo number_format($shipp,2); ?></td>
          </tr>
          <?php   
        }
        else 
        {
            $shipping=0;
        }
        
        $gt=($subtotal+$shipping)-$discoun;
      ?>
      <tr class="row-table-4">
        <td class="right cart-total-name last"><b>Total:</b></td>
        <td class="right cart-total1 last">$<?php echo number_format($gt,2); ?></td>
      </tr>
          </tbody>
    </table>
    <?php
    }
    else 
   {
       ?>
    <div class="warning">Warning : Already Used</div>
    <input type="hidden" name="subtotal" id="subtotal" value="<?php echo $subtotal; ?>">
    <table id="total">
            <tbody>
      <tr class="row-table-1">
        <td class="right cart-total-name "><b>Sub-Total:</b></td>
        <td class="right cart-total1 ">$<?php echo number_format($subtotal,2); ?></td>
      </tr>
      <?php
      $datad=$obj->SelectAllByID("order_discount",array("cart_id"=>$cart));
      $discoun=0;
      if(!empty($datad))
      foreach($datad as $dd):
          $discoun+=$dd->discount_amount;
      ?>
      <tr class="row-table-1">
        <td class="right cart-total-name "><b>(-) Cupon Discount :</b></td>
        <td class="right cart-total1 ">$<?php echo number_format($dd->discount_amount,2); ?></td>
      </tr>
      <?php
        endforeach;
          
        $shipp=$obj->SelectAllByVal("order_shipping","cart_id",$cart,"shipping");
        if($shipp!=0 || $shipp!='')
        {
            $shipping=$shipp;
         ?>
      <tr class="row-table-4">
        <td class="right cart-total-name "><b>(+) Shipping Cost :</b></td>
        <td class="right cart-total1 ">$<?php echo number_format($shipp,2); ?></td>
      </tr>
      <?php   
        }
        else 
        {
            $shipping=0;
        }
        
        $gt=($subtotal+$shipping)-$discoun;
      ?>
      <tr class="row-table-4">
        <td class="right cart-total-name last"><b>Total:</b></td>
        <td class="right cart-total1 last">$<?php echo number_format($gt,2); ?></td>
      </tr>
          </tbody>
    </table>
<?php  
   }
} 
else 
{
  ?>
    <div class="warning">Danger : Invalid Cupon Code</div>
<input type="hidden" name="subtotal" id="subtotal" value="<?php echo $subtotal; ?>">
    <table id="total">
            <tbody>
      <tr class="row-table-1">
        <td class="right cart-total-name "><b>Sub-Total:</b></td>
        <td class="right cart-total1 ">$<?php echo number_format($subtotal,2); ?></td>
      </tr>
      <?php
      $datad=$obj->SelectAllByID("order_discount",array("cart_id"=>$cart));
      $discoun=0;
      if(!empty($datad))
      foreach($datad as $dd):
          $discoun+=$dd->discount_amount;
      ?>
      <tr class="row-table-1">
        <td class="right cart-total-name "><b>(-) Cupon Discount :</b></td>
        <td class="right cart-total1 ">$<?php echo number_format($dd->discount_amount,2); ?></td>
      </tr>
      <?php
        endforeach;
          
        $shipp=$obj->SelectAllByVal("order_shipping","cart_id",$cart,"shipping");
        if($shipp!=0 || $shipp!='')
        {
            $shipping=$shipp;
         ?>
      <tr class="row-table-4">
        <td class="right cart-total-name "><b>(+) Shipping Cost :</b></td>
        <td class="right cart-total1 ">$<?php echo number_format($shipp,2); ?></td>
      </tr>
      <?php   
        }
        else 
        {
            $shipping=0;
        }
        
        $gt=($subtotal+$shipping)-$discoun;
      ?>
      <tr class="row-table-4">
        <td class="right cart-total-name last"><b>Total:</b></td>
        <td class="right cart-total1 last">$<?php echo number_format($gt,2); ?></td>
      </tr>
          </tbody>
    </table>
<?php
}
?>
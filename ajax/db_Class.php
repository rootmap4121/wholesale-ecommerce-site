<?php
class db_class {

    public function open() {
        $con = mysqli_connect("localhost", "root", "", "igeek");
        //$con = mysqli_connect("localhost", "ptdxrklv_igeel", "igeek", "ptdxrklv_igeek");
        return $con;
    }

    public function close($con) {
        mysqli_close($con);
    }
    
 
    
 function order_status($status)
    {
        if($status==1)
        {
            return "Pending";
        }
        elseif($status==2) 
        {
            return "Payment Completed";
        }
        elseif($status==0) 
        {
            return "pending";
        }
    }
    
    function Error($msg,$location)
    {
        $errmsg_arr[]="<div class='warning'  style=''>Warning : ".$msg." <img src='images/close-1.png' alt='' class='close'></div>";
        $errflag = true;
        if ($errflag) {
            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
            session_write_close();
            header("location:".$location);
            exit();
        }       
    }
    
    function Success($msg,$location)
    {
        $errmsg_arr[]="<div class='successmsg'  style=''>".$msg." <img src='images/close-1.png' alt='' class='close'></div>";
        $errflag = true;
        if ($errflag) {
            $_SESSION['SMSG_ARR'] = $errmsg_arr;
            session_write_close();
            header("location:".$location);
            exit();
        }  
    }
    
    function ShowMsg()
    {
        if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
            foreach($_SESSION['ERRMSG_ARR'] as $msg) 
            {
				unset($_SESSION['ERRMSG_ARR']);
                return $msg;  
            }
   			
			
        }

        if( isset($_SESSION['SMSG_ARR']) && is_array($_SESSION['SMSG_ARR']) && count($_SESSION['SMSG_ARR']) >0 ) {
            foreach($_SESSION['SMSG_ARR'] as $msgs) 
            {
				unset($_SESSION['SMSG_ARR']);
                return $msgs;  
            }
   			
        }       
    }
    
    function login($email,$password,$success)
    {
            if(empty($email) or empty($password))
            {
                return $this->Error("Fields Should Not Be Empty",$this->filename());
            }
            else
            {
                $insertarray=array("email"=>$email,"password"=>$this->password($password));
                if($this->exists_multiple("customer", $insertarray)==1)
                {
                    $loginarray=array("email"=>$email,"password"=>$this->password($password),"status"=>2);
                    if($this->exists_multiple("customer", $loginarray)==1)
                    {
                        session_regenerate_id();
                        $_SESSION['SESS_CUSID']=  $this->SelectAllByVal2("customer","email",$email,"password",$this->password($password),"id");
                        $_SESSION['SESS_CUSNAME']=  $this->SelectAllByVal2("customer","email",$email,"password",$this->password($password),"fname");
                        session_write_close();
                        $this->insert("loginfo",array("detail"=>"Login Success in User Panel by ".$email,"date"=>date('Y-m-d'),"status"=>1));
                        return $this->Success("Thank You For Login ,Welcome To Your Account.",$success);
                    }
                    else
                    {
                        $this->insert("loginfo",array("detail"=>"Account is not Activated  for ".$email,"date"=>date('Y-m-d'),"status"=>1));
                        return $this->Error("Your Account is Not Activated Yet.",$this->filename());
                    }
                    
                }
                else
                {
                    $this->insert("loginfo",array("detail"=>"Login Wrong in User Panel by ".$email,"date"=>date('Y-m-d'),"status"=>1));
                    return $this->Error("Failed Login, Please Try Again",$this->filename());
                }
            }
    }
    
    
    function checklogin()
    {
        if($_SESSION['SESS_CUSID']=='')
        {
            return $this->Error("Session Exipred, Please Login ... ","login.php");
        }
    }
    
    function checkmenu()
    {
        if($_SESSION['SESS_CUSID']=='')
        {
            return $this->Error("Please Login, access These Page... ","login.php");
        }
    }
    
    function authprice($price)
    {
        if(@$_SESSION['SESS_CUSID']=='')
        {
            return "<a href='login.php' class='button' style='margin-right:5px;'> <span>Login For Price</span> </a>";
        }
        else
        {
            return "$".$price;
        }
    }
    
    
    /**
     * Insert query for Object
     * @param type $object
     * @param type $object_array
     * @return string/Exception
     */
    function insert($object, $object_array) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        foreach ($object_array as $col => $val) {
            if ($count++ != 0)
                $fields .= ', ';
            $col = mysqli_real_escape_string($con, $col);
            $val = mysqli_real_escape_string($con, $val);
            $fields .= "`$col` = '$val'";
        }
        $query = "INSERT INTO `$object` SET $fields";
        if (mysqli_query($con, $query)) {
            $this->close($con);
            return 1;
        } else {
            return 0;
        }
    }
    
    function cart($getcart)
    {
        if($getcart!='')
        {
            return $cart=$getcart;
        }
         else 
        {
            $_SESSION['SESS_CART']=time();
            session_write_close();
            return $cart=$_SESSION['SESS_CART'];
        }
    }
    
    function newcart($getcart)
    {
        unset($_SESSION['SESS_CART']);
        if($getcart!='')
        {
            $_SESSION['SESS_CART']=time();
            session_write_close();
            return $cart=$_SESSION['SESS_CART'];
        }
         else 
        {
            $_SESSION['SESS_CART']=time();
            session_write_close();
            return $cart=$_SESSION['SESS_CART'];
        }
    }
    
    
    function cartdata($pid,$cart)
    {
		if(isset($_SESSION['SESS_CUSID'])!='')
		{
        if($this->exists_multiple("cart",array("pid"=>$pid,"cart_id"=>$cart))==0)
        {
            $this->insert("cart",array("pid"=>$pid,"cart_id"=>$cart,"quantity"=>1,"status"=>1));
        }
        else 
        {
            $this->SelectAllByVal2incre("cart","pid",$pid,"cart_id",$cart,"quantity"); 
        }

        return $this->exists_multiple("cart",array("cart_id"=>$cart));
		}
		else
		{
			return 0;	
		}
    }
    
    
    function lastid($object) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM `$object` ORDER BY id DESC LIMIT 1";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        }
       
    }
    
    
    
    
    /**
     * if the object is exists
     * @param type $object
     * @param type $object_array
     * @return int
     */
    
	
	function exists_multiple($object, $object_array) {
        $count = 0; 
        $fields = ''; 
        $con = $this->open(); 
            foreach ($object_array as $col => $val) 
            { 
                if ($count++ != 0) 
                $fields .= ' AND '; 
            $col = mysqli_real_escape_string($con, $col); 
            $val = mysqli_real_escape_string($con, $val); 
            $fields .= "`$col` = '$val' "; 
            } 
            $query = "SELECT * FROM `$object` WHERE $fields"; 
            $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            $this->close($con);
            if ($count !=0) {
                return $count;
            }
            else
            {
                return 0;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
    
    
      
    
    function totalrows($object) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM `$object`";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            $this->close($con);
                return $count;
            } else {
            $this->close($con);
            return 0;
        }
    }
    
    function MakePassword($pass)
    {
        $bytes = 555000;
        $salt = base64_encode($bytes);
        $hash=hash('sha512', $salt . $pass);
        return md5($hash);
    }
    
    /**
     * Select all the objects
     * @param type $object
     * @return array
     */
    function SelectAll($object) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM `$object`";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        }
    }
    
    
        
    
    /**
     * Select object by ID
     * @param type $object
     * @param type $object_array
     * @return int
     */
    function SelectAllByID($object, $object_array) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        if (count($object_array) <= 1) {
            foreach ($object_array as $col => $val) {
                if ($count++ != 0)
                    $fields .= ', ';
                $col = mysqli_real_escape_string($con, $col);
                $val = mysqli_real_escape_string($con, $val);
                $fields .= "`$col` = '$val'";
            }
        }
        $query = "SELECT * FROM `$object` WHERE $fields";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
	
    function SelectAllByID_Multiple($object, $object_array) 
    {
        $count = 0; 
        $fields = ''; 
        $con = $this->open(); 
            foreach ($object_array as $col => $val) 
            { 
                if ($count++ != 0) 
                $fields .= ' AND '; 
            $col = mysqli_real_escape_string($con, $col); 
            $val = mysqli_real_escape_string($con, $val); 
            $fields .= "`$col` = '$val' "; 
            } 
            
        $query = "SELECT * FROM `$object` WHERE $fields ORDER BY id DESC";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
    function SelectAllByID_Multiple_site($object, $object_array,$pid,$order,$limit) 
    {
        $count = 0; 
        $fields = ''; 
        $con = $this->open(); 
            foreach ($object_array as $col => $val) 
            { 
                if ($count++ != 0) 
                $fields .= ' AND '; 
            $col = mysqli_real_escape_string($con, $col); 
            $val = mysqli_real_escape_string($con, $val); 
            $fields .= "`$col` = '$val' "; 
            } 
            
        $query = "SELECT * FROM `$object` WHERE $fields AND id!='$pid' ORDER BY id $order LIMIT $limit";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
        function SelectAllByID_site($object, $object_array,$order,$limit) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        if (count($object_array) <= 1) {
            foreach ($object_array as $col => $val) {
                if ($count++ != 0)
                    $fields .= ', ';
                $col = mysqli_real_escape_string($con, $col);
                $val = mysqli_real_escape_string($con, $val);
                $fields .= "`$col` = '$val'";
            }
        }
        $query = "SELECT * FROM `$object` WHERE $fields ORDER BY id $order LIMIT $limit";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
        function SelectAllByVal($object,$field,$fval,$value) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM `$object` WHERE `$field`='$fval'";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
               $rows = mysqli_fetch_array($result);
                    $objects= $rows[$value];
                $this->close($con);
                return $objects;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
    function SelectAllByVal2($object,$field,$fval,$field2,$fval2,$value) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM `$object` WHERE `$field`='$fval' AND `$field2`='$fval2'";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
               $rows = mysqli_fetch_array($result);
                    $objects= $rows[$value];
                $this->close($con);
                return $objects;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
    function SelectAllByVal2incre($object,$field,$fval,$field2,$fval2,$fetch)
    {
        $link=$this->open();
        $sql="UPDATE `$object` SET $fetch=$fetch+1 WHERE `$field`='$fval' AND `$field2`='$fval2'";
        $result=mysqli_query($link, $sql);
        if ($result) {
            $this->close($link);
            return 1;
        } else {
            return 0;
        }
    }
    




    function SelectAll_Site($object,$order,$limit) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM `$object` order by id $order LIMIT $limit";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        }
    }
    
        function SelectAll_sdate($object,$field,$date) {
            $count = 0;
            $fields = '';
            $con = $this->open();
            $query = "SELECT * FROM `$object` WHERE `$field`='$date'";
            $result = mysqli_query($con, $query);
            if ($result) {
                $count = mysqli_num_rows($result);

                if ($count >= 1) {
                    //$object[]=array();
                    while ($rows = $result->fetch_object()) {
                        $objects[] = $rows;
                    }
                    $this->close($con);
                    return $objects;
                }
            }
        }
        
        function SelectAll_ddate($object,$field,$startdate,$enddate) {
            $count = 0;
            $fields = '';
            $con = $this->open();
            $query = "SELECT * FROM `$object` WHERE `$field` >= '$startdate' AND `$field` <= '$enddate'";
            $result = mysqli_query($con, $query);
            if ($result) {
                $count = mysqli_num_rows($result);

                if ($count >= 1) {
                    //$object[]=array();
                    while ($rows = $result->fetch_object()) {
                        $objects[] = $rows;
                    }
                    $this->close($con);
                    return $objects;
                }
            }
        }
    

    
    
    
    function filename()
    {
        return basename($_SERVER['PHP_SELF']);
    }
    
    function status($status)
    {
        if($status==1)
        {
            return "New";
        }
        elseif($status==2) 
        {
            return "Lost";
        }
        elseif($status==3) 
        {
            return "Stolen";
        }
    }
    
    
    function password($password)
    {
        $st="geek";
        $newpassword=$st."-".$password;
        $convert=md5($newpassword);
        return $convert;
    }
    
    

    /**
     * Delete the object from database
     * @param type $object
     * @param type $object_array
     * @return string|\Exception
     */
    function delete($object, $object_array) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        if (count($object_array) <= 1) {
            foreach ($object_array as $col => $val) {
                if ($count++ != 0)
                    $fields .= ', ';
                $col = mysqli_real_escape_string($con, $col);
                $val = mysqli_real_escape_string($con, $val);
                $fields .= "`$col` = '$val'";
            }
        }
        $query = "Delete FROM `$object` WHERE $fields";
        if (mysqli_query($con, $query)) {

            $this->close($con);
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Delete the object
     * @param type $object
     * @param type $object_array
     */
    function update($object, $object_array) {
        $con_key_from_arr = array_keys($object_array);
        $key = $con_key_from_arr[0];
        $value = array_shift($object_array);
        $fields = array();
        $con = $this->open();
        foreach ($object_array as $field => $val) {
            $fields[] = "$field = '" . mysqli_real_escape_string($con, $val) . "'";
        }
        $query = "UPDATE `$object` SET " . join(', ', $fields) . " WHERE $key = '$value'";

        if (mysqli_query($con, $query)) {

            $this->close($con);
            return 1;
        } else {
            return 0;
        }
    }
    
    
    

    
    function baseUrl($suffix = '') {
        $protocol = strpos($_SERVER['SERVER_SIGNATURE'], '443') !== false ? 'https://' : 'http://';
        $web_root = $protocol . $_SERVER['HTTP_HOST'] . "/" . "";
        $suffix = ltrim($suffix, '/');
        return $web_root . trim($suffix);
    }

    
   
    
    function clean($str) {
        $str = @trim($str);
        if (get_magic_quotes_gpc()) {
            $str = stripslashes($str);
        }
        return mysql_real_escape_string($str);
    }

    function RandNumber($e) {
        for ($i = 0; $i < $e; $i++) {
            @$rand = $rand . rand(0, 9);
        }
        return $rand;
    }
    
    function carrier($st)
    {
        if($st==1){ $power="Verizon"; }
        elseif ($st==2){ $power="US IMEI Blacllist"; }
		elseif ($st==3){ $power="Canada IMEI Blacklist"; }
		elseif ($st==4){ $power="Telus"; }
		elseif ($st==5){ $power="Metro PCS"; }
		elseif ($st==6){ $power="Straight Talk"; }
		elseif ($st==7){ $power="Net10"; }
		elseif ($st==8){ $power="Tracfone"; }
		elseif ($st==9){ $power="Cricket"; }
        else{ $power="Not Selected Yet"; }
        return $power;
    }
            
    function dmy($month)
    {
        $chkj = strlen($month);
        if ($chkj == 1) {
           return $chkjval = "0" . $month;
        } 
        else 
        {
           return $chkjval = $month;
        }
    }

    function randomPassword() {
        $alphabet = "EF+GHI234WXYZ567+89@(0-=1){<>/\_+$}[]%$*ABCD";
        $pass = array();
        for ($i = 0; $i < 6; $i++) {
            $n = rand(0, strlen($alphabet) - 1);
            $pass[$i] = $alphabet[$n];
        }
        return implode($pass);
    }

    function cleanQuery($string) {
        if (get_magic_quotes_gpc())
            $string = stripslashes($string);
        return mysql_escape_string($string);
    }

    function limit_words($string, $word_limit) {
        $words = explode(" ", $string);
        return implode(" ", array_splice($words, 0, $word_limit)) . "...";
    }
    
    function today() {
        return date('Y-m-d');
    }

    
    function getExtension($str) {
        $i = strrpos($str, ".");
        if (!$i) {
            return "";
        }
        $l = strlen($str) - $i;
        $ext = substr($str, $i + 1, $l);
        return $ext;
    }
	
	
	

}
?>

<div style="display: block;" class="checkout-content"><p>Please select the preferred payment method to use on this order.</p>
    <table class="radio">
        <tbody>
            <tr class="highlight">
                <td><input name="payment_method" value="1" id="cod_0" type="radio">
                </td>
                <td><label for="cod">Paypal Checkout</label></td>
            </tr>
            <tr class="highlight">
                <td><input name="payment_method" value="2" id="cod_1" checked="checked" type="radio">
                </td>
                <td><label for="cod">Local Payment</label></td>
            </tr>
        </tbody></table>
    <br>
    <b>Add Comments About Your Order</b>
    <textarea name="methodcomment" id="methodcomment" rows="8" style="width: 98%;"></textarea>
    <br>
    <br>
    <div class="buttons">
        <div class="right">
            <div class="mt">
                <a id="button-payment-method" onclick="step5('1')" class="button"><span>Continue</span></a>
            </div>
        </div>
    </div>
</div>
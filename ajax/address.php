<?php
session_start(); 
include('db_Class.php'); 
$obj = new db_class();
@$cart=$obj->cart($_SESSION['SESS_CART']);
$obj->checkmenu();
extract($_GET);
if($q==1)
{
?>
        <div id="shipping-existing">
            <select name="address_id" id="address_id" style="width: 100%; margin-bottom: 15px;" size="5">
                <?php
                $data=$obj->SelectAllByID("shipping_address",array("cusid"=>$_SESSION['SESS_CUSID']));
                if(!empty($data))
                foreach($data as $row):
                ?>
                <option value="<?php echo $row->address; ?>" selected="selected"><?php echo $row->address; ?></option>
                <?php 
                endforeach;
                ?>
            </select>
        </div>

        <br>
        
        <div class="buttons">
            <div class="right">
                <a id="button-shipping-address" onclick="step3('1')" class="button"><span>Continue</span></a>
            </div>
        </div>
    <?php
}
elseif($q==2)
{
    ?>
<div id="shipping-new">
        <table class="form">
            <tbody><tr>
                    <td><span class="required">*</span> First Name:</td>
                    <td><input name="firstname" id="firstname" class="large-field" type="text"></td>
                </tr>
                <tr>
                    <td><span class="required">*</span> Last Name:</td>
                    <td><input name="lastname"  id="lastname"  class="large-field" type="text"></td>
                </tr>
                <tr>
                    <td><span class="required">*</span> Address:</td>
                    <td><input name="address" id="address" class="large-field" type="text"></td>
                </tr>
                <tr>
                    <td><span class="required">*</span> City:</td>
                    <td><input name="city" id="city"  class="large-field" type="text"></td>
                </tr>
                <tr>
                    <td><span class="required">*</span> Post Code:</td>
                    <td><input name="postcode" id="postcode"  class="large-field" type="text"></td>
                </tr>
                <tr>
                    <td><span class="required">*</span> Country:</td>
                    <td><select name="country_id" id="country_id" class="large-field">
                            <option value=""> --- Please Select --- </option>
                             <option value="United States" selected="selected">United States</option>
                        </select></td>
                </tr>
                <tr>
                    <td><span class="required">*</span> Region / State:</td>
                    <td><select name="state" id="state">
                                                        <option value=""> --- Please Select --- </option>
                                                        <option value="Alabama">Alabama</option>
                                                        <option value="Alaska">Alaska</option>
                                                        <option value="American Samoa">American Samoa</option>
                                                        <option value="Arizona">Arizona</option>
                                                        <option value="Arkansas">Arkansas</option>
                                                        <option value="Armed Forces Africa">Armed Forces Africa</option>
                                                        <option value="Armed Forces Americas">Armed Forces Americas</option>
                                                        <option value="Armed Forces Canada">Armed Forces Canada</option>
                                                        <option value="Armed Forces Europe">Armed Forces Europe</option>
                                                        <option value="Armed Forces Middle East">Armed Forces Middle East</option>
                                                        <option value="Armed Forces Pacific">Armed Forces Pacific</option>
                                                        <option value="California">California</option>
                                                        <option value="Colorado">Colorado</option>
                                                        <option value="Connecticut">Connecticut</option>
                                                        <option value="Delaware">Delaware</option>
                                                        <option value="District of Columbia">District of Columbia</option>
                                                        <option value="Federated States Of Micronesia">Federated States Of Micronesia</option>
                                                        <option value="Florida">Florida</option>
                                                        <option value="Georgia">Georgia</option>
                                                        <option value="Guam">Guam</option>
                                                        <option value="Hawaii">Hawaii</option>
                                                        <option value="Idaho">Idaho</option>
                                                        <option value="Illinois">Illinois</option>
                                                        <option value="Indiana">Indiana</option>
                                                        <option value="Iowa">Iowa</option>
                                                        <option value="Kansas">Kansas</option>
                                                        <option value="Kentucky">Kentucky</option>
                                                        <option value="Louisiana">Louisiana</option>
                                                        <option value="Maine">Maine</option>
                                                        <option value="Marshall Islands">Marshall Islands</option>
                                                        <option value="Maryland">Maryland</option>
                                                        <option value="Massachusetts">Massachusetts</option>
                                                        <option value="Michigan">Michigan</option>
                                                        <option value="Minnesota">Minnesota</option>
                                                        <option value="Mississippi">Mississippi</option>
                                                        <option value="Missouri">Missouri</option>
                                                        <option value="Missouri">Montana</option>
                                                        <option value="Nebraska">Nebraska</option>
                                                        <option value="Nevada">Nevada</option>
                                                        <option value="New Hampshire">New Hampshire</option>
                                                        <option value="New Jersey">New Jersey</option>
                                                        <option value="New Mexico">New Mexico</option>
                                                        <option value="New York">New York</option>
                                                        <option value="North Carolina">North Carolina</option>
                                                        <option value="North Dakota">North Dakota</option>
                                                        <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                        <option value="Ohio">Ohio</option>
                                                        <option value="Oklahoma">Oklahoma</option>
                                                        <option value="Oregon">Oregon</option>
                                                        <option value="Palau">Palau</option>
                                                        <option value="Pennsylvania">Pennsylvania</option>
                                                        <option value="Puerto Rico">Puerto Rico</option>
                                                        <option value="Rhode Island">Rhode Island</option>
                                                        <option value="South Carolina">South Carolina</option>
                                                        <option value="South Dakota">South Dakota</option>
                                                        <option value="Tennessee">Tennessee</option>
                                                        <option value="Texas">Texas</option>
                                                        <option value="Utah">Utah</option>
                                                        <option value="Vermont">Vermont</option>
                                                        <option value="Virgin Islands">Virgin Islands</option>
                                                        <option value="Virginia">Virginia</option>
                                                        <option value="Washington">Washington</option>
                                                        <option value="West Virginia">West Virginia</option>
                                                        <option value="Wisconsin">Wisconsin</option>
                                                        <option value="Wyoming">Wyoming</option>
                                                    </select></td>
                </tr>
            </tbody></table>
    </div>
        
    <br>
    <div class="buttons">
        <div class="right">
            <a id="button-shipping-address" onclick="step3_save('1')" class="button"><span>Continue</span></a>
        </div>
    </div>
    <?php    
}
else 
{
    echo "Please Select One Address To Complete Step 3";
}
?>
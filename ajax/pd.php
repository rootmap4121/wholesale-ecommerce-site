<?php
extract($_GET);
include('db_Class.php');
$obj = new db_class();

if ($p != '') {
    $data = $obj->SelectAllByID("service", array("id" => $p));
    if (!empty($data))
        foreach ($data as $row):
            ?>
            <div class="search-pice border-1">
                <div class="left">$<?php echo $row->price; ?> USD</div>
                <div class="right">Delivery Time <br> <?php echo $row->delivery_time; ?></div>
            </div>
            <?php
        endforeach;
}
else {
    ?>
    <div class="search-pice border-1">
        Please Select A Service First
    </div>
    <?php
}
?>
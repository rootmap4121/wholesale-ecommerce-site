<?php
session_start();
extract($_GET);
include('db_Class.php');
$obj=new db_class();
@$cart=$obj->cart($_SESSION['SESS_CART']);

if(isset($_SESSION['SESS_CUSID'])!='')
{
    if($obj->exists_multiple("cart",array("cart_id"=>$cart))!=0)
    {
    $cost=$obj->SelectAllByVal("shipping_method_option","id",$shipping_cost,"cost");
    
    $insert=array("cusid"=>$_SESSION['SESS_CUSID'],"payment_method"=>$payment,"payment_method_comment"=>$methodcomment,"shipping_id"=>$shipping_cost,"shipping_cost"=>$cost,"shipping_address"=>$address,"cart_id"=>$cart,"payment_status"=>1,"date"=>date('Y-m-d'),"status"=>1);
    if($obj->exists_multiple("product_order",array("cart_id"=>$cart))==0)
    {
        
        $obj->insert("loginfo",array("detail"=>"A Order Has Been Placed By Customer","date"=>date('Y-m-d'),"status"=>1));
        $obj->update("cart",array("cart_id"=>$cart,"status"=>2));
        $obj->insert("product_order", $insert);
        
        //email Script Start 
        //$subject = "Product Order Via Wireless Geeks";
        // Receive form's message value into php $message variable
        $message = '<!DOCTYPE HTML><head><meta http-equiv="content-type" content="text/html"><title>Email notification</title>';
 $message .= "<link href='http://wirelessgeekswholesale.com/css/stylesheet.css' rel='stylesheet' type='text/css' /></head>";
 $message .= "<body><div class='bg-1'><div style='display: block;' class='checkout-content'><div class='checkout-product'>";
        $message .= "Automated Product Order Email ";
        $message .= "<img src='http://wirelessgeekswholesale.com/images/logo.png'><br>";
        $message .= "<table border='1' cellpadding='4' cellspacing='1'>
                    <thead>
                    <tr style='background:#ccc;'>
                    <td>Product Name</td>
                    <td>Category</td>
                    <td>Quantity</td>
                    <td>Price</td>
                    <td>Total</td>
                </tr></thead><tbody>";
                $data=$obj->SelectAllByID_Multiple("cart",array("cart_id"=>$cart,"status"=>2));
                $subtotal=0;
                $x=1;
                if(!empty($data))
                foreach($data as $row):
                $cid=$obj->SelectAllByVal("product","id",$row->pid,"cid");
                $price=$obj->SelectAllByVal("product","id",$row->pid,"price");
                $tt=$price*$row->quantity;
                $subtotal+=$tt;
                 $message .="
                <tr>
                    
                    <td><a href=''>".$obj->SelectAllByVal("product","id",$row->pid,"name")."</a></td>
                    <td>".$obj->SelectAllByVal("category","id",$cid,"name")."</td>
                    <td>".$row->quantity."</td>
                    <td>$".number_format($price,2)."</td>
                    <td>$".number_format($tt,2)."</td>
                </tr>";
                $x++;
                endforeach;  
            $message .="</tbody>";
            
            
            $message .="<tfoot>
                <tr>
                    <td colspan='4'><b>Sub-Total:</b></td>
                    <td>$".number_format($subtotal,2)."</td>
                </tr>";

                if($obj->exists_multiple("product_order",array("cart_id"=>$cart))!=0)
                {
                    $shipping_title=$obj->SelectAllByVal("shipping_method_option","id",$shipping_cost,"name");
                    $cs=$obj->SelectAllByVal("shipping_method_option","id",$shipping_cost,"cost");
                
                $message .="<tr>
                    <td colspan='4'><b>(+)".$shipping_title.":</b></td>
                    <td>$".number_format($cs,2)."</td>
                </tr>";
                
                    $shcost=$cs;
                }
                else
                {
                    $shcost=0;
                }

                $datad=$obj->SelectAllByID("order_discount",array("cart_id"=>$cart));
                $discoun=0;
                if(!empty($datad))
                foreach($datad as $dd):
                    $discoun+=$dd->discount_amount;

                $message .="<tr>
                    <td colspan='4'><b>(-) Cupon Discount :</b></td>
                    <td>$".number_format($dd->discount_amount,2)."</td>
                </tr>";
                  endforeach;
                  $all=($subtotal+$shipping_cost)-$discoun; 
                $message .="<tr>
                    <td colspan='4'><b>Total:</b></td>
                    <td>$".number_format($all)."</td>
                </tr>
            </tfoot>
        </table></div></div></div></body>";

                
                
         $too='admin@wirelessgeekswholesale.com'; // give to email address       
        $to=$obj->SelectAllByVal("customer","id",$_SESSION['SESS_CUSID'],"email");             // give to email address
        $subject = 'Wiressless Geeks - Online Order in';  //change subject of email
        $from    = 'admin@wirelessgeekswholesale.com';                           // give from email address

        // mandatory headers for email message, change if you need something different in your setting.
        $headers  = "From: " . $from . "\r\n";
        $headers .= "Reply-To: ". $from . "\r\n";
        $headers .= "CC: admin@wirelessgeekswholesale.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        mail($to, $subject, $message, $headers);   
        mail($too, $subject, $message, $headers);   
        //email Script End
        
    }
    else
    {
        $insertu=array("cart_id"=>$cart,"cusid"=>$_SESSION['SESS_CUSID'],"payment_method"=>$payment,"payment_method_comment"=>$methodcomment,"shipping_id"=>$shipping_cost,"shipping_cost"=>$cost,"shipping_address"=>$address,"payment_status"=>1);
        $obj->insert("loginfo",array("detail"=>"A Order Has Been Placed By Customer","date"=>date('Y-m-d'),"status"=>1));
        $obj->update("cart",array("cart_id"=>$cart,"status"=>2));
        $obj->update("product_order", $insertu);
        
        
        //email Script Start 
        //$subject = "Product Order Via Wireless Geeks";
        // Receive form's message value into php $message variable
        $message = '<!DOCTYPE HTML><head><meta http-equiv="content-type" content="text/html"><title>Email notification</title>';
 $message .= "<link href='http://wirelessgeekswholesale.com/css/stylesheet.css' rel='stylesheet' type='text/css' /></head>";
 $message .= "<body><div class='bg-1'><div style='display: block;' class='checkout-content'><div class='checkout-product'>";
        $message .= "Automated Product Order Email ";
        $message .= "<img src='http://wirelessgeekswholesale.com/images/logo.png'><br>";
        $message .= "<table border='1' cellpadding='4' cellspacing='1'>
                    <thead>
                    <tr style='background:#ccc;'>
                    <td>Product Name</td>
                    <td>Category</td>
                    <td>Quantity</td>
                    <td>Price</td>
                    <td>Total</td>
                </tr></thead><tbody>";
                $data=$obj->SelectAllByID_Multiple("cart",array("cart_id"=>$cart,"status"=>2));
                $subtotal=0;
                $x=1;
                if(!empty($data))
                foreach($data as $row):
                $cid=$obj->SelectAllByVal("product","id",$row->pid,"cid");
                $price=$obj->SelectAllByVal("product","id",$row->pid,"price");
                $tt=$price*$row->quantity;
                $subtotal+=$tt;
                 $message .="
                <tr>
                    
                    <td><a href=''>".$obj->SelectAllByVal("product","id",$row->pid,"name")."</a></td>
                    <td>".$obj->SelectAllByVal("category","id",$cid,"name")."</td>
                    <td>".$row->quantity."</td>
                    <td>$".number_format($price,2)."</td>
                    <td>$".number_format($tt,2)."</td>
                </tr>";
                $x++;
                endforeach;  
            $message .="</tbody>";
            
            
            $message .="<tfoot>
                <tr>
                    <td colspan='4'><b>Sub-Total:</b></td>
                    <td>$".number_format($subtotal,2)."</td>
                </tr>";

                if($obj->exists_multiple("product_order",array("cart_id"=>$cart))!=0)
                {
                    $shipping_title=$obj->SelectAllByVal("shipping_method_option","id",$shipping_cost,"name");
                    $cs=$obj->SelectAllByVal("shipping_method_option","id",$shipping_cost,"cost");
                
                $message .="<tr>
                    <td colspan='4'><b>(+)".$shipping_title.":</b></td>
                    <td>$".number_format($cs,2)."</td>
                </tr>";
                
                    $shcost=$cs;
                }
                else
                {
                    $shcost=0;
                }

                $datad=$obj->SelectAllByID("order_discount",array("cart_id"=>$cart));
                $discoun=0;
                if(!empty($datad))
                foreach($datad as $dd):
                    $discoun+=$dd->discount_amount;

                $message .="<tr>
                    <td colspan='4'><b>(-) Cupon Discount :</b></td>
                    <td>$".number_format($dd->discount_amount,2)."</td>
                </tr>";
                  endforeach;
                  $all=($subtotal+$shipping_cost)-$discoun; 
                $message .="<tr>
                    <td colspan='4'><b>Total:</b></td>
                    <td>$".number_format($all)."</td>
                </tr>
            </tfoot>
        </table></div></div></div></body>";

                
                
        $too='admin@wirelessgeekswholesale.com'; // give to email address        
        $to=$obj->SelectAllByVal("customer","id",$_SESSION['SESS_CUSID'],"email");             // give to email address
        $subject = 'Wiressless Geeks - Online Order Updated';  //change subject of email
        $from    = 'admin@wirelessgeekswholesale.com';                           // give from email address

        // mandatory headers for email message, change if you need something different in your setting.
        $headers  = "From: " . $from . "\r\n";
        $headers .= "Reply-To: ". $from . "\r\n";
        $headers .= "CC: admin@wirelessgeekswholesale.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        mail($to, $subject, $message, $headers);   
        mail($too, $subject, $message, $headers);   
        //email Script End
        
        
    }
if($payment==1)
{
?>
<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="upload" value="1">

<input type="hidden" name="currency_code" value="USD">

<input type="hidden" name="cmd" value="_cart">
<input type="hidden" name="return" value="<?php echo $obj->baseUrl('success.php?cart_id='.$cart); ?>">
<input type="hidden" name="cancel_return" value="<?php echo $obj->baseUrl('error.php?cart_id='.$cart); ?>">
<input type="hidden" name="invoice" value="<?php echo $cart; ?>">
<INPUT TYPE="hidden" name="charset" value="utf-8">

<input type="hidden" name="business" value="wirelessgeeks@yahoo.com">
<input type="hidden" name="paymentaction" value="order">
<div style="display: block;" class="checkout-content"><div class="checkout-product">
        <table>
            <thead>
                <tr>
                    <td class="name">Product Name</td>
                    <td class="model">Category</td>
                    <td class="quantity">Quantity</td>
                    <td class="price">Price</td>
                    <td class="total">Total</td>
                </tr>
            </thead>
            <tbody>
                <?php
                $data=$obj->SelectAllByID_Multiple("cart",array("cart_id"=>$cart,"status"=>2));
                $subtotal=0;
                $x=1;
                if(!empty($data))
                foreach($data as $row):
                ?>
                <tr>
                    
                    <td class="name"><a href="#"><?php echo $obj->SelectAllByVal("product","id",$row->pid,"name"); ?></a></td>
                    <td class="model">
                    <?php $cid=$obj->SelectAllByVal("product","id",$row->pid,"cid"); 
                    echo $obj->SelectAllByVal("category","id",$cid,"name");
                    $price=$obj->SelectAllByVal("product","id",$row->pid,"price");
                    $tt=$price*$row->quantity;
                    $subtotal+=$tt;
                    ?>    
                     <input type="hidden" name="item_name_<?php echo $x; ?>" value="<?php echo $obj->SelectAllByVal("product","id",$row->pid,"name"); ?>">
                     <input type="hidden" name="item_code_<?php echo $x; ?>" value="<?php echo $obj->SelectAllByVal("product","id",$row->pid,"id"); ?>">
                    <input type="hidden" name="amount_<?php echo $x; ?>" value="<?php echo $price; ?>">
                    <input type="hidden" name="quantity_<?php echo $x; ?>" value="<?php echo $row->quantity; ?>">
                    </td>
                    <td class="quantity"><?php echo $row->quantity; ?></td>
                    <td class="price">$<?php echo number_format($price,2); ?></td>
                    <td class="total">$<?php echo number_format($tt,2); ?></td>
                </tr>
                <?php 
                $x++;
                endforeach;  ?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="4" class="price"><b>Sub-Total:</b></td>
                    <td class="total">$<?php echo number_format($subtotal,2); ?></td>
                </tr>
                <?php
                if($obj->exists_multiple("product_order",array("cart_id"=>$cart))!=0)
                {
                    $shipping_title=$obj->SelectAllByVal("shipping_method_option","id",$shipping_cost,"name");
                    $cs=$obj->SelectAllByVal("shipping_method_option","id",$shipping_cost,"cost");
                ?>
                <tr>
                    <td colspan="4" class="price"><b>(+)<?php echo $shipping_title; ?>:</b></td>
                    <td class="total">$<?php echo number_format($cs,2); ?></td>
                </tr>
                <?php
                    $shcost=$cs;
                }
                else
                {
                    $shcost=0;
                }
                ?>
                <input type="hidden" name="discount_amount_1" value="<?php echo $obj->SelectAllByVal("order_discount","cart_id",$cart,"discount_amount"); ?>">
                <input type="hidden" name="shipping_1" value="5">
                <input type="hidden" name="CARTBORDERCOLOR" value="0000CD">
                <?php
                $datad=$obj->SelectAllByID("order_discount",array("cart_id"=>$cart));
                $discoun=0;
                if(!empty($datad))
                foreach($datad as $dd):
                    $discoun+=$dd->discount_amount;
                ?>
                <tr>
                    <td class="price" colspan="4"><b>(-) Cupon Discount :</b></td>
                  <td class="total">$<?php echo number_format($dd->discount_amount,2); ?></td>
                </tr>
                <?php
                  endforeach;
                  
                ?>
                <tr>
                    <td colspan="4" class="price"><b>Total:</b></td>
                    <td class="total">$<?php $all=($subtotal+$shipping_cost)-$discoun; echo number_format($all); ?></td>
                </tr>
            </tfoot>
        </table>
    </div>
    <div class="payment"><div class="buttons">
            <div class="right">
                <button type="submit" id="button-confirm" class="btn"><span>Confirm & Checkout with Paypal Order</span></button>
            </div>
        </div>

    </div>
</div>

</form>
<?php
}
else
{
?>
<input type="hidden" name="upload" value="1">

<input type="hidden" name="currency_code" value="USD">

<input type="hidden" name="cmd" value="_cart">
<input type="hidden" name="return" value="<?php echo $obj->baseUrl('success.php?cart_id='.$cart); ?>">
<input type="hidden" name="cancel_return" value="<?php echo $obj->baseUrl('error.php?cart_id='.$cart); ?>">
<input type="hidden" name="invoice" value="<?php echo $cart; ?>">
<INPUT TYPE="hidden" name="charset" value="utf-8">

<input type="hidden" name="business" value="wirelessgeeks@yahoo.com">
<input type="hidden" name="paymentaction" value="order">
<div style="display: block;" class="checkout-content"><div class="checkout-product">
        <table>
            <thead>
                <tr>
                    <td class="name">Product Name</td>
                    <td class="model">Category</td>
                    <td class="quantity">Quantity</td>
                    <td class="price">Price</td>
                    <td class="total">Total</td>
                </tr>
            </thead>
            <tbody>
                <?php
                $data=$obj->SelectAllByID_Multiple("cart",array("cart_id"=>$cart,"status"=>2));
                $subtotal=0;
                $x=1;
                if(!empty($data))
                foreach($data as $row):
                ?>
                <tr>
                    
                    <td class="name"><a href="#"><?php echo $obj->SelectAllByVal("product","id",$row->pid,"name"); ?></a></td>
                    <td class="model">
                    <?php $cid=$obj->SelectAllByVal("product","id",$row->pid,"cid"); 
                    echo $obj->SelectAllByVal("category","id",$cid,"name");
                    $price=$obj->SelectAllByVal("product","id",$row->pid,"price");
                    $tt=$price*$row->quantity;
                    $subtotal+=$tt;
                    ?>    
                     <input type="hidden" name="item_name_<?php echo $x; ?>" value="<?php echo $obj->SelectAllByVal("product","id",$row->pid,"name"); ?>">
                     <input type="hidden" name="item_code_<?php echo $x; ?>" value="<?php echo $obj->SelectAllByVal("product","id",$row->pid,"id"); ?>">
                    <input type="hidden" name="amount_<?php echo $x; ?>" value="<?php echo $price; ?>">
                    <input type="hidden" name="quantity_<?php echo $x; ?>" value="<?php echo $row->quantity; ?>">
                    </td>
                    <td class="quantity"><?php echo $row->quantity; ?></td>
                    <td class="price">$<?php echo number_format($price,2); ?></td>
                    <td class="total">$<?php echo number_format($tt,2); ?></td>
                </tr>
                <?php 
                $x++;
                endforeach;  ?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="4" class="price"><b>Sub-Total:</b></td>
                    <td class="total">$<?php echo number_format($subtotal,2); ?></td>
                </tr>
                <?php
                if($obj->exists_multiple("product_order",array("cart_id"=>$cart))!=0)
                {
                    $shipping_title=$obj->SelectAllByVal("shipping_method_option","id",$shipping_cost,"name");
                    $cs=$obj->SelectAllByVal("shipping_method_option","id",$shipping_cost,"cost");
                ?>
                <tr>
                    <td colspan="4" class="price"><b>(+)<?php echo $shipping_title; ?>:</b></td>
                    <td class="total">$<?php echo number_format($cs,2); ?></td>
                </tr>
                <?php
                $shcost=$cs;
                }
                else
                {
                    $shcost=0;
                }
                ?>
                <input type="hidden" name="discount_amount_1" value="<?php echo $obj->SelectAllByVal("order_discount","cart_id",$cart,"discount_amount"); ?>">
                <input type="hidden" name="shipping_1" value="5">
                <input type="hidden" name="CARTBORDERCOLOR" value="0000CD">
                <?php
                $datad=$obj->SelectAllByID("order_discount",array("cart_id"=>$cart));
                $discoun=0;
                if(!empty($datad))
                foreach($datad as $dd):
                    $discoun+=$dd->discount_amount;
                ?>
                <tr>
                    <td class="price" colspan="4"><b>(-) Cupon Discount :</b></td>
                  <td class="total">$<?php echo number_format($dd->discount_amount,2); ?></td>
                </tr>
                <?php
                  endforeach;
                  
                ?>
                <tr>
                    <td colspan="4" class="price"><b>Total:</b></td>
                    <td class="total">$<?php $all=($subtotal+$shcost)-$discoun; echo number_format($all); ?></td>
                </tr>
            </tfoot>
        </table>
    </div>
    <div class="payment"><div class="buttons">
            <div class="right">
                <a href="successorder.php?cartid=<?php echo $cart; ?>" id="button-confirm" class="button"><span>Finish Order</span></a>
            </div>
        </div>

    </div>
</div>

<?php
}
    }
}
 else {
     echo "Please Login / Refresh The Page";
}
?>
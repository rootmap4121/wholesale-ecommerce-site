<?php
session_start();
include('db_Class.php');
$obj = new db_class();
@$cart = $obj->cart($_SESSION['SESS_CART']);
function payment($pay)
{
    if($pay==1)
    {
        return "paypal";
    }
    else 
    {
        return "Local payment";   
    }
}
extract($_GET);
if($q==1)
{
?>
<h1>My Account Information</h1>
    <form action="" method="post" enctype="multipart/form-data" id="edit">
    <h2>Your Personal Details</h2>
    <div class="content">
      <table class="form">
        <tbody><tr>
          <td><span class="required">*</span> First Name:</td>
          <td><input name="firstname" value="<?php echo $obj->SelectAllByVal("customer","id",$_SESSION['SESS_CUSID'],"fname"); ?>" type="text">
            </td>
        </tr>
        <tr>
          <td><span class="required">*</span> Last Name:</td>
          <td><input name="lastname" value="<?php echo $obj->SelectAllByVal("customer","id",$_SESSION['SESS_CUSID'],"lname"); ?>" type="text">
            </td>
        </tr>
        <tr>
          <td><span class="required">*</span> E-Mail:</td>
          <td><input name="email" value="<?php echo $obj->SelectAllByVal("customer","id",$_SESSION['SESS_CUSID'],"email"); ?>" type="text">
            </td>
        </tr>
        <tr>
          <td><span class="required">*</span> Telephone:</td>
          <td><input name="telephone" value="<?php echo $obj->SelectAllByVal("customer","id",$_SESSION['SESS_CUSID'],"talephone"); ?>" type="text">
            </td>
        </tr>
      </tbody></table>
    </div>
    <div class="buttons">
        <div class="left"><a href="account.php" class="button-back-left"><span>Back</span></a></div>
      <div class="right">
        <button type="submit" name="edit" class="btn"><span>Continue Save</span></button>
      </div>
    </div>
  </form>
<?php
}
elseif($q==2)
{
?>
<h1>Change Password</h1>
    <form action="" method="post" enctype="multipart/form-data" id="password">
    <h2>Your Password</h2>
    <div class="content">
      <table class="form">
        <tbody><tr>
          <td><span class="required">*</span> Password:</td>
          <td><input name="password" value="" type="password">
            </td>
        </tr>
        <tr>
          <td><span class="required">*</span> Password Confirm:</td>
          <td><input name="confirm" value="" type="password">
            </td>
        </tr>
      </tbody></table>
    </div>
    <div class="buttons">
        <div class="left"><a href="account.php" class="button-back-left"><span>Back</span></a></div>
      <div class="right"><button type="submit" name="passw" class="btn"><span>Continue Save</span></button></div>
    </div>
  </form>
<?php    
}
elseif($q==3)
{
?>
<h1>Address Book</h1>
    <h2>Address Book Entries</h2>
       <form action="" method="post" enctype="multipart/form-data" id="password"> <div class="content">
      
      <table style="width: 100%;">
        <tbody><tr>
          <td><?php echo $_SESSION['SESS_CUSNAME']; ?> <?php echo $obj->SelectAllByVal("customer","id",$_SESSION['SESS_CUSID'],"lname"); ?><br><?php echo $obj->SelectAllByVal("customer","id",$_SESSION['SESS_CUSID'],"talephone"); ?><br><?php echo $obj->SelectAllByVal("customer","id",$_SESSION['SESS_CUSID'],"company"); ?><br>
              <textarea name="address" style="width: 50%; height: 100px;"><?php echo $obj->SelectAllByVal("customer","id",$_SESSION['SESS_CUSID'],"address1"); ?></textarea> 
              </td>
        </tr>
      </tbody></table>
      
    </div>
        <div class="buttons">
            <div class="left"><a href="account.php" class="button-back-left"><span>Back</span></a></div>
      <div class="right"><button type="submit" name="addr" class="btn"><span>Update Address Book</span></button></div>
</div></form>
<?php    
}

elseif($q==4)
{
?>
<h1>Your Order History</h1>
  <table class="list">
    <thead>
      <tr>
        <td class="left">Order Date</td>
        <td class="left">Order ID</td>
        <td class="right">Payment Method</td>
        <td class="right">Amount (USD)</td>
        <td class="right">Order Status</td>
      </tr>
    </thead>
    <tbody>
        <?php
        $chkorder=$obj->exists_multiple("product_order",array("cusid"=>$_SESSION['SESS_CUSID']));
        if($chkorder==0)
        {
        ?>
      <tr>
        <td class="center" colspan="4">You do not have any transactions!</td>
      </tr>
        <?php 
        }
        else 
        {
     $data=$obj->SelectAllByID("product_order",array("cusid"=>$_SESSION['SESS_CUSID']));
     if(!empty($data))
     foreach($data as $row)
     {
         $order=$obj->SelectAllByID("cart",array("cart_id"=>$row->cart_id));
         $am=0;
         foreach ($order as $or)
         {
             $price=$obj->SelectAllByVal("product","id",$or->pid,"price");
             $tp=$or->quantity*$price;
             $am+=$tp;
         }
         
         $orderdis=$obj->SelectAllByID("order_discount",array("cart_id"=>$row->cart_id));
         $disc=0;
         if(!empty($orderdis))
         foreach ($orderdis as $dis)
         {
             
             $disc+=$dis->discount_amount;
         }
         
         $amount=$am-$disc;
         ?>
      <tr>
        <td class="left"><?php echo $row->date; ?></td>
        <td class="left"><?php echo $row->cart_id; ?></td>
        <td class="right"><?php echo payment($row->payment_method); ?></td>
        <td class="right"><?php echo $amount; ?></td>
        <td class="right"><?php echo $obj->order_status($row->status); ?></td>
      </tr>
      <?php
     }
     }
        ?>
     </tbody>
  </table>
  
  <div class="buttons">
      <div class="right"><a href="account.php" class="button-cont-right"><span>Continue</span></a></div>
  </div>
<?php    
}
elseif($q==5)
{
?>
<h1>Your Transactions</h1>
  <p>Your current balance is:<b> $0.00</b>.</p>
  <table class="list">
    <thead>
      <tr>
        <td class="left">Date Added</td>
        <td class="left">Description</td>
        <td class="right">Amount (USD)</td>
      </tr>
    </thead>
    <tbody>
            <tr>
        <td class="center" colspan="3">You do not have any transactions!</td>
      </tr>
          </tbody>
  </table>
  
  <div class="buttons">
      <div class="right"><a href="account.php" class="button-cont-right"><span>Continue</span></a></div>
  </div>
<?php    
}
else 
{
    echo "Reload Page Please,Maybe Session Expired";
}
?>
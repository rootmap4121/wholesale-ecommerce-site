<?php session_start();
include('ajax/db_Class.php');
$obj = new db_class();
@$cart = $obj->cart($_SESSION['SESS_CART']);

if(isset($_POST['send']))
{
    // Receive form's subject value into php $subject variable
    $subject = "Wireless Geeks Product Return Email";
    extract($_POST);
    // Receive form's message value into php $message variable
    $message = " Full Name : " . $firstname." ".$lastname."\n";
    $message .= "Phone : " . $phone."\n";
    $message .= "Email : " . $email."\n";
    $message .= "Order ID : " . $orderid."\n";
    $message .= "Order Date : " . $order_date."\n";
    $message .= "Product : " . $cid." ".$product."\n";
    $message .= "Quantity : " . $quantity."\n";
    $message .= "Return Reason Id : " . $return_reason_id."\n";
    $message .= "Product is opened : " . $opened."\n";
    $message .= "Faulty or other details : " . $comment."\n";

    // Receive form's sender name value into php $sender variable
    $sender = "returns@wireless-geeks.com";
    //$sender = "contact@amsitsoft.com";

    // Receive form's user email value into php $user_email variable
    $user_email = "returns@wireless-geeks.com";

    // the email address where the message will be sent
    $TO ="returns@wireless-geeks.com";

    $send_email = mail($TO, $subject, $message, "From: " . $sender . " || " . $user_email . ">");

    // To check email has been sent or not
    if ($send_email) {
           $obj->Success("Successfully Send",$obj->filename());
    } else {
           $obj->Error("Successfully Send",$obj->filename());
    }
}

?>
<!DOCTYPE html>
<html dir="ltr" lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>Mobile online store</title>
        <meta name="description" content="Mobile online store" />
        <link href="images/favicon.png" rel="icon" />
<?php include('include/headlink.php'); ?>
    </head>
    <body class="common-home">
        <div class="bg-1">
            <div class="main-shining">
                <div class="row-1">
                    <?php include('include/header.php'); ?>
                    <div class="clear"></div>
<?php include('include/menu.php'); ?>
                </div>
                <div class="clear"></div>

<?php //include('include/slider_home.php');  ?>

                <div class="main-container">
                    <p id="back-top"> <a href="#top"><span></span></a> </p>
                    <div id="container">
                        <div id="notification"> </div>

<?php include('include/category.php'); ?>

                        <div id="content">
                            
                           



                            
                            <div class="breadcrumb">
                                <a href="index.php">Home</a>
         » <a href="account.php">Account</a>
         » <a class="last" href="return.php">Product Returns</a>
      </div>
  
  <div class="box-container">
      <?php
      echo $obj->ShowMsg();
      ?>
    <h1>Product Returns</h1>
    <p>Please complete the form below to request an RMA number.</p>    
    <form action="return.php" method="post" enctype="multipart/form-data" id="return">
    <h2>Order Information</h2>
    <div class="content">
      <div class="left"><span class="required">*</span> First Name:<br>
        <input name="firstname" value="" class="large-field" type="text">
        <br>
                <br>
        <span class="required">*</span> Last Name:<br>
        <input name="lastname" value="" class="large-field" type="text">
        <br>
                <br>
        <span class="required">*</span> E-Mail:<br>
        <input name="email" value="" class="large-field" type="text">
        <br>
                <br>
        <span class="required">*</span> Telephone:<br>
        <input name="phone" value="" class="large-field" type="text">
        <br>
                <br>
      </div>
      <div class="right"><span class="required">*</span> Order ID:<br>
        <input name="orderid" value="" class="large-field" type="text">
        <br>
                <br>
        Order Date:<br>
        <input id="dp1397521683997" name="order_date" value="" class="large-field date hasDatepicker" type="text">
        <br>
      </div>
    </div>
    <h2>Product Information &amp; Reason for Return</h2>
    <div id="return-product">
      <div class="content">
        <div class="return-product">
          <div class="return-name"><span class="required">*</span> <b>Product Name:</b><br>
            <input name="product" value="" type="text">
            <br>
                      </div>
          <div class="return-model"><span class="required">*</span> <b>Category :</b><br>
            <input name="cid" value="" type="text">
            <br>
                      </div>
          <div class="return-quantity"><b>Quantity:</b><br>
            <input name="quantity" value="1" type="text">
          </div>
        </div>
        <div class="return-detail">
          <div class="return-reason"><span class="required">*</span> <b>Reason for Return:</b><br>
            <table>
                                          <tbody><tr>
                <td width="1"><input name="return_reason_id" value="Dead On Arrival" id="return-reason-id1" type="radio"></td>
                <td><label for="return-reason-id1">Dead On Arrival</label></td>
              </tr>
                                                        <tr>
                <td width="1"><input name="return_reason_id" value="Faulty, please supply details" id="return-reason-id4" type="radio"></td>
                <td><label for="return-reason-id4">Faulty, please supply details</label></td>
              </tr>
                                                        <tr>
                <td width="1"><input name="return_reason_id" value="Order Error" id="return-reason-id3" type="radio"></td>
                <td><label for="return-reason-id3">Order Error</label></td>
              </tr>
                                                        <tr>
                <td width="1"><input name="return_reason_id" value="Other, please supply details" id="return-reason-id5" type="radio"></td>
                <td><label for="return-reason-id5">Other, please supply details</label></td>
              </tr>
                                                        <tr>
                <td width="1"><input name="return_reason_id" value="Received Wrong Item" id="return-reason-id2" type="radio"></td>
                <td><label for="return-reason-id2">Received Wrong Item</label></td>
              </tr>
                                        </tbody></table>
                      </div>
          <div class="return-opened"><b>Product is opened:</b><br>
                        <input name="opened" value="opened" id="opened" type="radio">
                        <label for="opened">Yes</label>
                        <input name="opened" value="unopened" id="unopened" checked="checked" type="radio">
                        <label for="unopened">No</label>
            <br>
            <br>
            Faulty or other details:<br>
            <textarea name="comment" cols="150" rows="6"></textarea>
          </div>
          <div class="return-captcha"><b>Enter the code in the box below:</b><br>
            <input name="captcha" value="" type="text">
            <br>
            <br>
            <img src="images/captcha.jpg" alt="">
                      </div>
        </div>
      </div>
    </div>
        <div class="buttons">
      <div class="left"><a href="account.php" class="button-return-left"><span>Back</span></a></div>
      <div class="right">
          <input value="Continue" name="send" class="button-return-right" type="submit">
      </div>
    </div>
      </form>
  </div>




                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
<?php include('include/footer.php'); ?>
                <script type="text/javascript" 	src="js/livesearch.js"></script>
            </div>
        </div>
        <script type="text/javascript"></script>
    </body>
</html>
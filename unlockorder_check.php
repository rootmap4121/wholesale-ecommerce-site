<?php 
session_start();
include('ajax/db_Class.php');
$obj = new db_class();
@$cart = $obj->cart($_SESSION['SESS_CART']);
$obj->checkmenu();
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>Mobile online store</title>
        <meta name="description" content="Mobile online store" />
        <link href="images/favicon.png" rel="icon" />
	<?php include('include/headlink.php'); ?>
	
        <link href="css/select2-bootstrap.css" rel="stylesheet" type="text/css">
        <link href="css/select2.css" rel="stylesheet" type="text/css">
        <script>
        function showUser(str)
        {
        if (str=="")
          {
          document.getElementById("sp1").innerHTML="";
          return;
          }
        if (window.XMLHttpRequest)
          {// code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttp=new XMLHttpRequest();
          }
        else
          {// code for IE6, IE5
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
          }
        xmlhttp.onreadystatechange=function()
          {
          if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {
            $("#sp1").fadeOut();
            $("#sp1").fadeIn();
            document.getElementById("sp1").innerHTML=xmlhttp.responseText;
            }
          } 
        xmlhttp.open("GET","ajax/search.php?q="+str,true);
        xmlhttp.send();
         
        var xmlhttps;    
        if (str=="")
          {
          document.getElementById("sp").innerHTML="";
          return;
          }
        if (window.XMLHttpRequest)
          {// code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttps=new XMLHttpRequest();
          }
        else
          {// code for IE6, IE5
          xmlhttps=new ActiveXObject("Microsoft.XMLHTTPS");
          }
        xmlhttps.onreadystatechange=function()
          {
          if (xmlhttps.readyState==4 && xmlhttps.status==200)
            {
            $("#sp").fadeOut();
            $("#sp").fadeIn();
            document.getElementById("sp").innerHTML=xmlhttps.responseText;
            }
          }  
        xmlhttps.open("GET","ajax/pd.php?p="+str,true);
        xmlhttps.send();
         
        }
        </script>
        <script type="text/javascript">
            jQuery(function($) {
                    $("#single").change(function(){
                      $("#sp").show();
                      $("#sp1").show();
                    });
            })
        </script>
        
        <style type="text/css">
        #sp{ display:none; }
        #sp1{ display:none; }
        </style>
    </head>
    <body class="common-home">
        <div class="bg-1">
            <div class="main-shining">
                <div class="row-1">
                    <?php include('include/header.php'); ?>
                    <div class="clear"></div>
<?php include('include/menu.php'); ?>
                </div>
                <div class="clear"></div>

<?php //include('include/slider_home.php');  ?>

                <div class="main-container">
                    <p id="back-top"> <a href="#top"><span></span></a> </p>
                    <div id="container">
                        <div id="notification"> </div>

<?php include('include/category.php'); ?>

                        <div id="content">
                            
                           


<div class="breadcrumb">
    <a href="index.php">Home</a>
    » <a class="last" href="unlock.php">Unlocking Service </a> » <a class="last" href="unlock.php">Check Your Order </a>
      </div>
  
    <div class="box-container">
                                <h1>Check Your Service Order</h1>
                                <div class="login-content">
                                    
                                    <div style="margin-left: auto; margin-right: auto;">;
                                        <form action="unlockorder_status.php" method="post" enctype="multipart/form-data" id="login">
                                            <div class="content">
                                                <h2>Please Use Your Order ID</h2>
                                                <br>
                                                <b class="padd-form">Order ID:</b>
                                                <input class="q1 margen-bottom" type="text" name="orderid" value="" />
<br><br>
                                                <b class="padd-form">Respond Email Address:</b>
                                                <input class="q1 margen-bottom"  type="text" name="email" value="" />
                                                <br />
                                                
                                                <div class="buttons">
        <button class="button" name="submit" type="submit"><span>Check Now</span></button>
    </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>




                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
<?php include('include/footer.php'); ?>
                <script type="text/javascript" 	src="js/livesearch.js"></script>
            </div>
        </div>
        <script>

  var placeholder = "Please Select";

  $('.select2, .select2-multiple').select2({ placeholder: placeholder });

  $(".select2, .select2-multiple, .select2-allow-clear, .select2-remote").on( select2OpenEventName, function() {
	if ( $(this).parents('[class*="has-"]').length ) {
	  var classNames = $(this).parents('[class*="has-"]')[0].className.split(/\s+/);
	  for (var i=0; i<classNames.length; ++i) {
		  if ( classNames[i].match("has-") ) {
			$('#select2-drop').addClass( classNames[i] );
		  }
	  }
	}

  });

</script>
    </body>
</html>
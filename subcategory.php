<?php 
session_start();
$errmsg_arr[]='';
$errflag=false;
include('ajax/db_Class.php');
$obj = new db_class(); @$cart=$obj->cart($_SESSION['SESS_CART']);
if(isset($_GET['cart'])=='yes')
{
    if($obj->cartdata($_GET['pid'],$cart)!='')
    {
        echo $obj->Success("Product Successfully Added",$obj->filename()."?scid=".$_GET['scid']);
    }
    else 
    {
        echo $obj->Error("Error In Cart",$obj->filename()."?scid=".$_GET['scid']);
    }
}
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>Mobile online store</title>
        <meta name="description" content="Mobile online store" />
        <link href="images/favicon.png" rel="icon" />
<?php include('include/headlink.php'); ?>
        <script>
            
        </script>
    </head>
    <body class="product-category">
        <div class="bg-1">
            <div class="main-shining">
                <div class="row-1">
                   <?php include('include/header.php'); ?>
                    <div class="clear"></div>
                    <?php include('include/menu.php'); ?>
                </div>
                <div class="clear"></div>

                <div class="main-container">
                    <p id="back-top"> <a href="#top"><span></span></a> </p>
                    <div id="container">
                        <div id="notification"> </div>
                        <?php include('include/category.php'); ?>

                        <div id="content">  <div class="breadcrumb">
                                <a href="index.php">Home</a>
                                &raquo; <a href="subcategory.php?scid=<?php echo $_GET['scid']; ?>"> <?php echo $obj->SelectAllByVal("subcategory","id",$_GET['scid'],"name"); ?> (<?php echo $obj->exists_multiple("product",array("scid"=>$_GET['scid'])); ?>)</a>
                            </div>
                            <div class="product-filter">
                                <div class="sort"><b>Sort By:</b>
                                    <select onchange="location = this.value;">
                                        <option value="subcategory.php?scid=<?php echo $_GET['scid']; ?>&amp;sort=p.sort_order&amp;order=ASC" selected="selected">Default</option>
                                        <option value="subcategory.php?scid=<?php echo $_GET['scid']; ?>&amp;sort=pd.name&amp;order=ASC">Name (A - Z)</option>
                                        <option value="subcategory.php?scid=<?php echo $_GET['scid']; ?>&amp;sort=pd.name&amp;order=DESC">Name (Z - A)</option>
                                        <option value="subcategory.php?scid=<?php echo $_GET['scid']; ?>&amp;sort=p.price&amp;order=ASC">Price (Low &gt; High)</option>
                                        <option value="subcategory.php?scid=<?php echo $_GET['scid']; ?>&amp;sort=p.price&amp;order=DESC">Price (High &gt; Low)</option>
                                        <option value="subcategory.php?scid=<?php echo $_GET['scid']; ?>&amp;sort=rating&amp;order=DESC">Rating (Highest)</option>
                                        <option value="subcategory.php?scid=<?php echo $_GET['scid']; ?>&amp;sort=rating&amp;order=ASC">Rating (Lowest)</option>
                                        <option value="subcategory.php?scid=<?php echo $_GET['scid']; ?>&amp;sort=p.model&amp;order=ASC">Model (A - Z)</option>
                                        <option value="subcategory.php?scid=<?php echo $_GET['scid']; ?>&amp;sort=p.model&amp;order=DESC">Model (Z - A)</option>
                                    </select>
                                </div>
                                <div class="limit"><b>Show:</b>
                                    <select onchange="location = this.value;">
                                        <option value="subcategory.php?scid=<?php echo $_GET['scid']; ?>&amp;limit=6" selected="selected">6</option>
                                        <option value="subcategory.php?scid=<?php echo $_GET['scid']; ?>&amp;limit=25">25</option>
                                        <option value="subcategory.php?scid=<?php echo $_GET['scid']; ?>&amp;limit=50">50</option>
                                        <option value="subcategory.php?scid=<?php echo $_GET['scid']; ?>&amp;limit=75">75</option>
                                        <option value="subcategory.php?scid=<?php echo $_GET['scid']; ?>&amp;limit=100">100</option>
                                    </select>
                                </div>
                                
                                <div class="display"></div>
                            </div>
<?php echo $obj->ShowMsg(); ?>
                            <div class="product-grid">
                                <ul>
                                   
                                    <?php 
                                    $existscategorypro=$obj->exists_multiple("product",array("scid"=>$_GET['scid']));
                                    if($existscategorypro!=0)
                                    {
                                        @$getor=$_GET['order'];
                                        if($getor=='')
                                        {
                                            $order="DESC";
                                        }
                                        else 
                                        {
                                            $order=$_GET['order'];
                                        }
                                        
                                        @$getlimit=$_GET['limit'];
                                        if($getlimit=='')
                                        {
                                            $limit="10";
                                        }
                                        else 
                                        {
                                            $limit=$_GET['limit'];
                                        }
                                        
                                        
                                    $sqlproduct=$obj->SelectAllByID_Multiple_site("product",array("scid"=>$_GET['scid']),0,$order,$limit);
                                    //$sqlproduct=$obj->SelectAll("product");
                                        $i=1;
                                    if(!empty($sqlproduct))
                                    foreach ($sqlproduct as $product):
                                        if($i==1)
                                        {
                                        ?>
                                        <li class="first-in-line">
                                        <?php   
                                        }
                                        elseif($i==4) 
                                        {
                                            $i==1;
                                        ?>
                                        <li class="last-in-line">
                                        <?php
                                        }
                                        else 
                                        {
                                        ?>
                                        <li class="">
                                        <?php
                                        }
                                        ?>
                                        
                                        <div class="image"><a href="product_view.php?pid=<?php echo $product->id; ?>"><img id="img_43" src="product/<?php echo $product->photo3; ?>" title="<?php echo $product->name; ?>" alt="<?php echo $product->name; ?>" /></a></div>
                                        <div class="name"><a href="product_view.php?pid=<?php echo $product->id; ?>"><?php echo $product->name; ?></a></div>
                                        <div class="description">
                                        <?php echo $product->description; ?>
                                        </div>
                                        <div class="price" style="margin-right: 10px;">
                                            <span class="price-new"><?php echo $obj->authprice($product->price); ?></span>
                                         </div>

                                        <div class="cart"><a  href="<?php echo $obj->filename(); ?>?cart=yes&AMP;pid=<?php echo $product->id; ?>&AMP;scid=<?php echo $product->scid; ?>"   class="button"><span>Add to Cart</span></a></div>
                                    </li>
                                    <?php
                                    $i++;
                                    endforeach;
                                    }
                                 ?>
                                </ul>
                            </div>

                            <div class="pagination"><div class="links"> <b>1</b>  <a href="subcategory.php?scid=<?php echo $_GET['scid']; ?>&amp;page=2">2</a>  <a href="subcategory.php?scid=<?php echo $_GET['scid']; ?>&amp;page=3">3</a>  <a href="subcategory.php?scid=<?php echo $_GET['scid']; ?>&amp;page=2">&gt;</a> <a href="subcategory.php?scid=<?php echo $_GET['scid']; ?>&amp;page=3">&gt;|</a> </div><div class="results">Showing 1 to 6 of 13 (3 Pages)</div></div>
                        </div>
                        <script type="text/javascript"><!--
                        function display(view) {
                                
                                    $('.product-list').attr('class', 'product-grid');

                                    $('.product-grid ul li').each(function(index, element) {
                                        html = '';

                                        var image = $(element).find('.image').html();

                                        if (image != null) {
                                            html += '<div class="image">' + image + '</div>';
                                        }

                                        html += '<div class="name">' + $(element).find('.name').html() + '</div>';


                                        var price = $(element).find('.price').html();

                                        if (price != null) {
                                            html += '<div class="price">' + price + '</div>';
                                        }

                                        html += '<div class="description">' + $(element).find('.description').html() + '</div>';

                                        var rating = $(element).find('.rating').html();

                                        if (rating != null) {
                                            html += '<div class="rating">' + rating + '</div>';
                                        }

                                        html += '<div class="cart">' + $(element).find('.cart').html() + '</div>';
                                       

                                        $(element).html(html);
                                    });

                                    $('.display').html('<b>Display:</b> <a id="list_a" onclick="display(\'list\');">List</a>  <div id="grid_b"></div>');

                                    $.totalStorage('display', 'grid');
                                }

                            view = $.totalStorage('display');

                            if (view) {
                                display(view);
                            } else {
                                display('grid');
                            }
//--></script> 
                        <script type="text/javascript">
                            (function($) {
                                $.fn.equalHeights = function(minHeight, maxHeight) {
                                    tallest = (minHeight) ? minHeight : 0;
                                    this.each(function() {
                                        if ($(this).height() > tallest) {
                                            tallest = $(this).height()
                                        }
                                    });
                                    if ((maxHeight) && tallest > maxHeight)
                                        tallest = maxHeight;
                                    return this.each(function() {
                                        $(this).height(tallest)
                                    })
                                }
                            })(jQuery)
                            $(window).load(function() {
                                if ($(".cat-height").length) {
                                    $(".cat-height").equalHeights()
                                }
                            })
                        </script>
                        <div class="clear"></div>
                    </div>
                </div>
<?php include('include/footer.php'); ?>
                <script type="text/javascript" 	src="js/livesearch.js"></script>
            </div>
        </div>
        <script type="text/javascript"></script>
    </body>
</html>
<?php
session_start();
include('ajax/db_Class.php');
$obj = new db_class();
@$cart = $obj->cart($_SESSION['SESS_CART']);
$obj->checkmenu();
if(isset($_POST['login']))
{
    extract($_POST);
    $obj->login($email,$password,"account.php");
}
$obj->newcart($_SESSION['SESS_CART']);
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>Mobile online store</title>
        <meta name="description" content="Mobile online store" />
        <link href="images/favicon.png" rel="icon" />
        <?php include('include/headlink.php'); ?>
        

    </head>
    <body class="common-home">
        <div class="bg-1">
            <div class="main-shining">
                <div class="row-1">
                    <?php include('include/header.php'); ?>
                    <div class="clear"></div>
<?php include('include/menu.php'); ?>
                </div>
                <div class="clear"></div>

<?php //include('include/slider_home.php');   ?>

                <div class="main-container">
                    <p id="back-top"> <a href="#top"><span></span></a> </p>
                    <div id="container">
                        <?php if(isset($_SESSION['SESS_CUSID'])!='') { ?>
                        <div class='successmsg'  style=''><?php echo "Welcome ".$_SESSION['SESS_CUSNAME']." Sir, Today is ".date('D, d M, Y'); ?><a href="ajax/logout.php" style="float: right; margin-top:-4px;" class="button"><span>Logout</span></a></div>
                        <?php } ?>
                        <div id="notification"> </div>

<?php include('include/category.php'); ?>

                        <div id="content">

                            
                            <div class="breadcrumb">
                                <a href="index.php">Home</a>
                                » <a class="last" href="shoppingcart.php">Order Complete</a>
                            </div>
                            <div class="checkout">
                                <h1>Your Order Has Been Processed!</h1>
                                <p>Your order has been successfully processed!</p><p>You can view your order history by going to the <a href="account.php">my account</a> page and by clicking on <a href="account.php">My Account ->> Order history</a>.</p><p> page to view them.</p><p>Please direct any questions you have to the <a href="contact.php">store owner</a>.</p><p>Thanks for shopping with us online!</p>    <div class="buttons">
        <div class="right"><a href="index.php" class="button"><span>Continue</span></a></div>
    </div>
                            </div>




                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
<?php include('include/footer.php'); ?>
            </div>
        </div>
        <script type="text/javascript"></script>
    </body>
</html>
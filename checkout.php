<?php
session_start();
include('ajax/db_Class.php');
$obj = new db_class();
@$cart = $obj->cart($_SESSION['SESS_CART']);
$obj->checkmenu();
if(isset($_POST['login']))
{
    extract($_POST);
    $obj->login($email,$password,"account.php");
}

?>
<!DOCTYPE html>
<html dir="ltr" lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>Mobile online store</title>
        <meta name="description" content="Mobile online store" />
        <link href="images/favicon.png" rel="icon" />
<?php include('include/headlink.php'); ?>
        <script>
            function checkcoutas(str)
            {
                if (str == "")
                {
                    document.getElementById("step2").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        $("#step2").fadeOut(500);
                        $("#step2").fadeIn(500);
                        document.getElementById("step2").innerHTML = xmlhttp.responseText;
                    }
                }
                
                if(str==1)
                {
                    window.location="create_account.php";
                }
                elseif(str==2)
                {
                    email = document.getElementById('email').value;
                    password = document.getElementById('password').value;

                    xmlhttp.open("GET", "ajax/step2.php?q="+str+"&email="+email+"&password="+password, true);
                    xmlhttp.send();
                }
            }
        </script>
        <script>
            function step2account(str)
            {
                if (str == "")
                {
                    document.getElementById("account_ss").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        $("#account_ss").fadeOut(500);
                        $("#account_ss").fadeIn(500);
                        document.getElementById("account_ss").innerHTML = xmlhttp.responseText;
                    }
                }
                
                
                //xmlhttp.open("GET", "ajax/step2_success.php?q="+str+"&fname"+fname+"&lname"+lname+"&em"+em+"&telephone"+telephone+"&fax"+fax+"&pass"+pass+"&company"+company+"&address1"+address1+"&address2"+address2+"&city"+city+"&postcode"+postcode+"&country"+country+"&state"+state, true);
                xmlhttp.open("GET", "ajax/step2_success.php?q="+str, true);
                xmlhttp.send();
            }
        </script>
        <script>
            function step2(str)
            {
                if (str == "")
                {
                    document.getElementById("step3").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        $("#step3").fadeOut(500);
                        $("#step3").fadeIn(500);
                        document.getElementById("step3").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET", "ajax/step3.php?q=" + str, true);
                xmlhttp.send();
            }
        </script>
        <script>
            function newaddress(str)
            {
                if (str == "")
                {
                    document.getElementById("address").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        $("#address").fadeOut(500);
                        $("#address").fadeIn(500);
                        document.getElementById("address").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET", "ajax/address.php?q=" + str, true);
                xmlhttp.send();
            }
        </script>
        <script>
            function step3_save(str)
            {
                if (str == "")
                {
                    document.getElementById("step3").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        $("#step3").fadeOut(500);
                        $("#step3").fadeIn(500);
                        document.getElementById("step3").innerHTML = xmlhttp.responseText;
                    }
                }
                firstname=document.getElementById('firstname').value;
                lastname=document.getElementById('lastname').value;
                address=document.getElementById('address').value;
                city=document.getElementById('city').value;
                postcode=document.getElementById('postcode').value;
                country=document.getElementById('country_id').value;
                state=document.getElementById('state').value;
                
                
                xmlhttp.open("GET", "ajax/address_save.php?q="+str+"&firstname="+firstname+"&lastname="+lastname+"&address="+address+"&city="+city+"&postcode="+postcode+"&country="+country+"&state="+state, true);
                xmlhttp.send();
            }
        </script>
        <script>
            function step3(str)
            {
                if (str == "")
                {
                    document.getElementById("step4").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        $("#step4").fadeOut(500);
                        $("#step4").fadeIn(500);
                        document.getElementById("step4").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET", "ajax/step4.php?q=" + str, true);
                xmlhttp.send();
            }
        </script>
        <script>
            function step4(str)
            {
                if (str == "")
                {
                    document.getElementById("step5").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        $("#step5").fadeOut(500);
                        $("#step5").fadeIn(500);
                        document.getElementById("step5").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET", "ajax/step5.php?q=" + str, true);
                xmlhttp.send();
            }
        </script>
        <script>
            
            function get_radio_value() {
            var inputs = document.getElementsByName("sm");
            for (var i = 0; i < inputs.length; i++) {
              if (inputs[i].checked) {
                return inputs[i].value;
              }
            }
          }

          //function onSubmit() {
            //var id = get_radio_value();
           // alert("selected input is: " + id);
          //}
            
            function step5(str)
            {
                if (str == "")
                {
                    document.getElementById("step6").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        $("#addf").hide();
                        $("#step2").hide();
                        $("#step3").hide();
                        $("#step4").hide();
                        $("#step5").hide();
                        $("#step6").fadeOut(500);
                        $("#step6").fadeIn(1000);
                        document.getElementById("step6").innerHTML = xmlhttp.responseText;
                    }
                }
                address_id=document.getElementById('address_id').value;
                var shipping_cost=get_radio_value();
                shipping_comment=document.getElementById('shipping_comment').value;
                methodcomment=document.getElementById('methodcomment').value;
                var payment = 1;
                if(document.getElementById('cod_0').checked) {
                  payment = 1;
                }else if(document.getElementById('cod_1').checked) {
                  payment = 2;
                }
                
                xmlhttp.open("GET", "ajax/step6.php?q=" + str+"&shipping_cost="+shipping_cost+"&shipping_comment="+shipping_comment+"&methodcomment="+methodcomment+"&payment="+payment+"&address="+address_id, true);
                xmlhttp.send();
            }
        </script>
        

    </head>
    <body class="common-home">
        <div class="bg-1">
            <div class="main-shining">
                <div class="row-1">
                    <?php include('include/header.php'); ?>
                    <div class="clear"></div>
<?php include('include/menu.php'); ?>
                </div>
                <div class="clear"></div>

<?php //include('include/slider_home.php');   ?>

                <div class="main-container">
                    <p id="back-top"> <a href="#top"><span></span></a> </p>
                    <div id="container">
                        <?php if(isset($_SESSION['SESS_CUSID'])!='') { ?>
                        <div class='successmsg'  style=''><?php echo "Welcome ".$_SESSION['SESS_CUSNAME']." Sir, Today is ".date('D, d M, Y'); ?><a href="ajax/logout.php" style="float: right; margin-top:-4px;" class="button"><span>Logout</span></a></div>
                        <?php } ?>
                        <div id="notification"> </div>

<?php include('include/category.php'); ?>

                        <div id="content">

                            
                            <div class="breadcrumb">
                                <a href="index.php">Home</a>
                                » <a class="last" href="shoppingcart.php">Shopping Cart</a>
                            </div>
                            <div class="checkout">
                                <?php 
                                if($obj->exists_multiple("cart",array("cart_id"=>$cart))==0)
                                {
                                    echo "<h3>Shopping Cart Is Empty</h3>";
                                }
                                else
                                {
                                ?>
                                <h1>Checkout</h1>
                                <div id="checkout">
                                    <div class="checkout-heading"><div class="marker-chekout">Step 1: Checkout Options</div></div>
                                    <?php if(isset($_SESSION['SESS_CUSID'])=='') { ?>
                                    <div style="display: block;" class="checkout-content opt"><div class="left">
                                            <h2>New Customer</h2>
                                            <p>Checkout Options:</p>
                                            <div class="extra-wrap">  <div class="p1"><label for="register">
                                                        <input name="account" value="1" id="user_0" checked="checked" type="radio">
                                                        <b>Register Account</b></label></div></div>

                                            <div class="extra-wrap">
                                                <label for="guest">
                                                    <input name="account"  value="2"  id="user_1" type="radio">
                                                    <b>Guest Checkout</b></label>
                                            </div>

                                            <br>
                                            <p class="login-padd">By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.</p>
                                            <a id="button-account" onclick="checkcoutas('1')" class="button"><span>Continue</span></a><br>
                                            <br>
                                        </div>
                                        <div id="login" class="right">
                                        <form action="" method="post" enctype="multipart/form-data" id="login">
                                            <div class="content">
                                                <p>I am a returning customer</p>
                                                <b class="padd-form">E-Mail Address:</b>
                                                <input class="q1 margen-bottom" type="text" name="email" value="" />

                                                <b class="padd-form">Password:</b>
                                                <input class="q1 margen-bottom" type="password" name="password" value="" />
                                                <br />
                                                <div>
                                                    <a class="link-login" href="forgotten.php">Forgotten Password</a>
                                                </div>
                                                <button type="submit" name="login" class="btn"><span>Login</span></button><button type="submit" style="margin-left: 10px;" name="login" class="btn"><span>Reset</span></button>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                                <div id="payment-address">
                                    <div class="checkout-heading"><div class="marker-chekout"><span>Step 2: Account &amp; Billing Details</span></div></div>
                                    <?php if(isset($_SESSION['SESS_CUSID'])==''){ ?>
                                    <div id="step2"></div>
                                    <?php }else{ ?>
                                    <div style="display: block;" id="addf" class="checkout-content">
                                        <input name="shipping_address" onchange="newaddress('1')" value="existing" id="shipping-address-existing" checked="checked"  type="radio">
                                        <label for="shipping-address-existing">I want to use an existing address</label>
                                        
                                        <input name="shipping_address" onchange="newaddress('2')" value="existing" id="shipping-address-existing"type="radio">
                                        <label for="shipping-address-existing">I want to use a New Shipping address</label>
                                        
                                        
                                            <div id="address">
                                                <div id="shipping-existing">
                                                    <select name="address_id" id="address_id" style="width: 100%; margin-bottom: 15px;" size="5">
                                                        <?php
                                                        $data=$obj->SelectAllByID("shipping_address",array("cusid"=>$_SESSION['SESS_CUSID']));
                                                        if(!empty($data))
                                                        foreach($data as $row):
                                                        ?>
                                                        <option value="<?php echo $row->address; ?>" selected="selected"><?php echo $row->address; ?></option>
                                                        <?php 
                                                        endforeach;
                                                        ?>
                                                    </select>
                                                </div>
                                                
                                                <br>
                                                <div class="buttons">
                                                    <div class="right">
                                                        <a id="button-shipping-address" onclick="step3('1')" class="button"><span>Continue</span></a>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            
                                            

                                        </div>
                                    <?php } ?>
                                </div>
                                <div id="shipping-address">
                                    <div class="checkout-heading"><div class="marker-chekout">Step 3: Delivery Details</div></div>
                                    <div id="step3">
                                        
                                    </div>

                                </div>
                                <div id="shipping-method">
                                    <div class="checkout-heading"><div class="marker-chekout">Step 4: Delivery Method</div></div>
                                    <div id="step4"></div>

                                </div>
                                <div id="payment-method">
                                    <div class="checkout-heading"><div class="marker-chekout">Step 5: Payment Method</div></div>
                                    <div id="step5">
                                        
                                    </div>
                                </div>
                                <div id="confirm">
                                    <div class="checkout-heading"><div class="marker-chekout">Step 6: Confirm Order</div></div>
                                    <div id="step6"></div>
                                </div>
                                
                                <?php } ?>
                            </div>




                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
<?php include('include/footer.php'); ?>
            </div>
        </div>
        <script type="text/javascript"></script>
    </body>
</html>
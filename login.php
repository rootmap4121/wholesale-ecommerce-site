<?php 
session_start();
$errmsg_arr[]="";
$errflag = false;
include('ajax/db_Class.php');
$obj = new db_class();
@$cart = $obj->cart($_SESSION['SESS_CART']);
if(isset($_POST['login']))
{
    extract($_POST);
    $obj->login($email,$password,"account.php");
}
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>Mobile online store</title>
        <meta name="description" content="Mobile online store" />
        <link href="images/favicon.png" rel="icon" />
        <?php include('include/headlink.php'); ?>
    </head>
    <body class="account-login">
        <div class="bg-1">
            <div class="main-shining">
                <div class="row-1">
                    <?php include('include/header.php'); ?>
                    <div class="clear"></div>
                    <?php include('include/menu.php'); ?>
                </div>
                <div class="clear"></div>

                <div class="main-container">
                    <p id="back-top"> <a href="#top"><span></span></a> </p>
                    <div id="container"><?php echo $obj->ShowMsg(); ?>
                        <div id="notification"> </div>
                        
                           <?php include('include/category.php'); ?>
                        
                        <div id="content">  <div class="breadcrumb">
                                <a href="index.php">Home</a>
                                &raquo; <a href="account.php">Account</a>
                                &raquo; <a href="login.php">Login</a>
                            </div>

                            <div class="box-container">
                                <h1>Account Login</h1>
                                <div class="login-content">
                                    <div class="left">
                                        <h2>New Customer</h2>
                                        <div class="content">
                                            <p><b>Register Account</b></p>
                                            <p>By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.</p>
                                            <a href="create_account.php" class="button"><span>Continue</span></a></div>
                                    </div>
                                    <div class="right">
                                        <h2>Returning Customer</h2>
                                        <form action="login.php" method="post" enctype="multipart/form-data" id="login">
                                            <div class="content">
                                                <p>I am a returning customer</p>
                                                <b class="padd-form">E-Mail Address:</b>
                                                <input class="q1 margen-bottom" type="text" name="email" value="" />

                                                <b class="padd-form">Password:</b>
                                                <input class="q1 margen-bottom" type="password" name="password" value="" />
                                                <br />
                                                <div>
                                                    <a class="link-login" href="forgotten.php">Forgotten Password</a>
                                                </div>
                                                <button type="submit" name="login" class="btn"><span>Login</span></button><button type="submit" style="margin-left: 10px;" name="login" class="btn"><span>Reset</span></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript"><!--
                        $('#login input').keydown(function(e) {
                                if (e.keyCode == 13) {
                                    $('#login').submit();
                                }
                            });
//--></script> 
                        <div class="clear"></div>
                    </div>
                </div>
                <?php include('include/footer.php'); ?>
                <script type="text/javascript" 	src="js/livesearch.js"></script>
            </div>
        </div>
        <script type="text/javascript"></script>
    </body>
</html>
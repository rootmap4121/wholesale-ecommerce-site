<?php session_start();
include('ajax/db_Class.php');
$obj = new db_class();
@$cart = $obj->cart($_SESSION['SESS_CART']); ?>
<!DOCTYPE html>
<html dir="ltr" lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>Mobile online store</title>
        <meta name="description" content="Mobile online store" />
        <link href="images/favicon.png" rel="icon" />
<?php include('include/headlink.php'); ?>
    </head>
    <body class="common-home">
        <div class="bg-1">
            <div class="main-shining">
                <div class="row-1">
                    <?php include('include/header.php'); ?>
                    <div class="clear"></div>
<?php include('include/menu.php'); ?>
                </div>
                <div class="clear"></div>

<?php //include('include/slider_home.php');  ?>

                <div class="main-container">
                    <p id="back-top"> <a href="#top"><span></span></a> </p>
                    <div id="container">
                        <div id="notification"> </div>

<?php include('include/category.php'); ?>

                        <div id="content">
                            
                           


<div class="breadcrumb">
    <a href="index.php">Home</a>
    » <a class="last" href="delivery.php">Delivery</a>
      </div>
  
  <div class="box-container">
    <h1>Delivery</h1>
    <div class="about-page">
	<h3>
		FedEx Shipping.</h3>
	<p>
		We offer, FedEx shipping services for non local dealers & wholesales. We ship with insurance, & delivery confirmation. All orders placed before <strong>3PM</strong> will be shipped same day. Any orders placed after 3PM will be shipped the following business days. We do not ship orders on sundays. The FedEx shipping we offer is below.</p>
	<ul>
		<li>
			FedEx Ground</li>
		<li>
			FedEx Express Saver</li>
		<li>
			FedEx 2 Day</li>
		<li>
			FedEx Standard Overnight</li>
	</ul>
</div>
<div class="about-page">
	<h3>
		USPS Shipping.</h3>
	<p>
		We offer, USPS shipping services for non local dealers & wholesales. We ship with insurance, & delivery confirmation. All orders placed before <strong>3PM</strong> will be shipped same day. Any orders placed after 3PM will be shipped the following business days. We do not ship orders on sundays. The USPS shipping we offer is below. </p>
        <ul>
		<li>
			USPS Standard Flat Rat</li>
		<li>
			USPS Priority Mail 2 Day</li>
		<li>
			USPS Priority Mail Express 1 Day</li>
	</ul>
	
</div>
<div class="about-page">
	<h3>
		Local Deliveries</h3>
	<p>
		We offer, Local Deliveries to local dealers & wholesales. We deliver the product right to you with no shipping charge. No need to be out of parts, or pay for shipping from other suppliers. Order from Wirelss Geeks & get your parts today. <strong>There is no cut off time </strong> for local orders. As long as the order is placed from <strong>9AM-7PM</strong> then you will get your order that day. Below is a list of local citys we deliver to.</p>
	 <ul>
		<li>
			Detroit</li>
		<li>
			Hazel Park</li>
		<li>
			Dearborn</li>
        <li>
			Clinton Township</li>
            <li>
			Taylor</li>
		<li>
			Highland Park</li>
		<li>
			West Bloomfield</li>
        <li>
			Farmington Hills</li>
            <li>
			Birmingham</li>
		<li>
			Royal Oak</li>
		<li>
			Pontiac</li>
        <li>
			Redford</li>
            <li>
			Auburn Hills</li>
		<li>
			Grosse Pointe</li>
		<li>
			Hamtramck</li>
        <li>
			Rochester</li>
            <li>
			Rochester Hills</li>
            <li>
			Roseville</li>
            <li>
			Saint Clair Shores</li>
            <li>
			Shelby Township</li>
            <li>
			Sterling Heights</li>
            <li>
			Troy</li>
            <li>
			Waterford</li>
            <li>
			Utica</li>
            <li>
			White Lake</li>
            <li>
			Walled Lake</li>
            <li>
			Oak Park</li>
            <li>
			Novi</li>
            <p> If you are a dealer/wholesale & our in one of the cities listed above, we will deliver your order for free <strong> SAME DAY! </strong> Or you can even pick it up from our office in Madison Heights. 
	</ul>
</div>
<p>&nbsp;
	</p>
    <div class="buttons">
        <div class="right"><a href="index.php" class="button-inf-left"><span>Continue</span></a></div>
    </div>
  </div>




                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
<?php include('include/footer.php'); ?>
                <script type="text/javascript" 	src="js/livesearch.js"></script>
            </div>
        </div>
        <script type="text/javascript"></script>
    </body>
</html>
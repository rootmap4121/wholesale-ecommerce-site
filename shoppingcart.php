<?php 
session_start(); 
include('ajax/db_Class.php'); $obj = new db_class();
@$cart=$obj->cart($_SESSION['SESS_CART']);
$obj->checkmenu();
if(isset($_POST['shiping']))
{
    extract($_POST);
    $chk=$obj->exists_multiple("shipping_cost",array("country"=>$country,"state"=>$state));
    if($chk!=0)
    {
        
        $ship=$obj->SelectAllByVal2("shipping_cost","country",$country,"state",$state,"cost");
        if($ship!=0)
        {
            $shipping=$ship;
        }
        else
        {
            $shipping=0;
        }
        if($obj->exists_multiple("order_shipping",array("cart_id"=>$cart))==1)
        {
            if($obj->update("order_shipping",array("cart_id"=>$cart,"shipping"=>$shipping))==1)
            {
                $obj->Success("Shipping Cost Updated",$obj->filename());
            }
            else
            {
                $obj->Error("Shipping Cost update Failed",$obj->filename());
            }
        }
        else
        {
            if($obj->insert("order_shipping",array("cart_id"=>$cart,"shipping"=>$shipping))==1)
            {
                $obj->Success("Shipping Cost Added",$obj->filename());
            }
            else
            {
                $obj->Error("Shipping Cost Added Failed",$obj->filename());
            }
        }
    }
    else 
    {
        $obj->Error("This Country/State Shipping is Not Available in our System",$obj->filename());
    }
}

if(isset($_GET['cartdel']))
{
    if($obj->delete("cart",array("id"=>$_GET['cartdel']))==1)
    {
        $obj->delete("order_discount",array("cart_id"=>$cart));
        $obj->Success("Data Successfully Removed From Cart",$obj->filename());
    }
 else {
        $obj->Success("Data Failed Removed From Cart",$obj->filename());
    }
}
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>Mobile online store</title>
        <meta name="description" content="Mobile online store" />
        <link href="images/favicon.png" rel="icon" />
        <?php include('include/headlink.php'); ?>
        <script>
            function discount(str)
            {
                if (str == "")
                {
                    document.getElementById("discount").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        $("#discount").fadeOut(500);
                        $("#discount").fadeIn(500);
                        document.getElementById("discount").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET", "ajax/discount.php?q=" + str, true);
                xmlhttp.send();
            }
         </script>
         <script>
            function cuponcode(str)
            {
                if (str == "")
                {
                    document.getElementById("totalcost").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        $("#totalcost").fadeOut(500);
                        $("#totalcost").fadeIn(500);
                        document.getElementById("code").value ='';
                        document.getElementById("totalcost").innerHTML = xmlhttp.responseText;
                    }
                }
                subtotal=document.getElementById('subtotal').value;
                code=document.getElementById('code').value;
                xmlhttp.open("GET", "ajax/cupon.php?q="+str+"&subtotal="+subtotal+"&code="+code, true);
                xmlhttp.send();
            }
         </script>
         
         
         
         
         
    </head>
    <body class="common-home">
        <div class="bg-1">
            <div class="main-shining">
                <div class="row-1">
                    <?php include('include/header.php'); ?>
                    <div class="clear"></div>
<?php include('include/menu.php'); ?>
                </div>
                <div class="clear"></div>

<?php //include('include/slider_home.php');  ?>

                <div class="main-container">
                    <p id="back-top"> <a href="#top"><span></span></a> </p>
                    <div id="container">
                        <div id="notification"> </div>

<?php include('include/category.php'); ?>

                        <div id="content">
                            
                           
  <div class="breadcrumb">
      <a href="index.php">Home</a>
      » <a class="last" href="shoppingcart.php">Shopping Cart</a>
      </div>
                            
<div class="box-container">
    <?php 
    echo $obj->ShowMsg();
    $chkcart=$obj->exists_multiple("cart",array("cart_id"=>$cart));
    ?>
    <h1>Shopping Cart &nbsp; Item (<?php echo $chkcart; ?>)
      </h1>
        <form action="#" method="post" enctype="multipart/form-data">
    <div class="cart-info">
      <div class="shop-cart">
      <table>
        <thead>
          <tr>
            <td class="image">Image</td>
            <td class="name">Product Name</td>
            <td class="model">Category</td>
            <td class="quantity">Quantity</td>
            <td class="price">Unit Price</td>
            <td class="total" style="border-right:none;">Total</td>
          </tr>
        </thead>
        <tbody>
                   
            <?php
            $data=$obj->SelectAllByID("cart",array("cart_id"=>$cart));
            $stotal=0;
            if(!empty($data))
            foreach($data as $rr):
            ?>
            <tr>
            <td class="image">              
                <a href="">
                    <img height="47" width="47" src="product/<?php echo $obj->SelectAllByVal("product","id",$rr->pid,"photo"); ?>" alt="<?php echo $obj->SelectAllByVal("product","id",$rr->pid,"name"); ?>" title="<?php echo $obj->SelectAllByVal("product","id",$rr->pid,"name"); ?>">
                </a>
              </td>
            <td class="name">
                <a href=""><?php echo $obj->SelectAllByVal("product","id",$rr->pid,"name"); ?></a>
                            <div>
                              </div>
              </td>
            <td class="model"><?php $cid=$obj->SelectAllByVal("product","id",$rr->pid,"cid");
                echo $obj->SelectAllByVal("category","id",$cid,"name");
                ?></td>
            <td class="quantity"><input name="quantity[]" disabled="disabled" value="<?php echo $rr->quantity; ?>" size="1" type="text">
              
              <div class="wrapper mt5">
              <input src="images/update.png" alt="Update" title="Update" type="image">
              &nbsp;<a href="<?php echo $obj->filename(); ?>?cartdel=<?php echo $rr->id; ?>"><img src="images/remove.png" alt="Remove" title="Remove"></a>
              </div>
              </td>
              <td class="price">$<?php $unite=$obj->SelectAllByVal("product","id",$rr->pid,"price"); echo number_format($unite,2); ?></td>
            <td class="total" style="border-right:0;">$<?php $tprice=$rr->quantity*$unite; echo number_format($tprice,2); ?></td>
          </tr>
          <?php 
          $stotal+=$tprice;
          endforeach; ?>   
                
                            </tbody>
      </table>
          </div>
    </div>
  </form>
     <h2>What would you like to do next?</h2>
  <div class="content shop-cart-content">
   
    <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
    <table class="radio">
            <tbody><tr class="highlight">
                    <td><input name="next" value="coupon" onclick="discount('1')" id="use_coupon" type="radio"></td>
        <td><label for="use_coupon">Use Coupon Code</label></td>
      </tr>
      <!--<tr class="highlight">
          <td><input name="next" value="shipping" onclick="discount('2')" id="shipping_estimate" type="radio"></td>
        <td><label for="shipping_estimate">Estimate Shipping &amp; Taxes</label></td>
      </tr>-->
          </tbody></table>
  </div>
     <div class="cart-module" id="discount">
     
     </div>
    
  <div class="cart-bottom">
      <div class="cart-total" id="totalcost">
          <input type="hidden" name="subtotal" id="subtotal" value="<?php echo $stotal; ?>">
    <table id="total">
            <tbody><tr class="row-table-1">
        <td class="right cart-total-name "><b>Sub-Total:</b></td>
        <td class="right cart-total1 ">$<?php echo number_format($stotal,2); ?></td>
      </tr>
      <?php
      $datad=$obj->SelectAllByID("order_discount",array("cart_id"=>$cart));
      $discoun=0;
      if(!empty($datad))
      foreach($datad as $dd):
          $discoun+=$dd->discount_amount;
      ?>
      <tr class="row-table-4">
        <td class="right cart-total-name "><b>(-) Cupon Discount :</b></td>
        <td class="right cart-total1 ">$<?php echo number_format($dd->discount_amount,2); ?></td>
      </tr>
      <?php
        endforeach;
        
        $shipp=$obj->SelectAllByVal("order_shipping","cart_id",$cart,"shipping");
        if($shipp!=0 || $shipp!='')
        {
            $shipping=$shipp;
         ?>
      <tr class="row-table-4">
        <td class="right cart-total-name "><b>(+) Shipping Cost :</b></td>
        <td class="right cart-total1 ">$<?php echo number_format($shipp,2); ?></td>
      </tr>
      <?php   
        }
        else 
        {
            $shipping=0;
        }
        
        $gt=($stotal+$shipping)-$discoun;
      ?>
      <tr class="row-table-4">
        <td class="right cart-total-name last"><b>Total:</b></td>
        <td class="right cart-total1 last">$<?php echo number_format($gt,2); ?></td>
      </tr>
          </tbody></table>
  </div>
    <div class="buttons">
        <div class="right"><a href="checkout.php" class="button"><span>Checkout</span></a></div>
    <div class="center"><a href="index.php" class="button"><span>Continue Shopping</span></a></div>
  </div>
  </div>
  </div>




                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
<?php include('include/footer.php'); ?>
            </div>
        </div>
        <script type="text/javascript"></script>
    </body>
</html>
<?php session_start();
include('ajax/db_Class.php');
$obj = new db_class();
@$cart = $obj->cart($_SESSION['SESS_CART']); ?>
<!DOCTYPE html>
<html dir="ltr" lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>Mobile online store</title>
        <meta name="description" content="Mobile online store" />
        <link href="images/favicon.png" rel="icon" />
<?php include('include/headlink.php'); ?>
    </head>
    <body class="common-home">
        <div class="bg-1">
            <div class="main-shining">
                <div class="row-1">
                    <?php include('include/header.php'); ?>
                    <div class="clear"></div>
<?php include('include/menu.php'); ?>
                </div>
                <div class="clear"></div>

<?php //include('include/slider_home.php');  ?>

                <div class="main-container">
                    <p id="back-top"> <a href="#top"><span></span></a> </p>
                    <div id="container">
                        <div id="notification"> </div>

<?php include('include/category.php'); ?>

                        <div id="content">
                            
                           



                            <div class="breadcrumb">
                                <a href="index.php">Home</a>
                                » <a class="last" href="brand.php">Brand</a>
      </div>
  
    <div class="box-container">
    <h1>Find Your Favorite Brand</h1>
  <p class="manuf-p"><b>Brand Index:</b>
        &nbsp;&nbsp;&nbsp;<a href="category.php?cid=20"><b>C</b></a>
        &nbsp;&nbsp;&nbsp;<a href="category.php?cid=20"><b>D</b></a>
        &nbsp;&nbsp;&nbsp;<a href="category.php?cid=20"><b>E</b></a>
        &nbsp;&nbsp;&nbsp;<a href="category.php?cid=20"><b>L</b></a>
      </p>
    <div class="manufacturer-list">
    <div class="manufacturer-heading"><span>C</span><a id="C"></a></div>
    <div class="manufacturer-content">
                  <ul>
                                <li><a href="category.php?cid=20">Cras sit amet purus</a></li>
                      </ul>
                </div>
  </div>
  
    <div class="manufacturer-list">
    <div class="manufacturer-heading"><span>D</span><a id="D"></a></div>
    <div class="manufacturer-content">
                  <ul>
                                <li><a href="category.php?cid=20">Donec ut varius</a></li>
                      </ul>
            <ul>
                                <li><a href="category.php?cid=20">Douris in sem metus</a></li>
                      </ul>
                </div>
  </div>
  
    <div class="manufacturer-list">
    <div class="manufacturer-heading"><span>E</span><a id="E"></a></div>
    <div class="manufacturer-content">
                  <ul>
                                <li><a href="category.php?cid=20">Et dolore magna aliqua</a></li>
                      </ul>
            <ul>
                                <li><a href="category.php?cid=20">Etiam facilisis fermentum</a></li>
                      </ul>
                </div>
  </div>
  
    <div class="manufacturer-list">
    <div class="manufacturer-heading"><span>L</span><a id="L"></a></div>
    <div class="manufacturer-content">
                  <ul>
                                <li><a href="category.php?cid=20">Lorem ipsum dolor</a></li>
                      </ul>
                </div>
  </div>
  
    </div>




                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
<?php include('include/footer.php'); ?>
                <script type="text/javascript" 	src="js/livesearch.js"></script>
            </div>
        </div>
        <script type="text/javascript"></script>
    </body>
</html>
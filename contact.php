<?php 
session_start();
include('ajax/db_Class.php');
$obj = new db_class();
@$cart = $obj->cart($_SESSION['SESS_CART']); 
if(isset($_POST['send']))
{
    // Receive form's subject value into php $subject variable
    $subject = "Wireless Geeks Product Return Email";
    extract($_POST);
    // Receive form's message value into php $message variable
    $message = " Full Name : " . $name."\n";
    $message .= "Email : " . $email."\n";
    $message .= "Enquiry : " . $comment."\n";

    // Receive form's sender name value into php $sender variable
    $sender = "admin@wireless-geeks.com";
    //$sender = "contact@amsitsoft.com";

    // Receive form's user email value into php $user_email variable
    $user_email = "admin@wireless-geeks.com";

    // the email address where the message will be sent
    $TO ="admin@wireless-geeks.com";

    $send_email = mail($TO, $subject, $message, "From: " . $sender . " || " . $user_email . ">");

    // To check email has been sent or not
    if ($send_email) {
           $obj->Success("Successfully Send, Admin Will Contact with you shortly in next 48 hours.",$obj->filename());
    } else {
           $obj->Error("Send Error",$obj->filename());
    }
}

?>
<!DOCTYPE html>
<html dir="ltr" lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>Mobile online store</title>
        <meta name="description" content="Mobile online store" />
        <link href="images/favicon.png" rel="icon" />
<?php include('include/headlink.php'); ?>
    </head>
    <body class="common-home">
        <div class="bg-1">
            <div class="main-shining">
                <div class="row-1">
                    <?php include('include/header.php'); ?>
                    <div class="clear"></div>
<?php include('include/menu.php'); ?>
                </div>
                <div class="clear"></div>

<?php //include('include/slider_home.php');  ?>

                <div class="main-container">
                    <p id="back-top"> <a href="#top"><span></span></a> </p>
                    <div id="container">
                        <div id="notification"> </div>

<?php include('include/category.php'); ?>

                        <div id="content">
                            
                           


<div class="breadcrumb">
    <a href="index.php">Home</a>
    » <a class="last" href="contact.php">Contact Us</a>
      </div>
  
                            <form action="contact.php" method="post" enctype="multipart/form-data" id="contact">
    
    <h2 style="display:none">Our Location</h2>
    <div class="contact-info">
    <h1>Contact Us</h1>
      <div class="content">
<div class="map-left">      <div class="contact-box"><b>Address:</b><br>
        8901 Marmora Road, Glasgow,<br>
D04 89GR</div>
      <div class="contact-box-2">
                <b>Telephone:</b><br>
        1 800 234-5678<br>
        <br>
                        <b>Fax:</b><br>
        1 800 234-5678              </div></div>
               <div class="map-content">         
               <figure>
               <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2941.440456005736!2d-83.085933!3d42.50344700000001!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8824c556c7c9d6fd%3A0x1046be94a1a82bae!2sWireless+Geeks!5e0!3m2!1sen!2s!4v1397732013710" width="300" height="243" frameborder="0" style="border:0"></iframe>
                  </figure></div>
    </div>
    </div>
    <?php
      echo $obj->ShowMsg();
      ?>
    <div class="content contact-f"><h2>Contact Form</h2>
        
<div class="padd-content">   <div class="extra-wrap"> <div class="contact-form-left"><b>First Name:</b><br>
    <input class="q2" name="name" value="" type="text">
    <br>
    </div>
    <div class="contact-form-right"><b>E-Mail Address:</b><br>
    <input class="q2" name="email" value="" type="text">
    <br>
    </div></div></div>
    <b>Enquiry:</b><br>
    <textarea name="comment" cols="40" rows="10" style="width: 99%;"></textarea>
        <br>
    <b class="cap-p">Enter the code in the box below:</b><br>
    <input class="capcha" name="captcha" value="" type="text">
    <br>
    <div class="buttons">    <img src="images/captcha.jpg" alt="">
        <div class="right"><button class="btn" type="submit" name="send"><span>Send Now</span></button></div>
    </div>
    </div>

  </form>




                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
<?php include('include/footer.php'); ?>
                <script type="text/javascript" 	src="js/livesearch.js"></script>
            </div>
        </div>
        <script type="text/javascript"></script>
    </body>
</html>
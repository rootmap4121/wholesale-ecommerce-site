<?php 
session_start(); 
include('ajax/db_Class.php'); $obj = new db_class();
@$cart=$obj->cart($_SESSION['SESS_CART']);
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>Mobile online store</title>
        <meta name="description" content="Mobile online store" />
        <link href="images/favicon.png" rel="icon" />
        <?php include('include/headlink.php'); ?>
        <script>
            function CartAdd(str)
            {
                if (str == "")
                {
                    document.getElementById("haha").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        $("html, body").animate({ scrollTop: 0 }, "slow");
                        $("#haha").fadeOut(1000);
                        $("#haha").fadeIn(1000);
                        document.getElementById("haha").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET", "ajax/cart.php?q=" + str, true);
                xmlhttp.send();
                
                
                var xmlhttps;    
                if (str=="")
                  {
                  document.getElementById("dd").innerHTML="";
                  return;
                  }
                if (window.XMLHttpRequest)
                  {// code for IE7+, Firefox, Chrome, Opera, Safari
                  xmlhttps=new XMLHttpRequest();
                  }
                else
                  {// code for IE6, IE5
                  xmlhttps=new ActiveXObject("Microsoft.XMLHTTPS");
                  }
                xmlhttps.onreadystatechange=function()
                  {
                  if (xmlhttps.readyState==4 && xmlhttps.status==200)
                    {
                    $("#dd").fadeOut();
                    $("#dd").fadeIn();
                    document.getElementById("dd").innerHTML=xmlhttps.responseText;
                    }
                  }  
                xmlhttps.open("GET","ajax/cartinfo.php?q="+str,true);
                xmlhttps.send();
            }
            
        </script>
    </head>
    <body class="common-home">
        <div class="bg-1">
            <div class="main-shining">
                <div class="row-1">
                    <?php include('include/header.php'); ?>
                    <div class="clear"></div>
                    <?php include('include/menu.php'); ?>
                </div>
                <div class="clear"></div>

                <?php include('include/slider_home.php'); ?>

                <div class="main-container">
                    <p id="back-top"> <a href="#top"><span></span></a> </p>
                    <div id="container">
                        <div id="notification"> </div>

                        <?php include('include/category.php'); ?>

                        <div id="content"><div class="box featured">
                                <div class="box-heading"><span>Featured Products</span></div>
                                <div class="box-content">
                                    <div class="box-product">
                                        <ul>
                                            <?php
                                            $existsproduct = $obj->totalrows("product");
                                            if ($existsproduct != 0)
                                            $sqlfeture = $obj->SelectAll_Site("product", "DESC", 4);
                                            if (!empty($sqlfeture))
                                            $f = 1;
                                            $x=1;
                                            foreach ($sqlfeture as $feature):
                                            if($f==1){
                                            ?>
                                            <li class="first-in-line">
                                            <?php
                                            }
                                            elseif($f==4){
                                            $f==1;
                                            ?>
                                            <li class="last-in-line">        
                                            <?php }else{ ?>
                                            <li class="">
                                            <?php } ?>
                                                    <script type="text/javascript">
                                                        $(document).ready(function() {
                                                            $("a.colorbox<?php echo $x; ?>").colorbox({
                                                                rel: 'colorbox',
                                                                inline: true,
                                                                html: true,
                                                                width: '58%',
                                                                maxWidth: '780px',
                                                                height: '70%',
                                                                opacity: 0.6,
                                                                open: false,
                                                                returnFocus: false,
                                                                fixed: false,
                                                                title: false,
                                                                href: '.quick-view<?php echo $x; ?>',
                                                                current: 'Product {current} of {total}'
                                                            });
                                                        });
                                                    </script> 
                                                    <div class="inner-indent">

                                                        <div class="image2">
                                                            <a href="product_view.php?pid=<?php echo $feature->id; ?>"><img src="product/<?php echo $feature->photo2; ?>" alt="<?php echo $feature->name; ?>" /></a>
                                                        </div>

                                                        <div style="display:none;">
                                                            <div  class="quick-view<?php echo $f; ?> preview">
                                                                <div class="wrapper marg">
                                                                    <div class="left">
                                                                        <style type="text/css">
                                                                            .image3 a img{ width: 300px; height: 300px; }
                                                                        </style>
                                                                        <div class="image3">
                                                                            <a href="product_view.php?pid=<?php echo $feature->id; ?>"><img src="product/<?php echo $feature->photo4; ?>" height="90" width="90" alt="<?php echo $feature->name; ?>" /></a>

                                                                        </div>
                                                                    </div>
                                                                    <div class="right">
                                                                        <h2><?php echo $feature->name; ?></h2>
                                                                        <div class="inf">
                                                                            <span class="manufacture">Category : <a href="category.php?cid=<?php echo $feature->cid; ?>"><?php echo $obj->SelectAllByVal("category", "id", $feature->cid, "name"); ?></a></span>
                                                                            <span class="model">Sub - Category: <?php echo $obj->SelectAllByVal("subcategory", "id", $feature->scid, "name"); ?></span>
                                                                            <span class="prod-stock-2">Availability:</span>

                                                                            <span class="prod-stock">In Stock</span>
                                                                            <div class="price">
                                                                                <span class="text-price">Price: </span>
                                                                                <?php echo $obj->authprice($feature->price); ?>										  											  
                                                                            </div>
                                                                        </div>
                                                                        <div class="cart">
                                                                            <a data-id="<?php echo $feature->id; ?>;" onclick="CartAdd(<?php echo $feature->id; ?>)" class="button addToCart-1"><span>Add to cart</span></a>

                                                                            <a href="product_view.php?pid=<?php echo $feature->id; ?>" class="button details"><span>Details</span></a>
                                                                        </div>

                                                                        <div class="clear"></div>
                                                                        <div class="rating">
                                                                            <img height="18" src="images/stars-0.png" alt="0" />

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="description">
                                                                    <?php echo $feature->description; ?>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <a href="product_view.php?pid=<?php echo $feature->id; ?>"   rel="colorbox" class="colorbox<?php echo $x; ?> quick-view-button button"><span>Quick View</span></a>

                                                        <div class="inner">
                                                            <div class="f-left">
                                                                <div class="name"><a href="product_view.php?pid=<?php echo $feature->id; ?>"><?php echo $feature->name; ?></a></div>
                                                                <div class="price">
                                                                    <?php echo $obj->authprice($feature->price); ?>								  								</div>
                                                            </div>
                                                            <div class="cart">
                                                                <a data-id="<?php echo $feature->id; ?>;" onclick="CartAdd(<?php echo $feature->id; ?>)" class="button addToCart" style="position: absolute; right: 2px; bottom: 3px;"><span>Add to cart</span></a>
                                                            </div>
                                                            <div class="clear"></div>


                                                        </div>

                                                    </div>
                                                </li>
                                                <?php 
                                                $x++;
                                                $f++;
                                                endforeach; 
                                                ?>







                                        </ul>
                                        <div class="clear"></div>
                                        <div class="box_custom_image" style="margin-top:10px; width:780px;">
                                            <a href="#" class="border-1"><img src="images/img3.png"></a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
<?php include('include/footer.php'); ?>
                <script type="text/javascript" 	src="js/livesearch.js"></script>
            </div>
        </div>
        <script type="text/javascript"></script>
    </body>
</html>
<?php session_start();
include('ajax/db_Class.php');
$obj = new db_class();
@$cart = $obj->cart($_SESSION['SESS_CART']); ?>
<!DOCTYPE html>
<html dir="ltr" lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>Mobile online store</title>
        <meta name="description" content="Mobile online store" />
        <link href="images/favicon.png" rel="icon" />
<?php include('include/headlink.php'); ?>
    </head>
    <body class="common-home">
        <div class="bg-1">
            <div class="main-shining">
                <div class="row-1">
                    <?php include('include/header.php'); ?>
                    <div class="clear"></div>
<?php include('include/menu.php'); ?>
                </div>
                <div class="clear"></div>

<?php //include('include/slider_home.php');  ?>

                <div class="main-container">
                    <p id="back-top"> <a href="#top"><span></span></a> </p>
                    <div id="container">
                        <div id="notification"> </div>

<?php include('include/category.php'); ?>

                        <div id="content">
                            
                           



                            <div class="breadcrumb">
                                <a href="index.php">Home</a>
         » <a href="account.php">Account</a>
         » <a class="last" href="gift.php">Gift Voucher</a>
      </div>
  
  <div class="box-container">
    <h1>Purchase a Gift Certificate</h1>
    <p>This gift certificate will be emailed to the recipient after your order has been paid for.</p>
    <form action="gift.php" method="post" enctype="multipart/form-data" id="voucher">
    <table class="form">
      <tbody><tr>
        <td><span class="required">*</span> Recipient's Name:</td>
        <td><input class="q2" name="to_name" value="" type="text">
          </td>
      </tr>
      <tr>
        <td><span class="required">*</span> Recipient's Email:</td>
        <td><input class="q2" name="to_email" value="" type="text">
          </td>
      </tr>
      <tr>
        <td><span class="required">*</span> Your Name:</td>
        <td><input class="q2" name="from_name" value="" type="text">
          </td>
      </tr>
      <tr>
        <td><span class="required">*</span> Your Email:</td>
        <td><input class="q2" name="from_email" value="" type="text">
          </td>
      </tr>
      <tr>
        <td><span class="required">*</span> Gift Certificate Theme:</td>
        <td>                    <input name="voucher_theme_id" value="7" id="voucher-7" type="radio">
          <label for="voucher-7">Birthday</label>
                    <br>
                              <input name="voucher_theme_id" value="6" id="voucher-6" type="radio">
          <label for="voucher-6">Christmas</label>
                    <br>
                              <input name="voucher_theme_id" value="8" id="voucher-8" type="radio">
          <label for="voucher-8">General</label>
                    <br>
                    </td>
      </tr>
      <tr>
        <td>Message:<br><span class="help">(Optional)</span></td>
        <td><textarea name="message" cols="40" rows="5"></textarea></td>
      </tr>
      <tr>
        <td><span class="required">*</span> Amount:<br><span class="help">(Value must be between $1.00 and $1,000.00)</span></td>
        <td><input name="amount" value="25.00" size="5" type="text">
          </td>
      </tr>
    </tbody></table>
    <div class="buttons">
      <div class="right">I understand that gift certificates are non-refundable.                <input name="agree" value="1" type="checkbox">
                <a onclick="$('#voucher').submit();" class="button"><span>Continue</span></a></div>
      </div>
    
  </form>
  </div>



                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
<?php include('include/footer.php'); ?>
                <script type="text/javascript" 	src="js/livesearch.js"></script>
            </div>
        </div>
        <script type="text/javascript"></script>
    </body>
</html>
<?php
session_start();
$errmsg_arr[] = "";
$errflag = false;
include('ajax/db_Class.php');
$obj = new db_class();
@$cart = $obj->cart($_SESSION['SESS_CART']);
$obj->checklogin();

if(isset($_POST['edit']))
{
    extract($_POST);
    $array=array("id"=>$_SESSION['SESS_CUSID'],"fname"=>$firstname,"lname"=>$lastname,"email"=>$email,"talephone"=>$telephone);
    if($obj->update("customer",$array)==1)
    {
       echo $obj->Success("Account Detail Successfully Updated",$obj->filename());
    }
    else 
    {
        echo $obj->Error("Failed Please Try Again",$obj->filename());
    }
}
elseif(isset($_POST['passw']))
{
    extract($_POST);
    if(empty($password) or empty($confirm))
    {
        echo $obj->Error("Field Are Empty Please Try Again",$obj->filename());
    }
 elseif($password!=$confirm)
    {
        echo $obj->Error("Password Mismatch Please Try Again",$obj->filename());        
    }
    else 
    {
        $array=array("id"=>$_SESSION['SESS_CUSID'],"password"=>$obj->password($password));
        if($obj->update("customer",$array)==1)
        {
           echo $obj->Success("Account Password Successfully Updated",$obj->filename());
        }
        else 
        {
            echo $obj->Error("Failed Please Try Again",$obj->filename());
        }    
    }
}
 elseif(isset($_POST['addr']))
{
    extract($_POST);
    $array=array("id"=>$_SESSION['SESS_CUSID'],"address1"=>$address);
    if($obj->update("customer",$array)==1)
    {
       echo $obj->Success("Account Address Successfully Updated",$obj->filename());
    }
    else 
    {
        echo $obj->Error("Failed Please Try Again",$obj->filename());
    }
}



?>
<!DOCTYPE html>
<html dir="ltr" lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>Mobile online store</title>
        <meta name="description" content="Mobile online store" />
        <link href="images/favicon.png" rel="icon" />
        <?php include('include/headlink.php'); ?>
        <script>
            function account(str,userid)
            {
                if (str == "")
                {
                    document.getElementById("accountquery").innerHTML = "";
                    return;
                }
                
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        $("#accountquery").fadeOut(500);
                        $("#accountquery").fadeIn(500);
                        document.getElementById("accountquery").innerHTML = xmlhttp.responseText;
                    }
                }
                
                xmlhttp.open("GET", "ajax/account.php?q="+str+"&userid="+userid, true);
                xmlhttp.send();
            }
        </script>
    </head>
    <body class="account-login">
        <div class="bg-1">
            <div class="main-shining">
                <div class="row-1">
                    <?php include('include/header.php'); ?>
                    <div class="clear"></div>
                    <?php include('include/menu.php'); ?>
                </div>
                <div class="clear"></div>

                <div class="main-container">
                    <p id="back-top"> <a href="#top"><span></span></a> </p>
                    <div id="container"><?php echo $obj->ShowMsg(); ?>
                        <?php if(isset($_SESSION['SESS_CUSID'])!='') { ?>
                        <div class='successmsg'  style=''><?php echo "Welcome ".$_SESSION['SESS_CUSNAME']." Sir, Today is ".date('D, d M, Y'); ?><a href="ajax/logout.php" style="float: right; margin-top:-4px;" class="button"><span>Logout</span></a></div>
                        <?php } ?>
                        <div id="notification"> </div>

                        <?php include('include/category.php'); ?>

                        <div id="content">  


                            <div class="breadcrumb">
                                <a href="index.php">Home</a>
                                » <a class="last" href="account.php">Account</a>
                            </div>

                            <div class="box-container" id="accountquery">
                                <h1>My Account</h1>
                                <h2>My Account</h2>

                                <div class="content">
                                    <ul>
                                        <li><a href="#" onclick="account('1','2')">Edit your account information</a></li>
                                        <li><a href="#" onclick="account('2','2')">Change your password</a></li>
                                        <li><a href="#" onclick="account('3','2')">Modify your address book entries</a></li>
                                        
                                    </ul>
                                </div>
                                <h2>My Orders</h2>
                                <div class="content">
                                    <ul>
                                        <li><a href="#" onclick="account('4','2')">View your order history</a></li>
                                        <li><a href="#" onclick="account('5','2')">Your Transactions</a></li>
                                    </ul>
                                </div>
                                
                            </div>


                        </div>
                        <script type="text/javascript"><!--
                        $('#login input').keydown(function(e) {
                                if (e.keyCode == 13) {
                                    $('#login').submit();
                                }
                            });
//--></script> 
                        <div class="clear"></div>
                    </div>
                </div>
                <?php include('include/footer.php'); ?>
                <script type="text/javascript" 	src="js/livesearch.js"></script>
            </div>
        </div>
        <script type="text/javascript"></script>
    </body>
</html>
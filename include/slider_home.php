<div class="header-modules">
    <script type="text/javascript">
        $(window).load(function() {
            $('#slideshow0').nivoSlider({
                effect: 'fade', // Specify sets like: 'fold,fade,sliceDown'
                slices: 15, // For slice animations
                boxCols: 8, // For box animations
                boxRows: 4, // For box animations
                animSpeed: 500, // Slide transition speed
                pauseTime: 7000, // How long each slide will show
                startSlide: 0, // Set starting Slide (0 index)
                directionNav: false, // Next & Prev navigation
                controlNav: true, // 1,2,3... navigation
                controlNavThumbs: true, // Use thumbnails for Control Nav
                pauseOnHover: true, // Stop animation while hovering
                manualAdvance: false, // Force manual transitions
                prevText: 'Prev', // Prev directionNav text
                nextText: 'Next', // Next directionNav text
                randomStart: false, // Start on a random slide
                beforeChange: function() {
                }, // Triggers before a slide transition
                afterChange: function() {
                }, // Triggers after a slide transition
                slideshowEnd: function() {
                }, // Triggers after all slides have been shown
                lastSlide: function() {
                }, // Triggers when last slide is shown
                afterLoad: function() {
                } // Triggers when slider has loaded
            });
            $('.nivo-controlNav>a').not('.nivo-controlNav>a.nivo-control').remove();
        });
    </script>
    <div class="slideshow">
        <div id="slideshow0" class="nivoSlider" style="width: 785px; height: 421px;">
            <img title="&lt;h1&gt;iPhone 5&lt;/h1&gt;
                 &lt;h2&gt;LCD Specials&lt;/h2&gt;
                 &lt;p&gt;iPhone 5 LCD & Digitizer Combo
                 OEM Quality Parts, Fully Tested, & guaranteed.
                  &lt;/p&gt;
                 &lt;h3&gt; from &lt;span&gt;$60.00&lt;/span&gt;&lt;/h3&gt;
                 &lt;a class=&quot;button&quot; href=&quot;product_view.php?route=product/category&amp;path=24&quot;&gt;SHOP NOW&lt;/a&gt;  " src="images/slide-4-785x421.png" alt="" data-thumb="images/slide-4-785x421.png" />
            <img title="&lt;h1&gt;iPhone 5S&lt;/h1&gt;
                 &lt;h2&gt;LCD Specials&lt;/h2&gt;
                 &lt;p&gt;iPhone 5S LCD & Digitizer Combo
                 OEM Quality Part, Fully Tested, & guaranteed.
                 Best Price! &lt;/p&gt;
                 &lt;h3&gt; from &lt;span&gt;$60.00&lt;/span&gt;&lt;/h3&gt;
                 &lt;a class=&quot;button&quot; href=&quot;index.php?route=product/product&amp;product_id=28&quot;&gt;SHOP NOW&lt;/a&gt;" src="images/slide-1-785x421.png" alt="" data-thumb="images/slide-1-785x421.png" />
            <img title="&lt;h1&gt;Unlocks&lt;/h1&gt;
                 &lt;h2&gt;GSM Unlocks!&lt;/h2&gt;
                 &lt;p&gt;iPhone Factory Unlocks, Android Factory Unlocks
                 Same Day Service!
                 Guaranteed Results &lt;/p&gt;
                 &lt;h3&gt; from &lt;span&gt;$35.00&lt;/span&gt;&lt;/h3&gt;
                 &lt;a class=&quot;button&quot; href=&quot;index.php?route=product/product&amp;product_id=29&quot;&gt;SHOP NOW&lt;/a&gt;" src="images/slide-2-785x421.png" alt="" data-thumb="images/slide-2-785x421.png" />
            <img title="&lt;h1&gt;Local Deliveries&lt;/h1&gt;
                 &lt;h2&gt;We bring it to you&lt;/h2&gt;
                 &lt;p&gt;Have a local Repair Store, in the Detroit Metro Area?
                 Set up an account, place an order, & we will deliver it to you!
                Same day! &lt;/p&gt;
                 &lt;h3&gt; from &lt;span&gt;FREE&lt;/span&gt;&lt;/h3&gt;
                 &lt;a class=&quot;button&quot; href=&quot;index.php?route=product/product&amp;product_id=32&quot;&gt;SHOP NOW&lt;/a&gt;" src="images/slide-3-785x421.png" alt="" data-thumb="images/slide-3-785x421.png" />
        </div>
    </div>


    <div id="banner0" class="banner clearfix">
        <div><a href="product_view.php"><img src="images/banner-3-333x231.png" alt="" title="" /></a></div>
        <div><a href="product_view.php"><img src="images/banner-2-333x231.png" alt="" title="" /></a></div>
        <div><a href="product_view.php"><img src="images/banner-1-333x231.png" alt="" title="" /></a></div>
    </div>
    <div class="clear"></div>
</div>
<div class="footer-wrap">
    <div class="outer">
        <div id="footer">
            <div class="column col-1" style="width:130px;">
                <h3>Information</h3>
                <ul>
                    <li><a href="about.php">About Us</a></li>
                    <li><a href="delivery.php">Delivery</a></li>
                    <li><a href="privacy.php">Privacy Policy</a></li>
                    <li><a href="term.php">Terms &amp; Conditions</a></li>
                </ul>
            </div>
            <div class="column col-2" style="width:150px;">
                <h3>Customer Service</h3>
                <ul>
                    <li><a href="contact.php">Contact Us</a></li>
                    <li><a href="return.php">Returns</a></li>
                    <li><a href="sitemap.php">Site Map</a></li>
                </ul>
            </div>
            
            <div class="column col-4" style="width:150px;">
                <h3>My Account</h3>
                <ul>
                    <li><a href="account.php">My Account</a></li>
                    <li><a href="order_history.php">Order History</a></li>
                    <li><a href="unlockorder_check.php">Check Unlock Order</a></li>
                    <li><a href="newsletter.php">Newsletter</a></li>
                </ul>
            </div>
            <div class="column col-3" style="width:130px;">
                <!--<h3>Extras</h3>
                <ul>
                    <li><a href="gift.php">Gift Vouchers</a></li>
                    <li><a href="affiliate.php">Affiliates</a></li>
                    <li><a href="special.php">Specials</a></li>
                </ul>-->
            </div>
            <div class="column col-4" style="float:right; width:350px; margin-right:20px;">
                    <div class="box_custom_image" style="width:53%; float:right;">
                        <a href="#"><img src="images/logof.png" style="height:66px;"></a>
                    </div>
                    <div class="box_custom_image" style="width:45%; float:right;">
                        <a href="#"><img src="images/paypal.png"></a>
                    </div>
                    <div class="clear"></div>
                    <br />
                    <h3 style="width:400px; position:absolute; right:-10px;">Wireless Geeks Inc, & iGeek Inc. 2014 all rights reserved. </h3>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
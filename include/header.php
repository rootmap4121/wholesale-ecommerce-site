<div id="header">    
    <div class="outer">
        <div id="logo"><a href="index.php"><img src="images/logo.png" title="Mobile online store" alt="Mobile online store" /></a></div>
        <div class="header-top1"> 
            <div class="cart-position">
                <div class="cart-inner"><div id="cart">

                        <div class="heading">

                            <a href="#">	
                                <b>Shopping Cart</b>
                                <span class="sc-button"></span>


                                <span id="cart-item"><div id="haha"><?php echo $obj->exists_multiple("cart", array("cart_id" => $cart)); ?></div></span>
                                <span id="cart-total">
                                    <div id="dd"><?php
                                        $datacartitem = $obj->SelectAllByID("cart", array("cart_id" => $cart));
                                        $item = 0;
                                        $price = 0;
                                        if (!empty($datacartitem))
                                            foreach ($datacartitem as $cartitem):
                                                $item+=$cartitem->quantity;
                                                $sprice = $obj->SelectAllByVal("product", "id", $cartitem->pid, "price");
                                                $price+=$cartitem->quantity * $sprice;
                                            endforeach;

                                        echo $item;
                                        ?> item(s) - <strong>$<?php echo number_format($price, 2); ?></strong>
                                    </div>
                                </span>

                                <span class="clear"></span>
                            </a>
                        </div>

                        <div class="content">
                            <?php
                            if ($obj->exists_multiple("cart", array("cart_id" => $cart)) == 0) {
                                ?>
                                <div class="empty">Your shopping cart is empty!</div>
                                <?php
                            } else {
                                ?>
                                <span class="latest-added">Latest added product(s):</span>
                                <div class="mini-cart-info">
                                    <table class="cart">
                                        
                                        <tbody>
                                            <?php 
                                            $cartdata=$obj->SelectAllByID("cart",array("cart_id"=>$cart));
                                            $subtotal=0;
                                            if(!empty($cartdata))
                                            foreach ($cartdata as $cc):
                                            ?>
                                            <tr>
                                                <td class="name"><a href="#"><?php echo $obj->SelectAllByVal("product","id",$cc->pid,"name"); ?></a>
                                                    <div>
                                                    </div>
                                                    <span class="quantity"><?php $price=$obj->SelectAllByVal("product","id",$cc->pid,"price");
                                                    $total=$cc->quantity*$price;
                                                    echo $price; ?> &nbsp; x &nbsp; <?php echo $cc->quantity; ?> = </span>
                                                    <span class="total">$<?php echo number_format($total,2); ?></span>
                                                </td>
                                                <?php $subtotal+=$total; ?>
                                                <td class="remove"><a href="shoppingcart.php?cartdel=<?php echo $cc->id; ?>"><span><img src="images/close.png" alt="Remove" title="Remove"></span></a></td>
                                            </tr>
                                            <?php 
                                            endforeach;
                                            ?>
                                            
                                        </tbody>
                                    
                                    </table>
                                </div>
                                <div>
                                    <table class="total">
                                        <tbody><tr>
                                                <td class="total-right" align="right"><b>Sub-Total:</b></td>
                                                <td class="total-left" align="left"><span class="t-price">$<?php echo number_format($subtotal,2); ?></span></td>
                                            </tr>
                                            <tr>
                                                <td class="total-right" align="right"><b>Total:</b></td>
                                                <td class="total-left" align="left"><span class="t-price">$<?php echo number_format($subtotal,2); ?></span></td>
                                            </tr>
                                        </tbody></table>
                                </div>
                                <div class="checkout"><a class="button" href="shoppingcart.php"><span>View Cart</span></a> <a class="button" href="checkout.php"><span>Checkout</span></a></div>

    <?php
}
?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="banner-top"></div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>

    </div>
    <div class="clear"></div>
</div>
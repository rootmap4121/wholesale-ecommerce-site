<div class="box box-filter">
    <div class="box-heading"><span>Refine Search</span></div>
    <div class="box-content">
        <ul class="box-filter">
            <li><span id="filter-group1">Search</span>
                <ul>
                    <li>
                        <input type="checkbox" value="1" id="filter1" />
                        <label for="filter1">A-Z</label>
                    </li>
                    <li>
                        <input type="checkbox" value="2" id="filter2" />
                        <label for="filter2">Z-A</label>
                    </li>
                </ul>
            </li>
        </ul>
        <a id="button-filter" class="button"><span>Refine Search</span></a>
    </div>
</div>
<script type="text/javascript"><!--
$('#button-filter').bind('click', function() {
        filter = [];

        $('.box-filter input[type=\'checkbox\']:checked').each(function(element) {
            filter.push(this.value);
        });

        location = 'index.php?route=category.php?cid=' + filter.join(',');
    });
//--></script>
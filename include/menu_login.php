<div class="box account">
    <div class="box-heading"><span>Account</span></div>
    <div class="box-content">
        <ul class="acount">
            <li><a href="login.php">Login</a> / <a href="create_account.php">Register</a></li>
            <li><a href="forgotten.php">Forgotten Password</a></li>
            <li><a href="account.php">My Account</a></li>
            <li><a href="address.php">Address Books</a></li>
            <li><a href="wishlist.php">Wish List</a></li>
            <li><a href="order.php">Order History</a></li>
            <li><a href="download.php">Downloads</a></li>
            <li><a href="return.php">Returns</a></li>
            <li><a href="transaction.php">Transactions</a></li>
            <li><a href="newsletter.php">Newsletter</a></li>
        </ul>
    </div>
</div>
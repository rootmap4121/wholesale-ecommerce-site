<div id="column-left">
    <script type="text/javascript">
        $(document).ready(function() {
            $('li.cat-header ul').each(function(index) {
                $(this).prev().addClass('idCatSubcat')
            });
            $('li.cat-header a').after('<span></span>');
            $('li.cat-header ul').css('display', 'none');
            $('li.cat-header ul.active').css('display', 'block');
            $('li.cat-header ul').each(function(index) {
                $(this).prev().addClass('close').click(function() {
                    if (
                            $(this).next().css('display') == 'none') {
                        $(this).next().slideDown(400, function() {
                            $(this).prev().removeClass('collapsed').addClass('expanded');
                        });
                    } else {
                        $(this).next().slideUp(400, function() {
                            $(this).prev().removeClass('expanded').addClass('collapsed');
                            $(this).find('ul').each(function() {
                                $(this).hide().prev().removeClass('expanded').addClass('collapsed');
                            });
                        });
                    }
                    return false;
                });
            });
        });
    </script>
    <div class="box category">
        <div class="box-heading"><span>Categories</span></div>
        <div class="box-content">

            <div class="box-category">
                <ul>
                    <?php 
                    $sqlcategory=$obj->SelectAll("category");
                    if(!empty($sqlcategory))
                    foreach ($sqlcategory as $cat):
                    ?>
                    <li class="cat-header">
                        <a href="category.php?cid=<?php echo $cat->id; ?>"><?php echo $cat->name; ?></a>
                                <?php
                                $existssub=$obj->exists_multiple("subcategory",array("cid"=>$cat->id));
                                if($existssub!=0){
                                $sqlsubcat=$obj->SelectAllByID("subcategory",array("cid"=>$cat->id));
                                if(!empty($sqlsubcat))
                                
                                ?>
                                <ul>
                                    <?php foreach ($sqlsubcat as $subcat): ?>
                                    <li><a href="subcategory.php?scid=<?php echo $subcat->id; ?>"><?php echo $subcat->name; ?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                                <?php } ?>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
    
    <?php if($obj->filename()=="login.php" || $obj->filename()=="create_account.php"):  include('menu_login.php'); endif;  ?>
    <?php if($obj->filename()=="product_view.php"):  include('latest.php'); endif;  ?>
    <?php if($obj->filename()=="category.php" || $obj->filename()=="subcategory.php"):  include('menu_search.php'); include('bestseller.php'); endif;  ?>
    <div class="box_custom_image">
        <a href="#" class="border-1"><img src="images/cat/apple.png"></a>
    </div>
    <div class="box_custom_image">
        <a href="#" class="border-1"><img src="images/cat/samsung.png"></a>
    </div>
    <div class="box_custom_image">
        <a href="#" class="border-1"><img src="images/cat/loock.png"></a>
    </div>
</div>